#ifndef CIRCULARREMOVALDIALOG_H
#define CIRCULARREMOVALDIALOG_H

#include "customqfilterdialog.h"

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

#include "clouddata.h"
#include "cloudprocessing.h"
#include "batchprocessordialog.h"
#include "batchevent.h"

namespace Ui {
class CircularRemovalDialog;
}

class CircularRemovalDialog : public CustomQFilterDialog
{
    Q_OBJECT

public:
    explicit CircularRemovalDialog(QWidget *parent = 0);
    ~CircularRemovalDialog();

private slots:
    void on_pushButton_filter_clicked();
    void on_pushButton_close_clicked();
    void on_pushButton_batch_clicked();

private:
    Ui::CircularRemovalDialog *ui;

};

#endif // CIRCULARREMOVALDIALOG_H
