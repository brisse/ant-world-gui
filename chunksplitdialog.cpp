#include "chunksplitdialog.h"
#include "../build/debug/ui_chunksplitdialog.h"

ChunkSplitDialog::ChunkSplitDialog(QWidget *parent) :
    CustomQFilterDialog(parent),
    ui(new Ui::ChunkSplitDialog)
{
    ui->setupUi(this);
}

ChunkSplitDialog::~ChunkSplitDialog()
{
    delete ui;
}

void ChunkSplitDialog::on_pushButton_filter_clicked()
{
    int p_split = ui->spinBox_chunk_percentage->value();
    bool enforce_reduction = ui->checkBox_enforce_reduction->isChecked();
    int max_chunk_size = ui->spinBox_max_chunk_size->value();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        Process::extract_chunks_inplace (*cloud_it, p_split, enforce_reduction, max_chunk_size, this);
        emit send_status_message(QString("Cloud splitted! Number of clusters: " + QString::number((*cloud_it)->get_number_of_clusters())));
    }

    emit filter_button_pressed();
}

void ChunkSplitDialog::on_pushButton_clsoe_clicked()
{
    this->hide();
}

void ChunkSplitDialog::on_pushButton_batch_clicked()
{
    int p_split = ui->spinBox_chunk_percentage->value();
    bool enforce_reduction = ui->checkBox_enforce_reduction->isChecked();
    int max_chunk_size = ui->spinBox_max_chunk_size->value();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        batch_processor->add_event(new ChunkSplitEvent(*cloud_it, p_split, enforce_reduction, max_chunk_size, batch_processor));
    }

    emit send_status_message(QString("Chunk split batch processing for " + QString::number(clouds.size()) + " clouds." ));
}
