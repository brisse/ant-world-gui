#ifndef BATCHEVENT_H
#define BATCHEVENT_H

#include "clouddata.h"
#include "cloudprocessing.h"
#include "inputoutput.h"
#include <QWidget>
#include <QString>

class BatchEvent
{
public:
    virtual void execute(void) = 0;
    virtual std::string get_event_name (void) = 0;
    virtual QString get_event_details (void) = 0;
protected:
    CloudData* cloud;
    std::string event_name;
};

class CircularRemovalEvent : public BatchEvent
{
public:
    CircularRemovalEvent(CloudData* cloud, float x_center, float y_center, float distance);
    void execute(void);
    std::string get_event_name(void) {return event_name;}
    QString get_event_details(void);
private:
    float x_center;
    float y_center;
    float distance;
};

class CircularClusteringEvent : public BatchEvent
{
public:
    CircularClusteringEvent(CloudData* cloud, float x_center, float y_center, float distance);
    void execute(void);
    std::string get_event_name(void) {return event_name;}
    QString get_event_details(void);
private:
    float x_center;
    float y_center;
    float distance;
};

class DownSamplingEvent : public BatchEvent
{
public:
    DownSamplingEvent(CloudData* cloud, double leaf_x, double leaf_y, double leaf_z, bool use_min_number_of_points,
                      int min_number_of_points, bool use_filter_limits, double min_limit, double max_limit,
                      bool individual_cluster, bool enforce_reduction, int min_reduction_percentage, double reduction_increment, QWidget* caller_dialog);
    void execute(void);
    std::string get_event_name(void) {return event_name;}
    QString get_event_details(void);
private:
    double leaf_x;
    double leaf_y;
    double leaf_z;
    bool use_min_number_of_points;
    int min_number_of_points;
    bool use_filter_limits;
    double min_limit;
    double max_limit;
    bool individual_cluster;
    bool enforce_reduction;
    int min_reduction_percentage;
    double reduction_increment;
    QWidget* caller_dialog;
};

class CalcNormalsEvent : public BatchEvent
{
public:
    CalcNormalsEvent(CloudData* input_cloud, double search_radius, int number_of_nearest_neighbors,
                     bool use_search_radius, bool set_view_port, bool individual_clusters, QWidget* caller_dialog);
    void execute(void);
    std::string get_event_name(void) {return event_name;}
    QString get_event_details(void);
private:
    double search_radius;
    int number_of_nearest_neighbors;
    bool use_search_radius;
    bool set_view_port;
    bool individual_clusters;
    QWidget* caller_dialog;
};

class RadiusOutlierRemovalEvent : public BatchEvent
{
public:
    RadiusOutlierRemovalEvent(CloudData *input_cloud, double search_radius, int min_neighbors_in_radius,
                              bool individual_clusters, QWidget* caller_dialog);
    void execute(void);
    std::string get_event_name(void) {return event_name;}
    QString get_event_details(void);
private:
    double search_radius;
    int min_neighors_in_radius;
    bool individual_clusters;
    QWidget* caller_dialog;
};

class StatisticalOutlierRemovalEvent : public BatchEvent
{
public:
    StatisticalOutlierRemovalEvent(CloudData *input_cloud, int number_of_neighbors, double std_dev_multiplier,
                                   bool individual_clusters, QWidget* caller_dialog);
    void execute(void);
    std::string get_event_name(void) {return event_name;}
    QString get_event_details(void);
private:
    int number_of_neighbors;
    double std_dev_multiplier;
    bool individual_clusters;
    QWidget* caller_dialog;
};

class ChunkSplitEvent : public BatchEvent
{
public:
    ChunkSplitEvent(CloudData* cloud, int p_split, bool enforce_reduction, int max_chunk_size, QWidget *caller_dialog);
    void execute(void);
    std::string get_event_name(void) {return event_name;}
    QString get_event_details(void);
private:
    int p_split;
    bool enforce_reduction;
    int max_chunk_size;
    QWidget* caller_dialog;
};

class ColorFilteringEvent : public BatchEvent
{
public:
    ColorFilteringEvent(CloudData *cloud, const int red_lower, const int red_upper, const int green_lower, const int green_upper, const int blue_lower,
                        const int blue_upper, const bool inside_red, const bool inside_green, const bool inside_blue);
    void execute(void);
    std::string get_event_name(void) {return event_name;}
    QString get_event_details(void);
private:
    int red_lower;
    int red_upper;
    int green_lower;
    int green_upper;
    int blue_lower;
    int blue_upper;
    bool inside_red;
    bool inside_green;
    bool inside_blue;

};

class ConditionalFilteringEvent : public BatchEvent
{
public:
    ConditionalFilteringEvent(CloudData* cloud, double lower_limit, double upper_limit, std::string axis);
    void execute (void);
    std::string get_event_name(void) {return event_name;}
    QString get_event_details(void);
private:
    double lower_limit;
    double upper_limit;
    std::string axis;

};

class MLSUpsamplingEvent : public BatchEvent
{
public:
    MLSUpsamplingEvent(CloudData* input_cloud,
                       int sampling_method,
                       bool use_search_radius,
                       double search_radius,
                       double sqrt_gaussian_neighor,
                       bool polynomial_fit,
                       int polynomial_order,
                       double upsampling_radius,
                       double upsampling_step_size,
                       int desired_num_of_points_in_radius,
                       int dilation_iterations,
                       float dilation_voxel_size,
                       bool calculate_normals,
                       bool individual_cluster,
                       QWidget* caller_dialog);
    void execute (void);
    std::string get_event_name(void) {return event_name;}
    QString get_event_details(void);
private:
    int sampling_method;
    bool use_search_radius;
    double search_radius;
    double sqrt_gaussian_neighor;
    bool polynomial_fit;
    int polynomial_order;
    double upsampling_radius;
    double upsampling_step_size;
    int desired_num_of_points_in_radius;
    int dilation_iterations;
    float dilation_voxel_size;
    bool calculate_normals;
    bool individual_cluster;
    QWidget* caller_dialog;
};

class ExtractClustersEvent : public BatchEvent
{
public:
    ExtractClustersEvent(CloudData *cloud, const double cluster_tolerance, const bool use_min_cluster, const int min_cluster_size, const bool use_max_cluster, const int max_cluster_size);
    void execute (void);
    std::string get_event_name(void) {return event_name;}
    QString get_event_details(void);
private:
    double cluster_tolerance;
    int min_cluster_size;
    int max_cluster_size;
    bool use_min_cluster;
    bool use_max_cluster;
};

class ProgressiveMorphologicalFilterEvent : public BatchEvent
{
public:
    ProgressiveMorphologicalFilterEvent(CloudData *input_cloud, int max_window_size, float slope, float initial_distance,
                                        float max_distance, float cell_size, float base, bool exponential,
                                        bool individual_cluster, bool merge_resultant_cluster, QWidget *caller_dialog);
    void execute(void);
    std::string get_event_name(void) {return event_name;}
    QString get_event_details(void);
private:
    int max_window_size;
    float slope;
    float initial_distance;
    float max_distance;
    float cell_size;
    float base;
    bool exponential;
    bool individual_cluster;
    bool merge_resultant_cluster;
    QWidget* caller_dialog;
};

class ExtractConcaveHullCloudEvent : public BatchEvent
{
public:
    ExtractConcaveHullCloudEvent(CloudData *cloud, const double alpha, const int dimensions, const bool keep_informations,
                                 bool individual_clusters, QWidget* caller_dialog);
    void execute (void);
    std::string get_event_name(void) {return event_name;}
    QString get_event_details(void);
private:
    double alpha;
    int dimensions;
    bool keep_informations;
    bool individual_clusters;
    QWidget* caller_dialog;
};

class RegionGrowingSegmentationEvent : public BatchEvent
{
public:
    RegionGrowingSegmentationEvent(CloudData *cloud, bool individual_cluster, int number_of_neighbors,
                                   bool use_min_cluster_size, int min_cluster_size, bool use_max_cluster_size,
                                   int max_cluster_size, bool use_smoothness_threshold, double smoothness_threshold,
                                   bool use_curvature_threshold, double curvature_threshold, bool make_residual_test,
                                   double residual_test, bool merge_resultant_clusters, QWidget* caller_dialog);
    void execute (void);
    std::string get_event_name(void) {return event_name;}
    QString get_event_details(void);
private:
    bool individual_cluster;
    int number_of_neighbors;
    bool use_min_cluster_size;
    int min_cluster_size;
    bool use_max_cluster_size;
    int max_cluster_size;
    bool use_smoothness_threshold;
    double smoothness_threshold;
    bool use_curvature_threshold;
    double curvature_threshold;
    bool make_residual_test;
    double residual_test;
    bool merge_resultant_clusters;
    QWidget* caller_dialog;
};

class ConcaveHullMeshingEvent : public BatchEvent
{
public:
    ConcaveHullMeshingEvent(CloudData *cloud, const double alpha, const int dimensions, const bool keep_informations,
                            bool individual_clusters, QWidget* caller_dialog);
    void execute (void);
    std::string get_event_name(void) {return event_name;}
    QString get_event_details(void);
private:
    double alpha;
    int dimensions;
    bool keep_informations;
    bool individual_clusters;
    QWidget* caller_dialog;
};

class TriangleMeshingEvent : public BatchEvent
{
public:
    TriangleMeshingEvent(CloudData *cloud, double search_radius, double mu, int max_nearest_neighbors,
                         double max_surface_angle, double min_angle, double max_angle, bool normal_consistency,
                         bool individual_cluster, QWidget* caller_dialog);
    void execute (void);
    std::string get_event_name(void) {return event_name;}
    QString get_event_details(void);
private:
    CloudData *cloud;
    double search_radius;
    double mu;
    int max_nearest_neighbors;
    double max_surface_angle;
    double min_angle;
    double max_angle;
    bool normal_consistency;
    bool individual_cluster;
    QWidget* caller_dialog;
};

class PoissonSurfaceMeshingEvent : public BatchEvent
{
public:
    PoissonSurfaceMeshingEvent(CloudData *cloud, bool use_max_tree_depth, int maximum_tree_depth,
                               bool use_min_tree_depth, int min_tree_depth,
                               bool use_point_weight, double point_weight,
                               bool use_scale, double scale,
                               bool use_solver_divide, int solver_divide,
                               bool use_iso_divide, int iso_divide,
                               bool use_sample_per_node, double sample_per_node,
                               bool use_degree, int degree,
                               bool set_confidence, bool set_manifold, bool set_polygons,
                               bool individual_cluster, QWidget* caller_dialog);
    void execute (void);
    std::string get_event_name(void) {return event_name;}
    QString get_event_details(void);
private:
    bool use_max_tree_depth;
    int maximum_tree_depth;
    bool use_min_tree_depth;
    int min_tree_depth;
    bool use_point_weight;
    double point_weight;
    bool use_scale;
    double scale;
    bool use_solver_divide;
    int solver_divide;
    bool use_iso_divide;
    int iso_divide;
    bool use_sample_per_node;
    double sample_per_node;
    bool use_degree;
    int degree;
    bool set_confidence;
    bool set_manifold;
    bool output_polygons;
    bool individual_cluster;
    QWidget* caller_dialog;
};

class QuadraticMeshDecimationEvent : public BatchEvent
{
public:
    QuadraticMeshDecimationEvent(CloudData *cloud, float reduction_factor, int iterations, QWidget* caller_dialog);
    void execute (void);
    std::string get_event_name(void) {return event_name;}
    QString get_event_details(void);
private:
    CloudData *cloud;
    float reduction_factor;
    int iterations;
    QWidget* caller_dialog;
};

class MeshQuadraticClusteringEvent : public BatchEvent
{
public:
    MeshQuadraticClusteringEvent(CloudData *cloud,
                                 bool auto_adjust_number_of_devisions,
                                 int number_of_x_divisions,
                                 int number_of_y_divisions,
                                 int number_of_z_divisions,
                                 bool use_feature_edges,
                                 double feature_points_angle,
                                 bool use_internal_triangles,
                                 QWidget* caller_dialog);
    void execute (void);
    std::string get_event_name(void) {return event_name;}
    QString get_event_details(void);
private:
    CloudData *cloud;
    bool auto_adjust_number_of_devisions;
    int number_of_x_divisions;
    int number_of_y_divisions;
    int number_of_z_divisions;
    bool use_feature_edges;
    double feature_points_angle;
    bool use_internal_triangles;
    QWidget* caller_dialog;
};

class MeshSubdivisionEvent : public BatchEvent
{
public:
    MeshSubdivisionEvent(CloudData *cloud, int filter_type, QWidget* caller_dialog);
    void execute (void);
    std::string get_event_name(void) {return event_name;}
    QString get_event_details(void);
private:
    CloudData *cloud;
    int filter_type;
    QWidget* caller_dialog;
};

class SmoothMeshLaplacianEvent : public BatchEvent
{
public:
    SmoothMeshLaplacianEvent(CloudData *cloud,
                             int number_of_iterations,
                             float convergence,
                             float relaxation_factor,
                             bool use_feature_edge_smoothing,
                             float feature_angle,
                             float edge_angle,
                             bool use_boundary_smoothing,
                             QWidget *caller_dialog);
    void execute (void);
    std::string get_event_name(void) {return event_name;}
    QString get_event_details(void);
private:
    CloudData *cloud;
    int number_of_iterations;
    float convergence;
    float relaxation_factor;
    bool use_feature_edge_smoothing;
    float feature_angle;
    float edge_angle;
    bool use_boundary_smoothing;
    QWidget *caller_dialog;
};

class SmoothPolyDataFilterEvent : public BatchEvent
{
public:
    SmoothPolyDataFilterEvent(CloudData *cloud,
                              double convergence,
                              int iterations,
                              double relaxation_factor,
                              bool use_feature_edge_smoothing,
                              double  feature_angle,
                              double edge_angle,
                              bool use_boundary_smoothing,
                              QWidget *caller_dialog);
    void execute (void);
    std::string get_event_name(void) {return event_name;}
    QString get_event_details(void);
private:
    CloudData *cloud;
    double convergence;
    int iterations;
    double relaxation_factor;
    bool use_feature_edge_smoothing;
    double  feature_angle;
    double edge_angle;
    bool use_boundary_smoothing;
    QWidget *caller_dialog;
};

class FilterClustersEvent : public BatchEvent
{
public:
    FilterClustersEvent(CloudData *input_cloud, bool use_max_allowed_height, double max_allowed_height,
                        bool use_min_allowed_height, double min_allowed_height, bool use_max_allowed_cluster_size,
                        int max_allowed_cluster_size, bool use_min_allowed_cluster_size, int min_allowed_cluster_size,
                        QWidget* caller_dialog);
    void execute(void);
    std::string get_event_name(void) {return event_name;}
    QString get_event_details(void);
private:
    bool use_max_allowed_height;
    double max_allowed_height;
    bool use_min_allowed_height;
    double min_allowed_height;
    bool use_max_allowed_cluster_size;
    int max_allowed_cluster_size;
    bool use_min_allowed_cluster_size;
    int min_allowed_cluster_size;
    QWidget* caller_dialog;
};

class SaveCloudsEvent : public BatchEvent
{
public:
    SaveCloudsEvent(CloudData * cloud, QString path, QString cloud_suffix);
    void execute (void);
    std::string get_event_name(void) {return event_name;}
    QString get_event_details(void);
private:
    QString path;
    QString cloud_suffix;
};


#endif // BATCHEVENT_H
