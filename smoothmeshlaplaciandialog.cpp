#include "smoothmeshlaplaciandialog.h"
#include "../build/debug/ui_smoothmeshlaplaciandialog.h"

SmoothMeshLaplacianDialog::SmoothMeshLaplacianDialog(QWidget *parent) :
    CustomQFilterDialog(parent),
    ui(new Ui::SmoothMeshLaplacianDialog)
{
    ui->setupUi(this);
}

SmoothMeshLaplacianDialog::~SmoothMeshLaplacianDialog()
{
    delete ui;
}

void SmoothMeshLaplacianDialog::on_pushButton_filter_clicked()
{
    int number_of_iterations = this->ui->spinBox_number_of_iterations->value();
    float convergence = this->ui->doubleSpinBox_convergence->value();
    float relaxation_factor = this->ui->doubleSpinBox_relaxatoin_factor->value();
    bool use_feature_edge_smoothing = this->ui->checkBox_use_feature_edge_smoothing->isChecked();
    float feature_angle = this->ui->doubleSpinBox_feature_angle->value();
    float edge_angle = this->ui->doubleSpinBox_edge_angle->value();
    bool use_boundary_smoothing = this->ui->checkBox_use_boundary_smoothing->isChecked();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
//        Process::smooth_poly_data_filter_inplace(*cloud_it);
        Process::smooth_mesh_laplacian(*cloud_it, number_of_iterations, convergence, relaxation_factor, use_feature_edge_smoothing, feature_angle, edge_angle, use_boundary_smoothing, this);
        emit send_status_message(QString("Laplacian mesh smoothing done! Number of polygons: " + QString::number((*cloud_it)->get_mesh_size())));
    }

    emit filter_button_pressed();
}

void SmoothMeshLaplacianDialog::on_pushButton_batch_clicked()
{
    int number_of_iterations = this->ui->spinBox_number_of_iterations->value();
    float convergence = this->ui->doubleSpinBox_convergence->value();
    float relaxation_factor = this->ui->doubleSpinBox_relaxatoin_factor->value();
    bool use_feature_edge_smoothing = this->ui->checkBox_use_feature_edge_smoothing->isChecked();
    float feature_angle = this->ui->doubleSpinBox_feature_angle->value();
    float edge_angle = this->ui->doubleSpinBox_edge_angle->value();
    bool use_boundary_smoothing = this->ui->checkBox_use_boundary_smoothing->isChecked();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        batch_processor->add_event(new SmoothMeshLaplacianEvent(*cloud_it, number_of_iterations, convergence, relaxation_factor, use_feature_edge_smoothing, feature_angle, edge_angle, use_boundary_smoothing, batch_processor));
    }

    emit send_status_message(QString("Laplacian mesh smoothing batch processing for " + QString::number(clouds.size()) + " clouds." ));

}

void SmoothMeshLaplacianDialog::on_pushButton_close_clicked()
{
    this->hide();
}
