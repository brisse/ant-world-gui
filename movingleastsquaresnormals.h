#ifndef MOVINGLEASTSQUARESNORMALS_H
#define MOVINGLEASTSQUARESNORMALS_H

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/features/normal_3d.h>
#include <pcl/surface/mls.h>

#include "clouddata.h"

using namespace pcl;


class MovingLeastSquaresNormals : public MovingLeastSquares<PointXYZRGB, PointXYZRGB>
{

public:
    MovingLeastSquaresNormals();

    void get_normals(pcl::PointCloud<pcl::Normal> & ret_normals);

};

#endif // MOVINGLEASTSQUARESNORMALS_H
