#ifndef CLOUDDATA_H
#define CLOUDDATA_H

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/common/common.h>
#include <pcl/PolygonMesh.h>
#include <pcl/PointIndices.h>

#include <pcl/filters/radius_outlier_removal.h>

class CloudData
{
public:
    CloudData();
    CloudData(pcl::PointCloud<pcl::PointXYZRGB>::Ptr initial_cloud, std::string initial_name, std::string initial_path);
    void init(pcl::PointCloud<pcl::PointXYZRGB> initial_cloud, std::string initial_name, std::string initial_path);

    // getter
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr get_original_cloud (void)
    {return this->original_cloud;}
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr get_cloud (void)
    {return this->cloud;}
    pcl::PointCloud<pcl::Normal>::Ptr get_normals (void)
    {return this->normals;}
    pcl::PolygonMesh::Ptr get_mesh (void)
    {return this->mesh;}

    std::vector<pcl::PointIndices> get_cluster_indices (void)
    {return this->cluster_indices;}

    unsigned int get_original_cloud_size (void)
    {return original_cloud->size();}
    unsigned int get_cloud_size (void)
    {return cloud->size();}
    unsigned int get_normals_size (void)
    {return normals->size();}
    unsigned int get_mesh_size (void)
    {return mesh->polygons.size();}

    unsigned int get_number_of_clusters (void)
    {return cluster_indices.size();}

    std::string get_cloud_name (void)
    {return this->cloud_name;}
    std::string get_cloud_path (void)
    {return this->cloud_path;}

    pcl::PointIndices get_cloud_point_indices (void);
    pcl::PointIndices::Ptr get_cloud_point_indices_ptr(void);

    bool has_original_cloud (void)
    {return this->original_cloud_set;}
    bool has_cloud (void)
    {return this->cloud_set;}
    bool has_normals (void)
    {return this->normals_set;}
    bool has_mesh (void)
    {return this->mesh_set;}

    bool has_clusters (void)
    {return this->clusters_set;}

    bool has_color_cloud (void)
    {return this->is_colour_cloud;}

    // setter
    void set_original_cloud (pcl::PointCloud<pcl::PointXYZRGB> initial_cloud);
    void set_cloud (pcl::PointCloud<pcl::PointXYZRGB> initial_cloud);
    void set_normals (pcl::PointCloud<pcl::Normal> initial_normals);
    void set_mesh (pcl::PolygonMesh initial_mesh);

    void set_cluster_indices (std::vector<pcl::PointIndices> initial_indices);

    void set_cloud_name(std::string initial_name);
    void set_cloud_path(std::string initial_path);

    // reset point cloud
    void reset_cloud (void);

private:
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr original_cloud;
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud;
    pcl::PointCloud<pcl::Normal>::Ptr normals;
    pcl::PolygonMesh::Ptr mesh;

    std::vector<pcl::PointIndices> cluster_indices;

    unsigned int original_cloud_size;
    unsigned int cloud_size;
    unsigned int normals_size;
    unsigned int mesh_size;

    unsigned int number_of_clusters;

    bool original_cloud_set;
    bool cloud_set;
    bool normals_set;
    bool mesh_set;

    bool clusters_set;

    bool is_colour_cloud;

    std::string cloud_name;
    std::string cloud_path;

};

#endif // CLOUDDATA_H
