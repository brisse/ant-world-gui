#include "mlsupsamplingdialog.h"
#include "../build/debug/ui_mlsupsamplingdialog.h"

MLSUpsamplingDialog::MLSUpsamplingDialog(QWidget *parent) :
    CustomQFilterDialog(parent),
    ui(new Ui::MLSUpsamplingDialog)
{
    ui->setupUi(this);
}

MLSUpsamplingDialog::~MLSUpsamplingDialog()
{
    delete ui;
}

void MLSUpsamplingDialog::set_calc_normals(bool calculate_normals)
{
    this->ui->checkBox_calculate_smoothed_normals->setChecked(calculate_normals);
}

void MLSUpsamplingDialog::on_pushButton_2_clicked()
{
    // general parameters
    bool use_search_radius = ui->checkBox_use_search_radius->isChecked();
    double search_radius = ui->doubleSpinBox_search_radius->value();
    double sqrt_gaussian_neighbor = ui->doubleSpinBox_sqrt_gauss_neigh->value();
    bool polynomial_fit = ui->checkBox_polyfit->isChecked();
    int polynomial_order = ui->spinBox_polyorder->value();

    // sampling method: NONE
    bool sampling_method_none = ui->radioButton_NONE->isChecked();

    // sampling method: SAMPEL LOCAL PLANE
    bool sampling_method_sample_local_plane = ui->radioButton_SAMPLE_LOCAL_PLANE->isChecked();
    double upsampling_radius = ui->doubleSpinBox_upsampling_radius->value();
    double upsampling_step_size = ui->doubleSpinBox_upsampling_step_siye->value();

    // sampling method: RANDOM UNIFORM DENSITY
    bool sampling_method_random_uniform_density = ui->radioButton_RANDO_UNIFORM_DENSITY->isChecked();
    int desired_num_of_points_in_radius = ui->spinBox_desired_num_points_in_radius->value();

    // sampling method: VOXEL GRID DILATION
    bool sampling_method_voxel_grid_dilation = ui->radioButton_VOXEL_GRID_DILATION->isChecked();
    int dilation_iterations = ui->spinBox_dilation_iterations->value();
    double dilation_voxel_size = ui->doubleSpinBox_dilation_voxel_size->value();

    int sampling_method = 0;
    if (sampling_method_none)
        sampling_method = 0;
    else if (sampling_method_sample_local_plane)
        sampling_method = 1;
    else if (sampling_method_random_uniform_density)
        sampling_method = 2;
    else if (sampling_method_voxel_grid_dilation)
        sampling_method = 3;

    bool calculate_normals = ui->checkBox_calculate_smoothed_normals->isChecked();

    bool individual_cluster = ui->checkBox_individual_cluster->isChecked();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        Process::mls_upsampling_inplace_1(*cloud_it,
                                          sampling_method,
                                          use_search_radius,
                                          search_radius,
                                          sqrt_gaussian_neighbor,
                                          polynomial_fit,
                                          polynomial_order,
                                          upsampling_radius,
                                          upsampling_step_size,
                                          desired_num_of_points_in_radius,
                                          dilation_iterations,
                                          dilation_voxel_size,
                                          calculate_normals,
                                          individual_cluster,
                                          this);
        emit send_status_message(QString("MLS upsampling done! New cloud size is " + QString::number((*cloud_it)->get_cloud_size())));
    }

    emit filter_button_pressed();
}

void MLSUpsamplingDialog::on_pushButton_clicked()
{
    this->hide();
}

void MLSUpsamplingDialog::on_pushButton_batch_clicked()
{
    // general parameters
    bool use_search_radius = ui->checkBox_use_search_radius->isChecked();
    double search_radius = ui->doubleSpinBox_search_radius->value();
    double sqrt_gaussian_neighbor = ui->doubleSpinBox_sqrt_gauss_neigh->value();
    bool polynomial_fit = ui->checkBox_polyfit->isChecked();
    int polynomial_order = ui->spinBox_polyorder->value();

    // sampling method: NONE
    bool sampling_method_none = ui->radioButton_NONE->isChecked();

    // sampling method: SAMPEL LOCAL PLANE
    bool sampling_method_sample_local_plane = ui->radioButton_SAMPLE_LOCAL_PLANE->isChecked();
    double upsampling_radius = ui->doubleSpinBox_upsampling_radius->value();
    double upsampling_step_size = ui->doubleSpinBox_upsampling_step_siye->value();

    // sampling method: RANDOM UNIFORM DENSITY
    bool sampling_method_random_uniform_density = ui->radioButton_RANDO_UNIFORM_DENSITY->isChecked();
    int desired_num_of_points_in_radius = ui->spinBox_desired_num_points_in_radius->value();

    // sampling method: VOXEL GRID DILATION
    bool sampling_method_voxel_grid_dilation = ui->radioButton_VOXEL_GRID_DILATION->isChecked();
    int dilation_iterations = ui->spinBox_dilation_iterations->value();
    double dilation_voxel_size = ui->doubleSpinBox_dilation_voxel_size->value();

    int sampling_method = 0;
    if (sampling_method_none)
        sampling_method = 0;
    else if (sampling_method_sample_local_plane)
        sampling_method = 1;
    else if (sampling_method_random_uniform_density)
        sampling_method = 2;
    else if (sampling_method_voxel_grid_dilation)
        sampling_method = 3;

    bool calculate_normals = ui->checkBox_calculate_smoothed_normals->isChecked();

    bool individual_cluster = ui->checkBox_individual_cluster->isChecked();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        batch_processor->add_event(new MLSUpsamplingEvent(*cloud_it,
                                                          sampling_method,
                                                          use_search_radius,
                                                          search_radius,
                                                          sqrt_gaussian_neighbor,
                                                          polynomial_fit,
                                                          polynomial_order,
                                                          upsampling_radius,
                                                          upsampling_step_size,
                                                          desired_num_of_points_in_radius,
                                                          dilation_iterations,
                                                          dilation_voxel_size,
                                                          calculate_normals,
                                                          individual_cluster,
                                                          batch_processor));
    }

    emit send_status_message(QString("Upsampling batch processing for " + QString::number(clouds.size()) + " clouds." ));
}
