#ifndef RADIUSOUTLIERREMOVALDIALOG_H
#define RADIUSOUTLIERREMOVALDIALOG_H

#include "customqfilterdialog.h"
#include <QProgressDialog>

#include <pcl/point_types.h>
#include <pcl/common/common.h>
#include <pcl/filters/radius_outlier_removal.h>

#include "clouddata.h"
#include "cloudprocessing.h"

namespace Ui {
class RadiusOutlierRemovalDialog;
}

class RadiusOutlierRemovalDialog : public CustomQFilterDialog
{
    Q_OBJECT

public:
    explicit RadiusOutlierRemovalDialog(QWidget *parent = 0);
    ~RadiusOutlierRemovalDialog();


private:
    Ui::RadiusOutlierRemovalDialog *ui;

private slots:
    void on_pushButton_filter_clicked();
    void on_pushButton_close_clicked();
    void on_pushButton_batch_clicked();
};

#endif // RADIUSOUTLIERREMOVALDIALOG_H
