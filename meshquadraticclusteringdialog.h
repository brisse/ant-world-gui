#ifndef MESHQUADRATICCLUSTERINGDIALOG_H
#define MESHQUADRATICCLUSTERINGDIALOG_H

#include "customqfilterdialog.h"

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

#include "clouddata.h"
#include "cloudprocessing.h"


namespace Ui {
class MeshQuadraticClusteringDialog;
}

class MeshQuadraticClusteringDialog : public CustomQFilterDialog
{
    Q_OBJECT

public:
    explicit MeshQuadraticClusteringDialog(QWidget *parent = 0);
    ~MeshQuadraticClusteringDialog();

private slots:
    void on_pushButton_filter_clicked();
    void on_pushButton_batch_clicked();
    void on_pushButton_close_clicked();

private:
    Ui::MeshQuadraticClusteringDialog *ui;
};

#endif // MESHQUADRATICCLUSTERINGDIALOG_H
