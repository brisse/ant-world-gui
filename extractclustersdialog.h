#ifndef EXTRACTCLUSTERSDIALOG_H
#define EXTRACTCLUSTERSDIALOG_H

#include "customqfilterdialog.h"

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

#include <pcl/kdtree/kdtree.h>
#include <pcl/segmentation/extract_clusters.h>

#include "clouddata.h"
#include "cloudprocessing.h"

namespace Ui {
class ExtractClustersDialog;
}

class ExtractClustersDialog : public CustomQFilterDialog
{
    Q_OBJECT

public:
    explicit ExtractClustersDialog(QWidget *parent = 0);
    ~ExtractClustersDialog();

private:
    Ui::ExtractClustersDialog *ui;

private slots:
    void on_pushButton_filter_clicked();
    void on_pushButton_close_clicked();
    void on_pushButton_batch_clicked();
};

#endif // EXTRACTCLUSTERSDIALOG_H
