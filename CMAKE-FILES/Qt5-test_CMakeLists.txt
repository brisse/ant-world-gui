# Test Cmake Version
cmake_minimum_required( VERSION 3.6 )

# Create Project
project( ant-world )

# Find PCL Package
# set ( PCL_ROOT "/usr/local/Cellar/pcl/1.8.0_7")
find_package( PCL 1.8 REQUIRED )

# Find VTK Package
# set( VTK_DIR "/usr/local/Cellar/vtk/7.1.1_1" )
find_package( VTK REQUIRED )

# Find Qt Package
find_package( Qt5Widgets REQUIRED )

set(CMAKE_CXX_FLAGS -Wno-inconsistent-missing-override)

# Set Automatic MOC
set( CMAKE_INCLUDE_CURRENT_DIR ON )
set( CMAKE_AUTOMOC ON )

# Set Project Source Files
set (project_SOURCES
main.cpp
mainwindow.cpp
cloudprocessing.cpp
conditionalfilteringdialog.cpp
radiusoutlierremovaldialog.cpp
downsamplingdialog.cpp
mlsupsamplingdialog.cpp
calcnormalsdialog.cpp
trianglemeshingdialog.cpp
poissonsufracemeshingdialog.cpp
clouddata.cpp
extractclustersdialog.cpp
regiongrowingsegmentationdialog.cpp
chunksplitdialog.cpp
circularremovaldialog.cpp
colorfilteringdialog.cpp
disjoincloudsdialog.cpp
customqprogressdialog.cpp
bilateralupsamplingdialog.cpp
batchprocessordialog.cpp
batchevent.cpp
customqfilterdialog.cpp
inputoutput.cpp
statisticaloutlierremovaldialog.cpp
concavehullmeshdialog.cpp
concavehullpointclouddialog.cpp
smoothmeshlaplaciandialog.cpp
quadraticmeshdecimationdialog.cpp
linearmeshsubdivisiondialog.cpp
movingleastsquaresnormals.cpp
filterclusterdialog.cpp
progressivemorphologicalfilterdialog.cpp
smoothpolydatafilterdialog.cpp
meshquadraticclusteringdialog.cpp)

# Set Project Header Files
set (project_HEADERS
mainwindow.h
cloudprocessing.h
conditionalfilteringdialog.h
radiusoutlierremovaldialog.h
downsamplingdialog.h
mlsupsamplingdialog.h
calcnormalsdialog.h
trianglemeshingdialog.h
poissonsufracemeshingdialog.h
clouddata.h extractclustersdialog.h
regiongrowingsegmentationdialog.h
chunksplitdialog.h
circularremovaldialog.h
colorfilteringdialog.h
disjoincloudsdialog.h
customqprogressdialog.h
bilateralupsamplingdialog.h
batchprocessordialog.h
batchevent.h
customqfilterdialog.h
inputoutput.h
statisticaloutlierremovaldialog.h
concavehullmeshdialog.h
concavehullpointclouddialog.h
smoothmeshlaplaciandialog.h
quadraticmeshdecimationdialog.h
linearmeshsubdivisiondialog.h
movingleastsquaresnormals.h
filterclusterdialog.h
progressivemorphologicalfilterdialog.h
smoothpolydatafilterdialog.h
meshquadraticclusteringdialog.h)

# Set Project Form Files
set (project_FORMS mainwindow.ui
conditionalfilteringdialog.ui
radiusoutlierremovaldialog.ui
downsamplingdialog.ui
mlsupsamplingdialog.ui
calcnormalsdialog.ui
trianglemeshingdialog.ui
poissonsufracemeshingdialog.ui
extractclustersdialog.ui
regiongrowingsegmentationdialog.ui
chunksplitdialog.ui
circularremovaldialog.ui
colorfilteringdialog.ui
disjoincloudsdialog.ui
bilateralupsamplingdialog.ui
batchprocessordialog.ui
statisticaloutlierremovaldialog.ui
concavehullmeshdialog.ui
concavehullpointclouddialog.ui
smoothmeshlaplaciandialog.ui
quadraticmeshdecimationdialog.ui
linearmeshsubdivisiondialog.ui
filterclusterdialog.ui
progressivemorphologicalfilterdialog.ui
smoothpolydatafilterdialog.ui
meshquadraticclusteringdialog.ui)

# Generate MOC Code and UI Header
qt5_wrap_cpp( project_HEADERS_MOC ${project_HEADERS} )
qt5_wrap_ui( project_FORMS_HEADERS ${project_FORMS} )

# Add Executable
add_executable( ant-world  ${project_SOURCES} ${project_FORMS_HEADERS} ${project_HEADERS_MOC} )

# Additional Include Directories
include_directories( ${PCL_INCLUDE_DIRS} )
include_directories( ${CMAKE_CURRENT_SOURCE_DIR} )
include_directories( ${CMAKE_CURRENT_BINARY_DIR} )

# Load VTK Settings
include( ${VTK_USE_FILE} )

# Preprocessor Definitions
add_definitions( ${PCL_DEFINITIONS} )

# Additional Library Directories
link_directories( ${PCL_LIBRARY_DIRS} )

target_link_libraries (ant-world ${PCL_LIBRARIES} ${VTK_LIBRARIES} Qt5::Widgets)
