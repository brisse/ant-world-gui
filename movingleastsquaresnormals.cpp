#include "movingleastsquaresnormals.h"
using namespace pcl;

MovingLeastSquaresNormals::MovingLeastSquaresNormals() : MovingLeastSquares<PointXYZRGB, PointXYZRGB>()
{
}

void MovingLeastSquaresNormals::get_normals(pcl::PointCloud<Normal> &ret_normals)
{
    ret_normals = *this->normals_;
}
