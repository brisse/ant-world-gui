#include "downsamplingdialog.h"
#include "../build/debug/ui_downsamplingdialog.h"

DownSamplingDialog::DownSamplingDialog(QWidget *parent) :
    CustomQFilterDialog(parent),
    ui(new Ui::DownSamplingDialog)
{
    ui->setupUi(this);
}

DownSamplingDialog::~DownSamplingDialog()
{
    delete ui;
}

void DownSamplingDialog::on_pushButton_Filter_clicked()
{
    pcl::PointCloud<pcl::PointXYZRGB> ret_cloud;

    double leaf_x = ui->doubleSpinBox_X->value();
    double leaf_y = ui->doubleSpinBox_Y->value();
    double leaf_z = ui->doubleSpinBox_Z->value();

    bool use_min_number_of_points = ui->checkBox_min_number_of_points->isChecked();
    int min_number_of_points = ui->spinBox_min_number_of_points->value();

    bool use_filter_limits = ui->checkBox_use_filter_limits->isChecked();
    double min_limit = ui->doubleSpinBox_min_limit->value();
    double max_limit = ui->doubleSpinBox_max_limit->value();

    bool individual_cluster = ui->checkBox_individual_cluster->isChecked();

    bool enforce_reduction = ui->checkBox_enforce_reduction->isChecked();
    int min_reduction_percentage = ui->spinBox_min_reduction_percentage->value();
    double reduction_increment = ui->doubleSpinBox_reduction_increment->value();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        Process::downsample_cloud_inplace(*cloud_it, leaf_x, leaf_y, leaf_z, use_min_number_of_points, min_number_of_points, use_filter_limits, min_limit, max_limit, individual_cluster,
                                          enforce_reduction, min_reduction_percentage, reduction_increment, this);
        emit send_status_message(QString("Down-Sampling done! New cloud size is " + QString::number((*cloud_it)->get_cloud_size())));
    }

    emit filter_button_pressed();
}

void DownSamplingDialog::on_pushButton_Close_clicked()
{
    this->hide();
}

void DownSamplingDialog::on_pushButton_batch_clicked()
{
    double leaf_x = ui->doubleSpinBox_X->value();
    double leaf_y = ui->doubleSpinBox_Y->value();
    double leaf_z = ui->doubleSpinBox_Z->value();

    bool use_min_number_of_points = ui->checkBox_min_number_of_points->isChecked();
    int min_number_of_points = ui->spinBox_min_number_of_points->value();

    bool use_filter_limits = ui->checkBox_use_filter_limits->isChecked();
    double min_limit = ui->doubleSpinBox_min_limit->value();
    double max_limit = ui->doubleSpinBox_max_limit->value();

    bool individual_cluster = ui->checkBox_individual_cluster->isChecked();

    bool enforce_reduction = ui->checkBox_enforce_reduction->isChecked();
    int min_reduction_percentage = ui->spinBox_min_reduction_percentage->value();
    double reduction_increment = ui->doubleSpinBox_reduction_increment->value();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        batch_processor->add_event(new DownSamplingEvent(*cloud_it, leaf_x, leaf_y, leaf_z, use_min_number_of_points, min_number_of_points, use_filter_limits, min_limit, max_limit, individual_cluster,
                                                         enforce_reduction, min_reduction_percentage, reduction_increment, batch_processor));
    }

    emit send_status_message(QString("Down sampling batch processing for " + QString::number(clouds.size()) + " clouds." ));
}
