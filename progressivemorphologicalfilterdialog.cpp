#include "progressivemorphologicalfilterdialog.h"
#include "../build/debug/ui_progressivemorphologicalfilterdialog.h"

ProgressiveMorphologicalFilterDialog::ProgressiveMorphologicalFilterDialog(QWidget *parent) :
    CustomQFilterDialog(parent),
    ui(new Ui::ProgressiveMorphologicalFilterDialog)
{
    ui->setupUi(this);
}

ProgressiveMorphologicalFilterDialog::~ProgressiveMorphologicalFilterDialog()
{
    delete ui;
}

void ProgressiveMorphologicalFilterDialog::on_pushButton_filter_clicked()
{
    bool individual_cluster = this->ui->checkBox_individual_cluster->isChecked();
    bool merge_resultant_cluster = this->ui->checkBox_merge_resultant_cluster->isChecked();
    int max_window_size = this->ui->spinBox_max_window_size->value();
    float slope = this->ui->doubleSpinBox_slope->value();
    float initial_distance = this->ui->doubleSpinBox_initial_distance->value();
    float max_distance = this->ui->doubleSpinBox_max_distance->value();
    float cell_size = this->ui->doubleSpinBox_cell_size->value();
    float base = this->ui->doubleSpinBox_base->value();
    bool exponential = this->ui->checkBox_exponential->isChecked();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
       Process::progressive_morphological_filter_inplace(*cloud_it, max_window_size, slope, initial_distance, max_distance,  cell_size, base, exponential, individual_cluster, merge_resultant_cluster, this);
       emit send_status_message(QString("Progressive morph. filter extracted. New number of clusters in first cloud: " + QString::number(clouds.at(0)->get_number_of_clusters())));
    }

    emit filter_button_pressed();
}

void ProgressiveMorphologicalFilterDialog::on_pushButton_batch_clicked()
{
    bool individual_cluster = this->ui->checkBox_individual_cluster->isChecked();
    bool merge_resultant_cluster = this->ui->checkBox_merge_resultant_cluster->isChecked();
    int max_window_size = this->ui->spinBox_max_window_size->value();
    float slope = this->ui->doubleSpinBox_slope->value();
    float initial_distance = this->ui->doubleSpinBox_initial_distance->value();
    float max_distance = this->ui->doubleSpinBox_max_distance->value();
    float cell_size = this->ui->doubleSpinBox_cell_size->value();
    float base = this->ui->doubleSpinBox_base->value();
    bool exponential = this->ui->checkBox_exponential->isChecked();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        batch_processor->add_event(new ProgressiveMorphologicalFilterEvent(*cloud_it, max_window_size, slope, initial_distance, max_distance, cell_size, base, exponential, individual_cluster, merge_resultant_cluster, batch_processor));
    }

}

void ProgressiveMorphologicalFilterDialog::on_pushButton_close_clicked()
{
    this->hide();
}
