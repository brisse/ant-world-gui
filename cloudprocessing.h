#ifndef CLOUDPROCESSING_H
#define CLOUDPROCESSING_H

#include <cmath>
#include <QString>
#include <QProgressDialog>
#include <QWidget>
#include <QTime>
//#include <QDir>
//#include <QTime>
//#include <QDate>
//#include <QFile>
//#include <QTextStream>

#include <pcl/point_types.h>
#include <pcl/common/common.h>
#include <pcl/ModelCoefficients.h>

#include <pcl/filters/passthrough.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/filters/statistical_outlier_removal.h>

#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>

#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/segmentation/region_growing.h>
#include <pcl/segmentation/region_growing_rgb.h>
#include <pcl/segmentation/progressive_morphological_filter.h>

#include <pcl/surface/concave_hull.h>
#include <pcl/surface/mls.h>
#include <pcl/surface/poisson.h>
#include <pcl/surface/gp3.h>
#include <pcl/surface/bilateral_upsampling.h>

#include <pcl/surface/vtk_smoothing/vtk_mesh_smoothing_laplacian.h>
#include <pcl/surface/vtk_smoothing/vtk_mesh_quadric_decimation.h>
#include <pcl/surface/vtk_smoothing/vtk_mesh_subdivision.h>
//#include <pcl/surface/vtk_smoothing/vtk_mesh_smoothing_windowed_sinc.h>

#include <pcl/features/normal_3d_omp.h>
#include <pcl/features/normal_3d.h>

#include <pcl/kdtree/kdtree.h>
#include <pcl/kdtree/kdtree_flann.h>

//#include <pcl/io/pcd_io.h>
//#include <pcl/io/ply_io.h>

#include <pcl/search/kdtree.h>

#include <pcl/conversions.h>
#include <pcl/PCLPointCloud2.h>

#include <pcl/surface/vtk_smoothing/vtk_utils.h>
//#include <vtk-5.8/vtkSmartPointer.h>
#include <vtkSmartPointer.h>
#include <vtkPolyData.h>
#include <vtkCleanPolyData.h>
#include <vtkAppendFilter.h>
#include <vtkAppendPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkVersion.h>
#include <vtkDecimatePro.h>
#include <vtkQuadricClustering.h>

#include <vtkSmoothPolyDataFilter.h>
#include <vtkDelaunay2D.h>
#include <vtkPolyDataNormals.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>

#include <vtkMeshQuality.h>
#include <vtkDoubleArray.h>
#include <vtkCellData.h>

#include "clouddata.h"
#include "customqprogressdialog.h"
#include "movingleastsquaresnormals.h"

namespace Process
{
void convert_cloudXYZRGB_2_cloudXYZ(pcl::PointCloud<pcl::PointXYZRGB> const & cloudXYZRGB,
                                    pcl::PointCloud<pcl::PointXYZ> & cloudXYZ);

void extract_normals_from_pointnormals(pcl::PointCloud<pcl::PointNormal> const & cloudPointNormal,
                                       pcl::PointCloud<pcl::Normal> & ret_normals);

void extract_points_from_pointnormals(pcl::PointCloud<pcl::PointNormal> const & cloudPointNormal,
                                      pcl::PointCloud<pcl::PointXYZRGB> & ret_points);

void extract_clusters_from_cloud (CloudData* cur_cloud, std::vector<pcl::PointCloud<pcl::PointXYZRGB>::Ptr> & ret_cloud_clusters);
void extract_normal_clusters_from_cloud (CloudData* cur_cloud, std::vector<pcl::PointCloud<pcl::Normal>::Ptr> & ret_normal_clusters);
void extract_clusters_from_cloud_inverted(CloudData *cloud, CloudData & tmp_cloud_data);
//void extract_mesh_clusters_from_cloud (CloudData* cur_cloud, std::vector<pcl::PolygonMesh::Ptr> & ret_mesh_clusters);

float dist_x_y(pcl::PointXYZRGB & p1, pcl::PointXYZRGB & p2);

void merge_clusters(CloudData* cur_cloud, CloudData & output_cloud);

void merge_clouds(std::vector<CloudData *> cloud_vec, CloudData & ret_cloud_data);

void split_cloud_into_clusters(CloudData* cloud, std::vector<CloudData> &ret_clouds);

void filter_clusters(CloudData* cloud, const bool & use_max_allowed_height,
                     const double & max_allowed_height, const bool & use_min_allowed_height,
                     const double & min_allowed_height, const bool & use_max_allowed_cluster_size,
                     const int & max_allowed_cluster_size, const bool & use_min_allowed_cluster_size,
                     const int & min_allowed_cluster_size);

void concatinate_meshes(std::vector<CloudData*> cloud_vec, pcl::PolygonMesh &ret_mesh);

void concatinate_meshes(std::vector<pcl::PolygonMesh> meshes_vec, pcl::PolygonMesh &ret_mesh);


/**
 * @brief smooth_poly_data_filter_inplace
 *
 * Example code:
 * http://www.vtk.org/Wiki/VTK/Examples/Cxx/PolyData/SmoothPolyDataFilter
 * Docu:
 * http://www.vtk.org/doc/nightly/html/classvtkSmoothPolyDataFilter.html
 *
 *
 * @param input_cloud
 */
void smooth_poly_data_filter_inplace(CloudData* input_cloud,
                                     const double & convergence,            // Specify a convergence criterion for the iteration process.
                                                                            // Smaller numbers result in more smoothing iterations.

                                     const int & iterations,                // Specify the number of iterations for Laplacian smoothing,

                                     const double & relaxation_factor,      // Specify the relaxation factor for Laplacian smoothing.
                                                                            // As in all iterative methods, the stability of the process is sensitive to this parameter.
                                                                            // In general, small relaxation factors and large numbers of iterations are more
                                                                            // stable than larger relaxation factors and smaller numbers of iterations.

                                     const bool & use_feature_edge_smoothing,   // Turn on/off smoothing along sharp interior edges.
                                     const double & feature_angle,      // Specify the feature angle for sharp edge identification.
                                     const double & edge_angle,         // Specify the edge angle to control smoothing along edges (either interior or boundary).
                                     const bool & use_boundary_smoothing,
                                     QWidget* caller_dialog);

void mesh_decimate_pro_inplace(CloudData* input_cloud,
                               const double & target_reduction,
                               const bool & preserve_topology
//                               const double & feature_angle,
//                               const bool & splitting,
//                               const double & split_angle,
//                               const bool & pre_split_mesh,
//                               const double & max_error,
//                               const bool & accumulate_error,
                               );

void mesh_quadratic_clustering_inplace(CloudData* input_cloud,
                                       const bool & auto_adjust_number_of_devisions,            //true
                                       const int & number_of_x_divisions,
                                       const int & number_of_y_divisions,
                                       const int & number_of_z_divisions,
                                       const bool & use_feature_edges,                          // false
                                       const double & feature_points_angle,
                                       const bool &use_internal_triangles,                      // true
                                       QWidget* caller_dialog);

double get_mesh_quality(CloudData* input_cloud);


template<typename filterType, typename cloudType>
void opt_cluster_cloud_processing(CloudData* input_cloud, QWidget* caller_dialog, filterType & filter, const bool & individual_clusters,
                                  const int & min_cluster_size, cloudType & ret_cloud);

template<typename filterType, typename cloudType>
void opt_cluster_cloud_filtering(CloudData* input_cloud, QWidget* caller_dialog, filterType filter, const bool & individual_clusters,
                                  const int & min_cluster_size, cloudType & ret_cloud);

template<typename filterType, typename cloudType>
void opt_cluster_cloud_computing(CloudData* input_cloud, QWidget* caller_dialog, filterType filter, const bool & individual_clusters,
                                  const int & min_cluster_size, cloudType & ret_cloud);

template<typename filterType, typename cloudType>
void opt_cluster_cloud_extraction(CloudData* input_cloud, QWidget* caller_dialog, filterType filter, const bool & individual_clusters,
                                  const int & min_cluster_size, cloudType & ret_cloud);

template<typename filterType, typename cloudType>
void opt_cluster_cloud_reconstruction(CloudData* input_cloud, QWidget* caller_dialog, filterType & filter, const bool & individual_clusters,
                                      const int & min_cluster_size, cloudType & ret_cloud);

template<typename filterType>
void opt_cluster_mesh_reconstruction(CloudData* input_cloud, QWidget* caller_dialog, filterType filter, const bool & individual_clusters,
                                     const int & min_cluster_size, pcl::PolygonMesh & ret_mesh);

template<typename filterType>
void opt_cluster_mesh_reconstruction_no_normals(CloudData* input_cloud, QWidget* caller_dialog, filterType filter, const bool & individual_clusters,
                                                const int & min_cluster_size, pcl::PolygonMesh & ret_mesh);

// **********************************************************
// ******************** PCL Functions ***********************
// **********************************************************
/**
* @brief: calculate normals
* (http://pointclouds.org/documentation/tutorials/normal_estimation.php)
**/
void extract_normals_inplace (CloudData* input_cloud, const double search_radius, const int number_of_nearest_neighbors,
                              const bool use_search_radius, const bool set_view_point, const bool individual_clusters, QWidget* caller_dialog);

void circular_removal_inplace(CloudData* cloud, const float x_center, const float y_center, const float distance);

void circular_clustering_inplace(CloudData* input_cloud, const float x_center, const float y_center, const float distance);

void filter_color_inplace(CloudData* cloud, const int red_lower, const int red_upper, const int green_lower, const int green_upper,
                          const int blue_lower, const int blue_upper, const bool inside_red, const bool inside_green, const bool inside_blue);

void conditional_vertex_filter_inplace (CloudData* input_cloud, const double lower_limit, const double upper_limit, const std::string axis);

void split_square(CloudData * cloud, float x0, float y0, float x1, float y1, pcl::PointIndices & indices);
void check_subsplit(float x0, float y0, float x1, float y1, int max_chunk_size, CloudData* cloud, std::vector<pcl::PointIndices> & indices);
void recursive_split(float x0, float y0, float x1, float y1, int max_chunk_size, CloudData* cloud, std::vector<pcl::PointIndices> & indices);

void extract_chunks_inplace (CloudData* input_cloud, const int p_split, bool enforce_reduction, int max_chunk_size, QWidget *caller_dialog);

/**
* @brief: downsampling of a point cloud using a voxel grid filter
* (http://pointclouds.org/documentation/tutorials/voxel_grid.php)
**/
void downsample_cloud_inplace (CloudData* input_cloud, const float leaf_x, const float leaf_y, const float leaf_z, const bool use_min_number_of_points,
                               const int min_number_of_points, const bool use_filter_limits, const double min_limit,  const double max_limit,
                               const bool individual_clusters, const bool enforce_reduction, const int min_reduction_percentage, const double reduction_increment, QWidget* caller_dialog);

/**
* @brief: extract Euclidean clusters from a point cloud
* (http://www.pointclouds.org/documentation/tutorials/cluster_extraction.php)
**/
void extract_clusters_from_cloud_inplace(CloudData *input_clouds, const double cluster_tolerance, const bool use_min_cluster, const int min_cluster_size, const bool use_max_cluster, const int max_cluster_size);

/**
* @brief: Mean Least Squares Upsampling
* http://www.pointclouds.org/assets/icra2012/surface.pdf
**/
void mls_upsampling_inplace(CloudData* input_cloud, const double search_radius, const bool ponlynomial_fit, const int polynomial_order,
                            const double upsampling_radius, const double upsampling_step_size, const int upsampling_method,
                            const bool individual_clusters, QWidget* caller_dialog);

void mls_upsampling_inplace_1(CloudData* input_cloud,
                              const int sampling_method,
                              const bool use_search_radius,
                              const double search_radius,
                              const double sqrt_gaussian_neighor,
                              const bool polynomial_fit,
                              const int polynomial_order,
                              const double upsampling_radius,
                              const double upsampling_step_size,
                              const int desired_num_of_points_in_radius,
                              const int dilation_iterations,
                              const float dilation_voxel_size,
                              const bool calculate_normals,
                              const bool individual_cluster,
                              QWidget* caller_dialog);

void bilateral_upsampling_inplace(CloudData* input_cloud, const int window_size, const bool use_sigma_color, const double sigma_color, const bool use_sigma_depth,
                                  const double sigma_depth, const bool individual_cluster, QWidget* caller_dialog);

/**
* @brief: poisson surface reconstruction
* (http://docs.pointclouds.org/trunk/classpcl_1_1_poisson.html#a99a06a05bcdbf6ef273cd264a6846ee4
*  http://www.pointclouds.org/assets/icra2012/surface.pdf)
**/
void poisson_surface_reconstruction_inplace(CloudData *input_cloud, const bool use_max_tree_depth, const int maximum_tree_depth,
                                            const bool use_min_tree_depth, const int min_tree_depth,
                                            const bool use_point_weight, const double point_weight,
                                            const bool use_scale, const double scale,
                                            const bool use_solver_divide, const int solver_divide,
                                            const bool use_iso_divide, const int iso_divide,
                                            const bool use_sample_per_node, const double sample_per_node,
                                            const bool use_degree, const int degree,
                                            const bool set_confidence, const bool set_manifold, const bool set_polygons,
                                            const bool individual_cluster, QWidget *caller_dialog);

void extract_concave_hull_mesh_inplace(CloudData *input_cloud,
                                       const double alpha,
                                       const int dimensions,
                                       const bool keep_informations,
                                       const bool individual_cluster,
                                       QWidget *caller_dialog);

void extract_concave_hull_cloud_inplace(CloudData *input_cloud,
                                        const double alpha,
                                        const int dimensions,
                                        const bool keep_informations,
                                        const bool individual_cluster,
                                        QWidget *caller_dialog);

void smooth_mesh_laplacian(CloudData* input_cloud,
                           const int number_of_iterations,
                           const float convergence,
                           const float relaxation_factor,
                           const bool use_feature_edge_smoothing,
                           const float feature_angle,
                           const float edge_angle,
                           const bool use_boundary_smoothing,
                           QWidget *caller_dialog);

void smoothing_windowed_sinc(CloudData* input_cloud);

void quadratic_mesh_decimation(CloudData* input_cloud,
                               const float target_reduction_factor,
                               const int iterations,
                               QWidget *caller_dialog);

void mesh_subdivision(CloudData* input_cloud,
                      const int filter_type,
                      QWidget *caller_dialog);

/**
* @brief: raidus based outlier removal
* (http://pointclouds.org/documentation/tutorials/remove_outliers.php#remove-outliers)
**/
void remove_outliers_radius_inplace_1(CloudData* input_cloud, const double search_radius, const int min_neighbors_in_radius, const bool individual_clusters, QWidget *caller_dialog);

/**
* @brief: statistical outlier removal
* (http://pointclouds.org/documentation/tutorials/statistical_outlier.php)
**/
void remove_outliers_statistical_inplace(CloudData* input_cloud, const int number_of_neighbors, const double std_dev_multiplyer, const bool individual_clusters, QWidget *caller_dialog);


/**
* @brief: region growing segmentation
*	http://pointclouds.org/documentation/tutorials/region_growing_segmentation.php#region-growing-segmentation
**/
void region_growing_segmentation_inplace(CloudData* input_cloud, const bool individual_cluster, const int number_of_neighbors, const bool use_min_cluster_size,
                                         const int min_cluster_size, const bool use_max_cluster_size, const int max_cluster_size, const bool use_smoothness_threshold,
                                         const double smoothness_threshold, const bool use_curvature_threshold, const double curvature_threshold, const bool make_residual_test,
                                         const double residual_test, const bool merge_resultant_clusters, QWidget *caller_dialog);

/**
* @brief: Fast triangulation of an unordered point cloud
* (http://pointclouds.org/documentation/tutorials/greedy_projection.php#greedy-triangulation)
*
* @param: search_radius - sphere radius that is to be used for determining the k-nearest neighbors used for triangulating
* @param: mu - multiplier of the nearest neighbor distance to obtain the final search radius for each point
*               (this will make the algorithm adapt to different point densities in the cloud).
* @param: max_nearest_neighbors - maximum number of nearest neighbors to be searched for
* @param: max_surface_angle - don't consider points for triangulation if their normal deviates more than this value from the query point's normal
* @param: min_angle - minimum angle each triangle should have
* @param: max_angle - maximum angle each triangle can have
* @param: normal_consistency - flag if the input normals are oriented consistently
**/
void fast_mesh_triangulation_inplace(CloudData* input_cloud, const double search_radius, const double mu, const int max_nearest_neighbors, const double max_surface_angle,
                                     const double min_angle, const double max_angle, const bool normal_consistency, const bool indibidual_cluster, QWidget* caller_dialog);

/**
 * @brief progressive_morphological_filter_inplace
 * (http://pointclouds.org/documentation/tutorials/progressive_morphological_filtering.php)
 * @param input_cloud
 * @param max_window_size
 * @param slope
 * @param initial_distance
 * @param max_distance
 * @param individual_cluster
 * @param caller_dialog
 */
void progressive_morphological_filter_inplace(CloudData* input_cloud, const int max_window_size, const float slope, const float initial_distance,
                                              const float max_distance, const float cell_size, const float base, const bool exponential,
                                              const bool indibidual_cluster, const bool merge_resultant_cluster, QWidget *caller_dialog);

}

#endif // CLOUDPROCESSING_H
