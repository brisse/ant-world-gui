#ifndef CONDITIONALFILTERINGDIALOG_H
#define CONDITIONALFILTERINGDIALOG_H

#include "customqfilterdialog.h"

#include <pcl/point_types.h>
#include <pcl/common/common.h>

#include <pcl/filters/passthrough.h>

#include "cloudprocessing.h"
#include "clouddata.h"

namespace Ui {
class ConditionalFilteringDialog;
}

class ConditionalFilteringDialog : public CustomQFilterDialog
{
    Q_OBJECT

public:
    explicit ConditionalFilteringDialog(QWidget *parent = 0); //, Qt::WindowFlags f = Qt::Sheet
    ~ConditionalFilteringDialog();

private:
    Ui::ConditionalFilteringDialog *ui;


private slots:
    void on_pushButton_filter_clicked();
    void on_pushButtoncancel_clicked();
    void on_pushButton_batch_clicked();
};

#endif // CONDITIONALFILTERINGDIALOG_H
