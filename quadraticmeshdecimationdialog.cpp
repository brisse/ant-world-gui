#include "quadraticmeshdecimationdialog.h"
#include "../build/debug/ui_quadraticmeshdecimationdialog.h"

QuadraticMeshDecimationDialog::QuadraticMeshDecimationDialog(QWidget *parent) :
    CustomQFilterDialog(parent),
    ui(new Ui::QuadraticMeshDecimationDialog)
{
    ui->setupUi(this);
}

QuadraticMeshDecimationDialog::~QuadraticMeshDecimationDialog()
{
    delete ui;
}

void QuadraticMeshDecimationDialog::on_pushButton_filter_clicked()
{
    float reduction_factor = ui->doubleSpinBox_reduction_factor->value();
    int iterations = ui->spinBox_iterations->value();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        Process::quadratic_mesh_decimation(*cloud_it, reduction_factor, iterations, this);

        //        Process::mesh_decimate_pro_inplace(*cloud_it, 0.3, false);
//                  Process::mesh_quadratic_clustering_inplace(*cloud_it, false, 1,1,1,true,1.0,true,this);
        emit send_status_message(QString("Quadratic mesh decimation done! New mesh size is " + QString::number((*cloud_it)->get_mesh_size())));
    }

    emit filter_button_pressed();
}

void QuadraticMeshDecimationDialog::on_pushButton_close_clicked()
{
    this->hide();
}

void QuadraticMeshDecimationDialog::on_pushButton_batch_clicked()
{
    float reduction_factor = ui->doubleSpinBox_reduction_factor->value();
    int iterations = ui->spinBox_iterations->value();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        batch_processor->add_event(new QuadraticMeshDecimationEvent(*cloud_it, reduction_factor, iterations, batch_processor));
    }

    emit send_status_message(QString("Quadratic mesh decimation batch processing for " + QString::number(clouds.size()) + " clouds." ));

}
