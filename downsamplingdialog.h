#ifndef DOWNSAMPLINGDIALOG_H
#define DOWNSAMPLINGDIALOG_H

//#include <QDialog>
#include "customqfilterdialog.h"

#include <pcl/point_types.h>
#include <pcl/common/common.h>
#include <pcl/filters/voxel_grid.h>

#include "clouddata.h"
#include "cloudprocessing.h"
#include "batchprocessordialog.h"
#include "batchevent.h"

namespace Ui {
class DownSamplingDialog;
}

class DownSamplingDialog : public CustomQFilterDialog
{
    Q_OBJECT

public:
    explicit DownSamplingDialog(QWidget *parent = 0);
    ~DownSamplingDialog();

private slots:
    void on_pushButton_Filter_clicked();
    void on_pushButton_Close_clicked();
    void on_pushButton_batch_clicked();

private:
    Ui::DownSamplingDialog *ui;

};

#endif // DOWNSAMPLINGDIALOG_H
