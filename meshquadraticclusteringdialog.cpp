#include "meshquadraticclusteringdialog.h"
#include "../build/debug/ui_meshquadraticclusteringdialog.h"

MeshQuadraticClusteringDialog::MeshQuadraticClusteringDialog(QWidget *parent) :
    CustomQFilterDialog(parent),
    ui(new Ui::MeshQuadraticClusteringDialog)
{
    ui->setupUi(this);
}

MeshQuadraticClusteringDialog::~MeshQuadraticClusteringDialog()
{
    delete ui;
}

void MeshQuadraticClusteringDialog::on_pushButton_filter_clicked()
{
    bool auto_adjust_number_of_devisions = this->ui->checkBox_auto_adjust_number_of_devisions->isChecked();
    int x = this->ui->spinBox_x->value();
    int y = this->ui->spinBox_y->value();
    int z = this->ui->spinBox_z->value();
    bool use_feature_edges = this->ui->checkBox_use_feature_edges->isChecked();
    double feature_point_angles = this->ui->doubleSpinBox_feature_points_angle->value();
    bool use_internal_triangles = this->ui->checkBox_use_internal_triangles->isChecked();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
       Process::mesh_quadratic_clustering_inplace(*cloud_it,
                                                  auto_adjust_number_of_devisions,
                                                  x,
                                                  y,
                                                  z,
                                                  use_feature_edges,
                                                  feature_point_angles,
                                                  use_internal_triangles,
                                                  this);

       emit send_status_message(QString("Mesh quadratic clustering done! Number of polygons: " + QString::number((*cloud_it)->get_mesh_size())));
    }
    emit filter_button_pressed();

}

void MeshQuadraticClusteringDialog::on_pushButton_batch_clicked()
{
    bool auto_adjust_number_of_devisions = this->ui->checkBox_auto_adjust_number_of_devisions->isChecked();
    int x = this->ui->spinBox_x->value();
    int y = this->ui->spinBox_y->value();
    int z = this->ui->spinBox_z->value();
    bool use_feature_edges = this->ui->checkBox_use_feature_edges->isChecked();
    double feature_point_angles = this->ui->doubleSpinBox_feature_points_angle->value();
    bool use_internal_triangles = this->ui->checkBox_use_internal_triangles->isChecked();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        batch_processor->add_event(new MeshQuadraticClusteringEvent(*cloud_it,
                                                                    auto_adjust_number_of_devisions,
                                                                    x,
                                                                    y,
                                                                    z,
                                                                    use_feature_edges,
                                                                    feature_point_angles,
                                                                    use_internal_triangles,
                                                                    batch_processor));
    }

     emit send_status_message(QString("Mesh Quadratic Clustering batch processing for " + QString::number(clouds.size()) + " clouds." ));
}

void MeshQuadraticClusteringDialog::on_pushButton_close_clicked()
{
    this->hide();
}
