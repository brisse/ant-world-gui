#include "circularremovaldialog.h"
#include "../build/debug/ui_circularremovaldialog.h"

CircularRemovalDialog::CircularRemovalDialog(QWidget *parent) :
    CustomQFilterDialog(parent),
    ui(new Ui::CircularRemovalDialog)
{
    ui->setupUi(this);
}

CircularRemovalDialog::~CircularRemovalDialog()
{
    delete ui;
}

void CircularRemovalDialog::on_pushButton_filter_clicked()
{
    float center_x = ui->doubleSpinBox_center_x->value();
    float center_y = ui->doubleSpinBox_center_y->value();
    float distance = ui->doubleSpinBox_distance->value();
    bool cluster_cloud = ui->checkBox_cluster_cloud->isChecked();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        if (cluster_cloud)
        {
             Process::circular_clustering_inplace(*cloud_it, center_x, center_y, distance);
        }
        else
        {
            Process::circular_removal_inplace(*cloud_it, center_x, center_y, distance);
        }

        emit send_status_message(QString("Points outside radius removed! New size: " + QString::number((*cloud_it)->get_cloud_size())));
    }

    emit filter_button_pressed();

}

void CircularRemovalDialog::on_pushButton_close_clicked()
{
    this->hide();
}

void CircularRemovalDialog::on_pushButton_batch_clicked()
{
    float center_x = ui->doubleSpinBox_center_x->value();
    float center_y = ui->doubleSpinBox_center_y->value();
    float distance = ui->doubleSpinBox_distance->value();
    bool cluster_cloud = ui->checkBox_cluster_cloud->isChecked();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        if (cluster_cloud)
        {
            batch_processor->add_event(new CircularClusteringEvent(*cloud_it, center_x, center_y, distance));
        }
        else
        {
            batch_processor->add_event(new CircularRemovalEvent(*cloud_it, center_x, center_y, distance));
        }
    }

    emit send_status_message(QString("Circular removal batch processing for " + QString::number(clouds.size()) + " clouds." ));

}
