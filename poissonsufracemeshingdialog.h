#ifndef POISSONSUFRACEMESHINGDIALOG_H
#define POISSONSUFRACEMESHINGDIALOG_H

#include "customqfilterdialog.h"

#include <pcl/point_types.h>
#include <pcl/surface/poisson.h>

#include "cloudprocessing.h"
#include "clouddata.h"

namespace Ui {
class PoissonSufraceMeshingDialog;
}

class PoissonSufraceMeshingDialog : public CustomQFilterDialog
{
    Q_OBJECT

public:
    explicit PoissonSufraceMeshingDialog(QWidget *parent = 0);
    ~PoissonSufraceMeshingDialog();

private slots:
    void on_pushButton_close_clicked();
    void on_pushButton_filter_clicked();

    void on_pushButton_batch_clicked();

private:
    Ui::PoissonSufraceMeshingDialog *ui;
};

#endif // POISSONSUFRACEMESHINGDIALOG_H
