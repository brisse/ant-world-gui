#ifndef SMOOTHPOLYDATAFILTERDIALOG_H
#define SMOOTHPOLYDATAFILTERDIALOG_H

#include <pcl/point_types.h>
#include <pcl/surface/gp3.h>

#include "cloudprocessing.h"
#include "clouddata.h"

#include "customqfilterdialog.h"

namespace Ui {
class SmoothPolyDataFilterDialog;
}

class SmoothPolyDataFilterDialog : public CustomQFilterDialog
{
    Q_OBJECT

public:
    explicit SmoothPolyDataFilterDialog(QWidget *parent = 0);
    ~SmoothPolyDataFilterDialog();

private slots:
    void on_pushButton_filter_clicked();
    void on_pushButton_batch_clicked();
    void on_pushButton_close_clicked();

private:
    Ui::SmoothPolyDataFilterDialog *ui;
};

#endif // SMOOTHPOLYDATAFILTERDIALOG_H
