#ifndef QUADRATICMESHDECIMATIONDIALOG_H
#define QUADRATICMESHDECIMATIONDIALOG_H

#include "customqfilterdialog.h"
#include <QProgressDialog>

#include <pcl/point_types.h>
#include <pcl/common/common.h>

#include "clouddata.h"
#include "cloudprocessing.h"

namespace Ui {
class QuadraticMeshDecimationDialog;
}

class QuadraticMeshDecimationDialog : public CustomQFilterDialog
{
    Q_OBJECT

public:
    explicit QuadraticMeshDecimationDialog(QWidget *parent = 0);
    ~QuadraticMeshDecimationDialog();

private slots:
    void on_pushButton_filter_clicked();
    void on_pushButton_close_clicked();

    void on_pushButton_batch_clicked();

private:
    Ui::QuadraticMeshDecimationDialog *ui;
};

#endif // QUADRATICMESHDECIMATIONDIALOG_H
