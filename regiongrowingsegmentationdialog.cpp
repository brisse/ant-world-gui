#include "regiongrowingsegmentationdialog.h"
#include "../build/debug/ui_regiongrowingsegmentationdialog.h"

RegionGrowingSegmentationDialog::RegionGrowingSegmentationDialog(QWidget *parent) :
    CustomQFilterDialog(parent),
    ui(new Ui::RegionGrowingSegmentationDialog)
{
    ui->setupUi(this);
}

RegionGrowingSegmentationDialog::~RegionGrowingSegmentationDialog()
{
    delete ui;
}

void RegionGrowingSegmentationDialog::on_pushButton_filter_clicked()
{
    bool individual_cluster = ui->checkBox_individual_cluster->isChecked();

    bool use_min_cluster_size = ui->checkBox_use_min_cluster_size->isChecked();
    int min_cluster_size = ui->spinBox_min_cluster_size->value();
    bool use_max_cluster_size = ui->checkBox_use_max_cluster_size->isChecked();
    int max_cluster_size = ui->spinBox_max_cluster_size->value();

    int number_of_neighbors = ui->spinBox_number_of_neighors->value();

    bool use_smoothness_threshold = ui->checkBox_use_smoothness_threshold->isChecked();
    double smoothness_threshold = ui->doubleSpinBox_smoothness_threshold->value() / 180.0 * M_PI;
    bool use_curvature_threshold = ui->checkBox_use_curvature_threshold->isChecked();
    double curvature_threshold = ui->doubleSpinBox_curvature_threshold->value();

    bool make_residual_test = ui->checkBox_make_residual_test->isChecked();
    double residual_test = ui->doubleSpinBox_residual_test->value();

    bool merge_resultant_clusters = ui->checkBox_merge_resultant_clusters->isChecked();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        Process::region_growing_segmentation_inplace(*cloud_it,
                                            individual_cluster,
                                            number_of_neighbors,
                                            use_min_cluster_size,
                                            min_cluster_size,
                                            use_max_cluster_size,
                                            max_cluster_size,
                                            use_smoothness_threshold,
                                            smoothness_threshold,
                                            use_curvature_threshold,
                                            curvature_threshold,
                                            make_residual_test,
                                            residual_test,
                                            merge_resultant_clusters,
                                            this);

        emit send_status_message(QString("Region growing seg. finished. Number of clusteres: " + QString::number((*cloud_it)->get_number_of_clusters())));
    }

    emit filter_button_pressed();
}

void RegionGrowingSegmentationDialog::on_pushButton_close_clicked()
{
    this->hide();
}

void RegionGrowingSegmentationDialog::on_pushButto_batch_clicked()
{
    bool individual_cluster = ui->checkBox_individual_cluster->isChecked();

    bool use_min_cluster_size = ui->checkBox_use_min_cluster_size->isChecked();
    int min_cluster_size = ui->spinBox_min_cluster_size->value();
    bool use_max_cluster_size = ui->checkBox_use_max_cluster_size->isChecked();
    int max_cluster_size = ui->spinBox_max_cluster_size->value();

    int number_of_neighbors = ui->spinBox_number_of_neighors->value();

    bool use_smoothness_threshold = ui->checkBox_use_smoothness_threshold->isChecked();
    double smoothness_threshold = ui->doubleSpinBox_smoothness_threshold->value() / 180.0 * M_PI;
    bool use_curvature_threshold = ui->checkBox_use_curvature_threshold->isChecked();
    double curvature_threshold = ui->doubleSpinBox_curvature_threshold->value();

    bool make_residual_test = ui->checkBox_make_residual_test->isChecked();
    double residual_test = ui->doubleSpinBox_residual_test->value();
    bool merge_resultant_clusters = ui->checkBox_merge_resultant_clusters->isChecked();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        batch_processor->add_event(new RegionGrowingSegmentationEvent(*cloud_it,
                                                                      individual_cluster,
                                                                      number_of_neighbors,
                                                                      use_min_cluster_size,
                                                                      min_cluster_size,
                                                                      use_max_cluster_size,
                                                                      max_cluster_size,
                                                                      use_smoothness_threshold,
                                                                      smoothness_threshold,
                                                                      use_curvature_threshold,
                                                                      curvature_threshold,
                                                                      make_residual_test,
                                                                      residual_test,
                                                                      merge_resultant_clusters,
                                                                      batch_processor));
    }

    emit send_status_message(QString("Region growing segmentation batch processing for " + QString::number(clouds.size()) + " clouds." ));
}
