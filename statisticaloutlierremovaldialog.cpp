#include "statisticaloutlierremovaldialog.h"
#include "../build/debug/ui_statisticaloutlierremovaldialog.h"

StatisticalOutlierRemovalDialog::StatisticalOutlierRemovalDialog(QWidget *parent) :
    CustomQFilterDialog(parent),
    ui(new Ui::StatisticalOutlierRemovalDialog)
{
    ui->setupUi(this);
}

StatisticalOutlierRemovalDialog::~StatisticalOutlierRemovalDialog()
{
    delete ui;
}

void StatisticalOutlierRemovalDialog::on_pushButton_filter_clicked()
{
    int number_of_neighbors = ui->spinBox_number_of_neighbors->value();
    double std_dev_multiplier = ui->doubleSpinBox_std_dev_multiplier->value();
    bool process_clusters = ui->checkBox_individual_cluster->isChecked();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        Process::remove_outliers_statistical_inplace(*cloud_it, number_of_neighbors, std_dev_multiplier, process_clusters, this);
        emit send_status_message(QString("Statistical outlier removal done! New cloud size is " + QString::number((*cloud_it)->get_cloud_size())));
    }

    emit filter_button_pressed();
}

void StatisticalOutlierRemovalDialog::on_pushButton_batch_clicked()
{
    int number_of_neighbors = ui->spinBox_number_of_neighbors->value();
    double std_dev_multiplier = ui->doubleSpinBox_std_dev_multiplier->value();
    bool process_clusters = ui->checkBox_individual_cluster->isChecked();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        batch_processor->add_event(new StatisticalOutlierRemovalEvent(*cloud_it, number_of_neighbors, std_dev_multiplier, process_clusters, batch_processor));
    }

    emit send_status_message(QString("Statistical outlier removal batch processing for " + QString::number(clouds.size()) + " clouds." ));
}

void StatisticalOutlierRemovalDialog::on_pushButton_close_clicked()
{
    this->hide();
}
