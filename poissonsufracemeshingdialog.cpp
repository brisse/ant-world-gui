#include "poissonsufracemeshingdialog.h"
#include "../build/debug/ui_poissonsufracemeshingdialog.h"

PoissonSufraceMeshingDialog::PoissonSufraceMeshingDialog(QWidget *parent) :
    CustomQFilterDialog(parent),
    ui(new Ui::PoissonSufraceMeshingDialog)
{
    ui->setupUi(this);
}

PoissonSufraceMeshingDialog::~PoissonSufraceMeshingDialog()
{
    delete ui;
}

void PoissonSufraceMeshingDialog::on_pushButton_close_clicked()
{
    this->hide();
}

void PoissonSufraceMeshingDialog::on_pushButton_filter_clicked()
{
    bool use_max_tree_depth = ui->checkBox_max_tree_depth->isChecked();
    int maximum_tree_depth = ui->spinBox_max_tree_depth->value();
    bool use_min_tree_depth = ui->checkBox_min_tree_depth->isChecked();
    int min_tree_depth = ui->spinBox_min_tree_depth->value();
    bool use_point_weight = ui->checkBox_use_point_weight->isChecked();
    double point_weight = ui->doubleSpinBox_use_point_weight->value();
    bool use_scale = ui->checkBox_use_scale->isChecked();
    double scale = ui->doubleSpinBox_use_scale->value();
    bool use_solver_divide = ui->checkBox_use_solver_divide->isChecked();
    int solver_divide = ui->spinBox_solver_divide->value();
    bool use_iso_divide = ui->checkBox_use_iso_divide->isChecked();
    int iso_divide = ui->spinBox_iso_divide->value();
    bool use_sample_per_node = ui->checkBox_use_sample_per_node->isChecked();
    double sample_per_node = ui->doubleSpinBox_sample_per_node->value();
    bool use_degree = ui->checkBox_use_degree->isChecked();
    int degree = ui->spinBox_degree->value();

    bool set_confidence = ui->checkBox_set_confidence->isChecked();
    bool set_manifold = ui->checkBox_set_manifold->isChecked();
    bool output_polygons = ui->checkBox_set_output_polygons->isChecked();

    bool individual_cluster = ui->checkBox_individual_cluster->isChecked();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
//        Process::poisson_surface_reconstruction_inplace(*cloud_it, maximum_tree_depth);
        Process::poisson_surface_reconstruction_inplace(*cloud_it, use_max_tree_depth, maximum_tree_depth, use_min_tree_depth, min_tree_depth, use_point_weight, point_weight,
                                                        use_scale, scale, use_solver_divide, solver_divide, use_iso_divide, iso_divide, use_sample_per_node, sample_per_node,
                                                        use_degree, degree, set_confidence, set_manifold, output_polygons, individual_cluster, this);
        emit send_status_message(QString("Poisson mesh calculated! Number of polygons: " + QString::number((*cloud_it)->get_mesh_size())));
    }

    emit filter_button_pressed();
}

void PoissonSufraceMeshingDialog::on_pushButton_batch_clicked()
{
    bool use_max_tree_depth = ui->checkBox_max_tree_depth->isChecked();
    int maximum_tree_depth = ui->spinBox_max_tree_depth->value();
    bool use_min_tree_depth = ui->checkBox_min_tree_depth->isChecked();
    int min_tree_depth = ui->spinBox_min_tree_depth->value();
    bool use_point_weight = ui->checkBox_use_point_weight->isChecked();
    double point_weight = ui->doubleSpinBox_use_point_weight->value();
    bool use_scale = ui->checkBox_use_scale->isChecked();
    double scale = ui->doubleSpinBox_use_scale->value();
    bool use_solver_divide = ui->checkBox_use_solver_divide->isChecked();
    int solver_divide = ui->spinBox_solver_divide->value();
    bool use_iso_divide = ui->checkBox_use_iso_divide->isChecked();
    int iso_divide = ui->spinBox_iso_divide->value();
    bool use_sample_per_node = ui->checkBox_use_sample_per_node->isChecked();
    double sample_per_node = ui->doubleSpinBox_sample_per_node->value();
    bool use_degree = ui->checkBox_use_degree->isChecked();
    int degree = ui->spinBox_degree->value();

    bool set_confidence = ui->checkBox_set_confidence->isChecked();
    bool set_manifold = ui->checkBox_set_manifold->isChecked();
    bool output_polygons = ui->checkBox_set_output_polygons->isChecked();

    bool individual_cluster = ui->checkBox_individual_cluster->isChecked();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        batch_processor->add_event(new PoissonSurfaceMeshingEvent(*cloud_it, use_max_tree_depth, maximum_tree_depth, use_min_tree_depth, min_tree_depth, use_point_weight, point_weight,
                                    use_scale, scale, use_solver_divide, solver_divide, use_iso_divide, iso_divide, use_sample_per_node, sample_per_node,
                                    use_degree, degree, set_confidence, set_manifold, output_polygons, individual_cluster, batch_processor));
    }

    emit send_status_message(QString("Poisson meshing batch processing for " + QString::number(clouds.size()) + " clouds." ));
}
