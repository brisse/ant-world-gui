#ifndef PROGRESSIVEMORPHOLOGICALFILTERDIALOG_H
#define PROGRESSIVEMORPHOLOGICALFILTERDIALOG_H

#include "customqfilterdialog.h"

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

#include "clouddata.h"
#include "cloudprocessing.h"

namespace Ui {
class ProgressiveMorphologicalFilterDialog;
}

class ProgressiveMorphologicalFilterDialog : public CustomQFilterDialog
{
    Q_OBJECT

public:
    explicit ProgressiveMorphologicalFilterDialog(QWidget *parent = 0);
    ~ProgressiveMorphologicalFilterDialog();

private slots:
    void on_pushButton_filter_clicked();
    void on_pushButton_batch_clicked();
    void on_pushButton_close_clicked();

private:
    Ui::ProgressiveMorphologicalFilterDialog *ui;
};

#endif // PROGRESSIVEMORPHOLOGICALFILTERDIALOG_H
