#ifndef BATCHPROCESSORDIALOG_H
#define BATCHPROCESSORDIALOG_H

//#include <QDialog>
#include <QWidget>
#include <QApplication>
#include <QTime>
#include <iostream>
#include <typeinfo>

#include "batchevent.h"
#include "clouddata.h"
#include "cloudprocessing.h"

namespace Ui {
class BatchProcessorDialog;
}

class BatchProcessorDialog : public QDialog
{
    Q_OBJECT

public:
    explicit BatchProcessorDialog(QWidget *parent = 0);
    ~BatchProcessorDialog();

    void add_event(BatchEvent * event);

    void start_execution(void);
    void refresh_list(void);
    void update_dialog(void);

private slots:
    void on_pushButton_start_clicked();

    void on_pushButton_close_clicked();

private:
    Ui::BatchProcessorDialog *ui;

    std::vector<BatchEvent *> events;

    std::vector<QString> events_processed_list;
    std::vector<QString> events_unprocessed_list;

signals:
    void filter_button_pressed();
};

#endif // BATCHPROCESSORDIALOG_H
