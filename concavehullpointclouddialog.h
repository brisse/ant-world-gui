#ifndef CONCAVEHULLPOINTCLOUDDIALOG_H
#define CONCAVEHULLPOINTCLOUDDIALOG_H

#include <pcl/point_types.h>
#include <pcl/surface/gp3.h>

#include "cloudprocessing.h"
#include "clouddata.h"

#include "customqfilterdialog.h"

namespace Ui {
class ConcaveHullPointCloudDialog;
}

class ConcaveHullPointCloudDialog : public CustomQFilterDialog
{
    Q_OBJECT

public:
    explicit ConcaveHullPointCloudDialog(QWidget *parent = 0);
    ~ConcaveHullPointCloudDialog();

private slots:
    void on_pushButton_filter_clicked();
    void on_pushButton_clicked();
    void on_pushButton_close_clicked();

private:
    Ui::ConcaveHullPointCloudDialog *ui;
};

#endif // CONCAVEHULLPOINTCLOUDDIALOG_H
