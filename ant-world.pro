#-------------------------------------------------
#
# Project created by QtCreator 2015-06-25T16:59:21
#
#-------------------------------------------------

QT       += core widgets   # gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ant-world
#TEMPLATE = app

macx {
    INCLUDEPATH += /usr/local/include
    INCLUDEPATH += /usr/local/include/pcl-1.8
    INCLUDEPATH += /usr/local/include/eigen3
    CONFIG += link_pkgconfig
    #PKGCONFIG += eigen3

}

unix {
    INCLUDEPATH += /usr/include/pcl-1.7
}

SOURCES += main.cpp\
        mainwindow.cpp \
    cloudprocessing.cpp \
    conditionalfilteringdialog.cpp \
    radiusoutlierremovaldialog.cpp \
    downsamplingdialog.cpp \
    mlsupsamplingdialog.cpp \
    calcnormalsdialog.cpp \
    trianglemeshingdialog.cpp \
    poissonsufracemeshingdialog.cpp \
    clouddata.cpp \
    extractclustersdialog.cpp \
    regiongrowingsegmentationdialog.cpp \
    chunksplitdialog.cpp \
    circularremovaldialog.cpp \
    colorfilteringdialog.cpp \
    disjoincloudsdialog.cpp \
    customqprogressdialog.cpp \
    bilateralupsamplingdialog.cpp \
    batchevent.cpp \
    batchprocessordialog.cpp \
    customqfilterdialog.cpp \
    inputoutput.cpp \
    statisticaloutlierremovaldialog.cpp \
    concavehullmeshdialog.cpp \
    concavehullpointclouddialog.cpp \
    smoothmeshlaplaciandialog.cpp \
    quadraticmeshdecimationdialog.cpp \
    linearmeshsubdivisiondialog.cpp \
    movingleastsquaresnormals.cpp \
    filterclusterdialog.cpp \
    progressivemorphologicalfilterdialog.cpp \
    smoothpolydatafilterdialog.cpp \
    meshquadraticclusteringdialog.cpp

HEADERS  += mainwindow.h \
    cloudprocessing.h \
    conditionalfilteringdialog.h \
    radiusoutlierremovaldialog.h \
    downsamplingdialog.h \
    mlsupsamplingdialog.h \
    calcnormalsdialog.h \
    trianglemeshingdialog.h \
    poissonsufracemeshingdialog.h \
    clouddata.h \
    extractclustersdialog.h \
    regiongrowingsegmentationdialog.h \
    chunksplitdialog.h \
    circularremovaldialog.h \
    colorfilteringdialog.h \
    disjoincloudsdialog.h \
    customqprogressdialog.h \
    bilateralupsamplingdialog.h \
    batchevent.h \
    batchprocessordialog.h \
    customqfilterdialog.h \
    inputoutput.h \
    statisticaloutlierremovaldialog.h \
    concavehullmeshdialog.h \
    concavehullpointclouddialog.h \
    smoothmeshlaplaciandialog.h \
    quadraticmeshdecimationdialog.h \
    linearmeshsubdivisiondialog.h \
    movingleastsquaresnormals.h \
    filterclusterdialog.h \
    progressivemorphologicalfilterdialog.h \
    smoothpolydatafilterdialog.h \
    meshquadraticclusteringdialog.h

FORMS    += mainwindow.ui \
    conditionalfilteringdialog.ui \
    radiusoutlierremovaldialog.ui \
    downsamplingdialog.ui \
    mlsupsamplingdialog.ui \
    calcnormalsdialog.ui \
    trianglemeshingdialog.ui \
    poissonsufracemeshingdialog.ui \
    extractclustersdialog.ui \
    regiongrowingsegmentationdialog.ui \
    chunksplitdialog.ui \
    circularremovaldialog.ui \
    colorfilteringdialog.ui \
    disjoincloudsdialog.ui \
    bilateralupsamplingdialog.ui \
    batchprocessordialog.ui \
    statisticaloutlierremovaldialog.ui \
    concavehullmeshdialog.ui \
    concavehullpointclouddialog.ui \
    smoothmeshlaplaciandialog.ui \
    quadraticmeshdecimationdialog.ui \
    linearmeshsubdivisiondialog.ui \
    filterclusterdialog.ui \
    progressivemorphologicalfilterdialog.ui \
    smoothpolydatafilterdialog.ui \
    meshquadraticclusteringdialog.ui

OTHER_FILES += \
    CMakeLists.txt
