#ifndef CALCNORMALSDIALOG_H
#define CALCNORMALSDIALOG_H

#include "customqfilterdialog.h"

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>

#include "clouddata.h"
#include "cloudprocessing.h"
#include "batchprocessordialog.h"
#include "batchevent.h"

namespace Ui {
class CalcNormalsDialog;
}

class CalcNormalsDialog : public CustomQFilterDialog
{
    Q_OBJECT

public:
    explicit CalcNormalsDialog(QWidget *parent = 0);
    ~CalcNormalsDialog();

private:
    Ui::CalcNormalsDialog *ui;

private slots:
    void on_pushButton_filter_clicked();
    void on_pushButton_close_clicked();
    void on_pushButton_batch_clicked();
};

#endif // CALCNORMALSDIALOG_H
