#ifndef TRIANGLEMESHINGDIALOG_H
#define TRIANGLEMESHINGDIALOG_H

#include "customqfilterdialog.h"

#include <pcl/point_types.h>
#include <pcl/surface/gp3.h>

#include "cloudprocessing.h"
#include "clouddata.h"

namespace Ui {
class TriangleMeshingDialog;
}

class TriangleMeshingDialog : public CustomQFilterDialog
{
    Q_OBJECT

public:
    explicit TriangleMeshingDialog(QWidget *parent = 0);
    ~TriangleMeshingDialog();


private:
    Ui::TriangleMeshingDialog *ui;

private slots:
    void on_pushButton_close_clicked();
    void on_pushButton_filter_clicked();
    void on_pushButton_batch_clicked();
};

#endif // TRIANGLEMESHINGDIALOG_H
