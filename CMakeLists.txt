# NOTE: This CMake file only works if the below mentioned libraries are linked
# To link the version type: 'brew switch <library-name> <version-number>'

# LINKED LIBRARIES:
# boost 1.58.0
# pcl 1.7.2
# vtk 6.3.0
# qt 4.8.7_2     # 'brew link --force qt' (after 'brew stitch qt 4.8.7_2' to link qt to /usr/local/lib)
# flann 1.8.4_1 (linked by pcl 1.7)
# hdf5 1.8.16_1

# jpeg 8d

# Test Cmake Version
cmake_minimum_required( VERSION 3.6 )

# Create Project
project( ant-world )

# Find PCL Package
# set ( PCL_ROOT "/usr/local/Cellar/pcl/1.8.0_7")
find_package( PCL 1.7.2 REQUIRED )

# Find VTK Package
# set( VTK_DIR "/usr/local/Cellar/vtk/7.1.1_1" )
find_package( VTK REQUIRED )

# Find Qt Package
#find_package( Qt5 REQUIRED )
set(QT_VERSION_REQ "5.2")
find_package(Qt5Core ${QT_VERSION_REQ} REQUIRED)
find_package(Qt5Quick ${QT_VERSION_REQ} REQUIRED)
find_package(Qt5Widgets ${QT_VERSION_REQ} REQUIRED)

set (CMAKE_AUTOMOC ON)
set (CMAKE_INCLUDE_CURRENT_DIR ON)


# Suppress some warnings
set(CMAKE_CXX_FLAGS -Wno-inconsistent-missing-override)

# Set Project Source Files
set (project_SOURCES
main.cpp
mainwindow.cpp
cloudprocessing.cpp
conditionalfilteringdialog.cpp
radiusoutlierremovaldialog.cpp
downsamplingdialog.cpp
mlsupsamplingdialog.cpp
calcnormalsdialog.cpp
trianglemeshingdialog.cpp
poissonsufracemeshingdialog.cpp
clouddata.cpp
extractclustersdialog.cpp
regiongrowingsegmentationdialog.cpp
chunksplitdialog.cpp
circularremovaldialog.cpp
colorfilteringdialog.cpp
disjoincloudsdialog.cpp
customqprogressdialog.cpp
bilateralupsamplingdialog.cpp
batchprocessordialog.cpp
batchevent.cpp
customqfilterdialog.cpp
inputoutput.cpp
statisticaloutlierremovaldialog.cpp
concavehullmeshdialog.cpp
concavehullpointclouddialog.cpp
smoothmeshlaplaciandialog.cpp
quadraticmeshdecimationdialog.cpp
linearmeshsubdivisiondialog.cpp
movingleastsquaresnormals.cpp
filterclusterdialog.cpp
progressivemorphologicalfilterdialog.cpp
smoothpolydatafilterdialog.cpp
meshquadraticclusteringdialog.cpp)

# Set Project Header Files
set (project_HEADERS
mainwindow.h
cloudprocessing.h
conditionalfilteringdialog.h
radiusoutlierremovaldialog.h
downsamplingdialog.h
mlsupsamplingdialog.h
calcnormalsdialog.h
trianglemeshingdialog.h
poissonsufracemeshingdialog.h
clouddata.h extractclustersdialog.h
regiongrowingsegmentationdialog.h
chunksplitdialog.h
circularremovaldialog.h
colorfilteringdialog.h
disjoincloudsdialog.h
customqprogressdialog.h
bilateralupsamplingdialog.h
batchprocessordialog.h
batchevent.h
customqfilterdialog.h
inputoutput.h
statisticaloutlierremovaldialog.h
concavehullmeshdialog.h
concavehullpointclouddialog.h
smoothmeshlaplaciandialog.h
quadraticmeshdecimationdialog.h
linearmeshsubdivisiondialog.h
movingleastsquaresnormals.h
filterclusterdialog.h
progressivemorphologicalfilterdialog.h
smoothpolydatafilterdialog.h
meshquadraticclusteringdialog.h)

# Set Project Form Files
set (project_FORMS mainwindow.ui
conditionalfilteringdialog.ui
radiusoutlierremovaldialog.ui
downsamplingdialog.ui
mlsupsamplingdialog.ui
calcnormalsdialog.ui
trianglemeshingdialog.ui
poissonsufracemeshingdialog.ui
extractclustersdialog.ui
regiongrowingsegmentationdialog.ui
chunksplitdialog.ui
circularremovaldialog.ui
colorfilteringdialog.ui
disjoincloudsdialog.ui
bilateralupsamplingdialog.ui
batchprocessordialog.ui
statisticaloutlierremovaldialog.ui
concavehullmeshdialog.ui
concavehullpointclouddialog.ui
smoothmeshlaplaciandialog.ui
quadraticmeshdecimationdialog.ui
linearmeshsubdivisiondialog.ui
filterclusterdialog.ui
progressivemorphologicalfilterdialog.ui
smoothpolydatafilterdialog.ui
meshquadraticclusteringdialog.ui)

# Generate MOC Code and UI Header
qt5_generate_moc( project_HEADERS_MOC ${project_HEADERS} )
qt5_wrap_ui( project_FORMS_HEADERS ${project_FORMS} )

# INCLUDE (${QT_USE_FILE})
# ADD_DEFINITIONS (${QT_DEFINITIONS})

# Add Executable
add_executable( ant-world  ${project_SOURCES} ${project_FORMS_HEADERS} ${project_HEADERS_MOC} )

# Additional Include Directories
include_directories( ${PCL_INCLUDE_DIRS} )
include_directories( ${CMAKE_CURRENT_SOURCE_DIR} )
include_directories( ${CMAKE_CURRENT_BINARY_DIR} )

# Load VTK Settings
include( ${VTK_USE_FILE} )

# Preprocessor Definitions
add_definitions( ${PCL_DEFINITIONS} )

# Additional Library Directories
link_directories( ${PCL_LIBRARY_DIRS} )

# target_link_libraries (ant-world ${QT_LIBRARIES} ${PCL_LIBRARIES} ${VTK_LIBRARIES} )
target_link_libraries(ant-world
    Qt5::Core
    Qt5::Quick
    Qt5::Widgets
    ${PCL_LIBRARIES}
    ${VTK_LIBRARIES}
)
