Habitat3D is an open source software tool to generate photorealistic meshes from registered 
point clouds of natural outdoor scenes. 
Details can be found in 

- Risse, B., Mangan, M., Stürzl, W., & Webb, B. (2018). Software to convert terrestrial LiDAR 
	scans of natural environments into photorealistic meshes. Environmental Modelling and Software, 
	99, 88–100. http://doi.org/10.1016/j.envsoft.2017.09.018

For more informations contact b.risse@uni-muenster.de