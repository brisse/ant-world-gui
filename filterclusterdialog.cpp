#include "filterclusterdialog.h"
#include "../build/debug/ui_filterclusterdialog.h"

FilterClusterDialog::FilterClusterDialog(QWidget *parent) :
    CustomQFilterDialog(parent),
    ui(new Ui::FilterClusterDialog)
{
    ui->setupUi(this);
}

FilterClusterDialog::~FilterClusterDialog()
{
    delete ui;
}

void FilterClusterDialog::on_pushButton_close_clicked()
{
    this->hide();
}

void FilterClusterDialog::on_pushButton_batch_clicked()
{
    bool use_max_allowed_height = ui->checkBox_max_allowed_height->isChecked();
    double max_allowed_height = ui->doubleSpinBox_max_allowed_height->value();
    bool use_min_allowed_height = ui->checkBox_min_allowed_height->isChecked();
    double min_allowed_height = ui->doubleSpinBox_min_allowed_height->value();
    bool use_max_allowed_cluster_size = ui->checkBox_use_max_allowed_cluster_size->isChecked();
    int max_allowed_cluster_size = ui->spinBox_max_allowed_cluster_size->value();
    bool use_min_allowed_cluster_size = ui->checkBox_use_min_allowed_cluster_size->isChecked();
    int min_allowed_cluster_size = ui->spinBox_min_allowed_cluster_size->value();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        batch_processor->add_event(new FilterClustersEvent(*cloud_it, use_max_allowed_height, max_allowed_height, use_min_allowed_height, min_allowed_height,
                                                           use_max_allowed_cluster_size, max_allowed_cluster_size, use_min_allowed_cluster_size,
                                                           min_allowed_cluster_size, batch_processor));
    }

    emit send_status_message(QString("Filter Clusters batch processing for " + QString::number(clouds.size()) + " clouds." ));
}

void FilterClusterDialog::on_pushButton_filter_clicked()
{
    bool use_max_allowed_height = ui->checkBox_max_allowed_height->isChecked();
    double max_allowed_height = ui->doubleSpinBox_max_allowed_height->value();
    bool use_min_allowed_height = ui->checkBox_min_allowed_height->isChecked();
    double min_allowed_height = ui->doubleSpinBox_min_allowed_height->value();
    bool use_max_allowed_cluster_size = ui->checkBox_use_max_allowed_cluster_size->isChecked();
    int max_allowed_cluster_size = ui->spinBox_max_allowed_cluster_size->value();
    bool use_min_allowed_cluster_size = ui->checkBox_use_min_allowed_cluster_size->isChecked();
    int min_allowed_cluster_size = ui->spinBox_min_allowed_cluster_size->value();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
       Process::filter_clusters(*cloud_it, use_max_allowed_height,max_allowed_height,use_min_allowed_height, min_allowed_height,
                                use_max_allowed_cluster_size, max_allowed_cluster_size, use_min_allowed_cluster_size, min_allowed_cluster_size);
       emit send_status_message(QString("Clusteres filtered. New number of clusters in first cloud: " + QString::number(clouds.at(0)->get_number_of_clusters())));
    }

    emit filter_button_pressed();
}
