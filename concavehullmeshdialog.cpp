#include "concavehullmeshdialog.h"
#include "../build/debug/ui_concavehullmeshdialog.h"

ConcaveHullMeshDialog::ConcaveHullMeshDialog(QWidget *parent) :
    CustomQFilterDialog(parent),
    ui(new Ui::ConcaveHullMeshDialog)
{
    ui->setupUi(this);
}

ConcaveHullMeshDialog::~ConcaveHullMeshDialog()
{
    delete ui;
}

void ConcaveHullMeshDialog::on_pushButton_filter_clicked()
{
    double alpha = this->ui->doubleSpinBox_alpha->value();
    int dimensions = this->ui->spinBox_dimensions->value();
    bool keep_informations = this->ui->checkBox_keep_informations->isChecked();
    bool individual_cluster = this->ui->checkBox_individual_cluster->isChecked();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        Process::extract_concave_hull_mesh_inplace(*cloud_it, alpha, dimensions, keep_informations, individual_cluster, this);
        emit send_status_message(QString("Concave hull meshing done! Number of polygons: " + QString::number((*cloud_it)->get_mesh_size())));
    }

    emit filter_button_pressed();
}

void ConcaveHullMeshDialog::on_pushButton_close_clicked()
{
    this->hide();
}

void ConcaveHullMeshDialog::on_pushButton_batch_clicked()
{
    double alpha = this->ui->doubleSpinBox_alpha->value();
    int dimensions = this->ui->spinBox_dimensions->value();
    bool keep_informations = this->ui->checkBox_keep_informations->isChecked();
    bool individual_cluster = this->ui->checkBox_individual_cluster->isChecked();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        batch_processor->add_event(new ConcaveHullMeshingEvent(*cloud_it, alpha, dimensions, keep_informations, individual_cluster, batch_processor));
    }

    emit send_status_message(QString("Concave hull meshing batch processing for " + QString::number(clouds.size()) + " clouds." ));

}
