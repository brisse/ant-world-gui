#ifndef LINEARMESHSUBDIVISIONDIALOG_H
#define LINEARMESHSUBDIVISIONDIALOG_H

#include "customqfilterdialog.h"
#include <QProgressDialog>

#include <pcl/point_types.h>
#include <pcl/common/common.h>

#include "clouddata.h"
#include "cloudprocessing.h"

namespace Ui {
class LinearMeshSubdivisionDialog;
}

class LinearMeshSubdivisionDialog : public CustomQFilterDialog
{
    Q_OBJECT

public:
    explicit LinearMeshSubdivisionDialog(QWidget *parent = 0);
    ~LinearMeshSubdivisionDialog();

private slots:
    void on_pushButton_filter_clicked();

    void on_pushButton_close_clicked();

    void on_pushButton_batch_clicked();

private:
    Ui::LinearMeshSubdivisionDialog *ui;
};

#endif // LINEARMESHSUBDIVISIONDIALOG_H
