#include "extractclustersdialog.h"
#include "../build/debug/ui_extractclustersdialog.h"

ExtractClustersDialog::ExtractClustersDialog(QWidget *parent) :
    CustomQFilterDialog(parent),
    ui(new Ui::ExtractClustersDialog)
{
    ui->setupUi(this);
}

ExtractClustersDialog::~ExtractClustersDialog()
{
    delete ui;
}

void ExtractClustersDialog::on_pushButton_filter_clicked()
{
    double cluster_tolerance = ui->doubleSpinBox_cluster_tolerance->value();
    int min_cluster_size = ui->spinBox_min_cluster_size->value();
    int max_cluster_size = ui->spinBox_max_cluster_size->value();
    bool use_min_cluster = ui->checkBox_use_min_cluster_size->isChecked();
    bool use_max_cluster = ui->checkBox_use_max_cluster_size->isChecked();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
       Process::extract_clusters_from_cloud_inplace(*cloud_it, cluster_tolerance, use_min_cluster, min_cluster_size, use_max_cluster, max_cluster_size);
       emit send_status_message(QString("Clusteres extracted. New number of clusters in first cloud: " + QString::number(clouds.at(0)->get_number_of_clusters())));
    }

    emit filter_button_pressed();
}

void ExtractClustersDialog::on_pushButton_close_clicked()
{
    this->hide();
}

void ExtractClustersDialog::on_pushButton_batch_clicked()
{
    double cluster_tolerance = ui->doubleSpinBox_cluster_tolerance->value();
    int min_cluster_size = ui->spinBox_min_cluster_size->value();
    int max_cluster_size = ui->spinBox_max_cluster_size->value();
    bool use_min_cluster = ui->checkBox_use_min_cluster_size->isChecked();
    bool use_max_cluster = ui->checkBox_use_max_cluster_size->isChecked();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        batch_processor->add_event(new ExtractClustersEvent(*cloud_it, cluster_tolerance, use_min_cluster, min_cluster_size, use_max_cluster, max_cluster_size));
    }

    emit send_status_message(QString("Upsampling batch processing for " + QString::number(clouds.size()) + " clouds." ));
}
