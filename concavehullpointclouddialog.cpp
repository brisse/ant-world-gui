#include "concavehullpointclouddialog.h"
#include "../build/debug/ui_concavehullpointclouddialog.h"

ConcaveHullPointCloudDialog::ConcaveHullPointCloudDialog(QWidget *parent) :
    CustomQFilterDialog(parent),
    ui(new Ui::ConcaveHullPointCloudDialog)
{
    ui->setupUi(this);
}

ConcaveHullPointCloudDialog::~ConcaveHullPointCloudDialog()
{
    delete ui;
}

void ConcaveHullPointCloudDialog::on_pushButton_filter_clicked()
{
    double alpha = this->ui->doubleSpinBox_alpha->value();
    int dimensions = this->ui->spinBox_dimensions->value();
    bool keep_informations = this->ui->checkBox_keep_informations->isChecked();
    bool individual_cluster = this->ui->checkBox_individual_cluster->isChecked();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        Process::extract_concave_hull_cloud_inplace(*cloud_it, alpha, dimensions, keep_informations, individual_cluster, this);
        emit send_status_message(QString("Concave hull cloud extraction done! Number of points: " + QString::number((*cloud_it)->get_cloud_size())));
    }

    emit filter_button_pressed();
}

void ConcaveHullPointCloudDialog::on_pushButton_clicked() // batch
{
    double alpha = this->ui->doubleSpinBox_alpha->value();
    int dimensions = this->ui->spinBox_dimensions->value();
    bool keep_informations = this->ui->checkBox_keep_informations->isChecked();
    bool individual_cluster = this->ui->checkBox_individual_cluster->isChecked();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        batch_processor->add_event(new ExtractConcaveHullCloudEvent(*cloud_it, alpha, dimensions, keep_informations, individual_cluster, batch_processor));
    }

    emit send_status_message(QString("Concave hull cloud batch processing for " + QString::number(clouds.size()) + " clouds." ));
}

void ConcaveHullPointCloudDialog::on_pushButton_close_clicked()
{
    this->hide();
}
