#include "bilateralupsamplingdialog.h"
#include "../build/debug/ui_bilateralupsamplingdialog.h"

BilateralUpsamplingDialog::BilateralUpsamplingDialog(QWidget *parent) :
    CustomQFilterDialog(parent),
    ui(new Ui::BilateralUpsamplingDialog)
{
    ui->setupUi(this);
}

BilateralUpsamplingDialog::~BilateralUpsamplingDialog()
{
    delete ui;
}

void BilateralUpsamplingDialog::on_pushButton_filter_clicked()
{
    int window_size = ui->spinBox_window_size->value();
    bool use_sigma_color = ui->checkBox_use_sigma_color->isChecked();
    double sigma_color = ui->doubleSpinBox_sigma_color->value();
    bool use_sigma_depth = ui->checkBox_use_sigma_depth->isChecked();
    double sigma_depth = ui->doubleSpinBox__sigma_depth->value();
    bool individual_cluster = ui->checkBox_individual_cluster->isChecked();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        Process::bilateral_upsampling_inplace(*cloud_it, window_size, use_sigma_color, sigma_color, use_sigma_depth, sigma_depth, individual_cluster, this);

        emit send_status_message(QString("Bilateral upsampling done! Number of points: " + QString::number((*cloud_it)->get_cloud_size())));
    }

    emit filter_button_pressed();
}

void BilateralUpsamplingDialog::on_pushButton_close_clicked()
{
    this->hide();
}
