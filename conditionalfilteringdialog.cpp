#include "conditionalfilteringdialog.h"
#include "../build/debug/ui_conditionalfilteringdialog.h"


ConditionalFilteringDialog::ConditionalFilteringDialog(QWidget *parent) :       //, Qt::WindowFlags f
    CustomQFilterDialog(parent),
    ui(new Ui::ConditionalFilteringDialog)
{
    ui->setupUi(this);

}

ConditionalFilteringDialog::~ConditionalFilteringDialog()
{
    delete ui;
}

void ConditionalFilteringDialog::on_pushButton_filter_clicked()
{
    double lower_limit = ui->doubleSpinBox_lower->value();
    double upper_limit = ui->doubleSpinBox_upper->value();
    std::string axis = ui->comboBox->currentText().toStdString();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        Process::conditional_vertex_filter_inplace(*cloud_it, lower_limit, upper_limit, axis);
        emit send_status_message(QString("Conditional filtering done! New cloud size is " + QString::number((*cloud_it)->get_cloud_size())));
    }

    emit filter_button_pressed();
}

void ConditionalFilteringDialog::on_pushButtoncancel_clicked()
{
    this->hide();
}

void ConditionalFilteringDialog::on_pushButton_batch_clicked()
{
    double lower_limit = ui->doubleSpinBox_lower->value();
    double upper_limit = ui->doubleSpinBox_upper->value();
    std::string axis = ui->comboBox->currentText().toStdString();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        batch_processor->add_event(new ConditionalFilteringEvent(*cloud_it, lower_limit, upper_limit, axis));
    }

    emit send_status_message(QString("Conditional filtering batch processing for " + QString::number(clouds.size()) + " clouds." ));
}
