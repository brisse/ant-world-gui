#include "clouddata.h"

CloudData::CloudData()
{
    original_cloud.reset(new pcl::PointCloud<pcl::PointXYZRGB>());
    cloud.reset(new pcl::PointCloud<pcl::PointXYZRGB>());
    normals.reset(new pcl::PointCloud<pcl::Normal>());
    mesh.reset(new pcl::PolygonMesh());
    cluster_indices.clear();

    this->original_cloud_set = false;
    this->cloud_set = false;
    this->normals_set = false;
    this->mesh_set = false;
    this->clusters_set = false;

    this->original_cloud_size = 0;
    this->cloud_size = 0;
    this->normals_size = 0;
    this->mesh_size = 0;
    this->number_of_clusters = 0;

    this->cloud_name = "";
    this->cloud_path = "";
}

CloudData::CloudData(pcl::PointCloud<pcl::PointXYZRGB>::Ptr initial_cloud, std::string initial_name, std::string initial_path)
{
    this->init(*initial_cloud, initial_name, initial_path);
}

void CloudData::init(pcl::PointCloud<pcl::PointXYZRGB> initial_cloud, std::string initial_name, std::string initial_path)
{
    this->set_original_cloud(initial_cloud);


    this->cloud_name = initial_name;
    this->cloud_path = initial_path;
}

pcl::PointIndices CloudData::get_cloud_point_indices()
{
    int number_of_points = this->cloud->points.size();
    pcl::PointIndices ret_indices;
    for (int i=0; i<number_of_points; ++i)
    {
        ret_indices.indices.push_back(i);
    }
    return ret_indices;
}

pcl::PointIndices::Ptr CloudData::get_cloud_point_indices_ptr()
{
    pcl::PointIndices ret_indices;
    ret_indices = this->get_cloud_point_indices();
    pcl::PointIndices::Ptr ret_indices_ptr = boost::make_shared<pcl::PointIndices>(ret_indices);

    return ret_indices_ptr;
}

void CloudData::set_original_cloud(pcl::PointCloud<pcl::PointXYZRGB> initial_cloud)
{
    original_cloud.reset(new pcl::PointCloud<pcl::PointXYZRGB>());
    cloud.reset(new pcl::PointCloud<pcl::PointXYZRGB>());
    normals.reset(new pcl::PointCloud<pcl::Normal>());
    mesh.reset(new pcl::PolygonMesh());
    cluster_indices.clear();

    *this->original_cloud = initial_cloud;
    this->original_cloud_set = true;
    this->original_cloud_size = initial_cloud.size();

    *this->cloud = initial_cloud;
    this->cloud_set = true;
    this->original_cloud_size = initial_cloud.size();

    this->normals_set = false;
    this->normals_size = 0;
    this->mesh_set = false;
    this->mesh_size = 0;

    this->clusters_set = false;
    this->number_of_clusters = 0;
}

void CloudData::set_cloud(pcl::PointCloud<pcl::PointXYZRGB> initial_cloud)
{
    cloud.reset(new pcl::PointCloud<pcl::PointXYZRGB>());
    normals.reset(new pcl::PointCloud<pcl::Normal>());
//    mesh.reset(new pcl::PolygonMesh());
    cluster_indices.clear();

    *this->cloud = initial_cloud;
    this->cloud_set = true;
    this->cloud_size = initial_cloud.size();

    this->normals_set = false;
    this->normals_size = 0;
//    this->mesh_set = false;
//    this->mesh_size = 0;

    this->clusters_set = false;
    this->number_of_clusters = 0;
}

void CloudData::set_normals(pcl::PointCloud<pcl::Normal> initial_normals)
{
    normals.reset(new pcl::PointCloud<pcl::Normal>());
//    mesh.reset(new pcl::PolygonMesh());

    *this->normals = initial_normals;
    this->normals_set = true;
    this->normals_size = initial_normals.size();

//    this->mesh_set = false;
//    this->mesh_size = 0;
}

void CloudData::set_mesh(pcl::PolygonMesh initial_mesh)
{
    mesh.reset(new pcl::PolygonMesh());

    *this->mesh = initial_mesh;
    this->mesh_set = true;
    this->mesh_size = initial_mesh.polygons.size();
}

void CloudData::set_cluster_indices(std::vector<pcl::PointIndices> initial_indices)
{
    cluster_indices.resize(initial_indices.size());
    this->cluster_indices = initial_indices;
    this->clusters_set = true;
    this->number_of_clusters = initial_indices.size();
}

void CloudData::set_cloud_name(std::string initial_name)
{
    this->cloud_name = initial_name;
}

void CloudData::set_cloud_path(std::string initial_path)
{
    this->cloud_path = initial_path;
}

void CloudData::reset_cloud()
{
    cloud.reset(new pcl::PointCloud<pcl::PointXYZRGB>());
    normals.reset(new pcl::PointCloud<pcl::Normal>());
    mesh.reset(new pcl::PolygonMesh());
    cluster_indices.clear();

    this->cloud = this->original_cloud;
    this->cloud_set = true;
    this->original_cloud_size = this->original_cloud->size();

    this->normals_set = false;
    this->normals_size = 0;
    this->mesh_set = false;
    this->mesh_size = 0;

    this->clusters_set = false;
    this->number_of_clusters = 0;
}
