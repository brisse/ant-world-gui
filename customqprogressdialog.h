#ifndef CUSTOMQPROGRESSDIALOG_H
#define CUSTOMQPROGRESSDIALOG_H

#include <QProgressDialog>
#include <QWidget>
#include <QString>

#include "clouddata.h"

class CustomQProgressDialog : public QProgressDialog
{
public:
    /**
     * @brief CustomQProgressDialog constructor for functions with the POSSIBILITY of individual cluster processing
     * @param input_cloud
     * @param individual_cluster
     * @param caller_dialog
     */
    CustomQProgressDialog(CloudData* input_cloud, const bool & individual_cluster, QWidget* caller_dialog);

    /**
     * @brief CustomQProgressDialog constructor for functions wihtout clusters (mode called "individual usage")
     * @param input_cloud
     * @param max_progress_value
     * @param caller_dialog
     */
    CustomQProgressDialog(CloudData* input_cloud, const int & max_progress_value, QWidget* caller_dialog);
    void update_progress(const int & value);
    void end_progress (void);

private:
    int max_progress_value;
    CloudData* cloud;
    bool individual_usage;
};

#endif // CUSTOMQPROGRESSDIALOG_H
