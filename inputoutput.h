#ifndef INPUTOUTPUT_H
#define INPUTOUTPUT_H

#include <pcl/point_types.h>
#include <pcl/common/common.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>

#include <QDir>
#include <QTime>
#include <QDate>
#include <QFile>
#include <QTextStream>

#include "clouddata.h"
#include "cloudprocessing.h"

namespace InputOutput
{

void save_clouds(std::vector<CloudData*> selected_clouds, QString cur_file_path, QString cloud_suffix);

void save_cloud(CloudData* cloud, QString cur_file_path, QString cloud_suffix);

void load_color_cloud(std::string path, CloudData & ret_cloud);

void load_cloud_data(QString cloudData_file, CloudData & ret_cloud);

}

#endif // INPUTOUTPUT_H
