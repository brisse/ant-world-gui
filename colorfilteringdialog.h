#ifndef COLORFILTERINGDIALOG_H
#define COLORFILTERINGDIALOG_H

#include "customqfilterdialog.h"

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

#include "clouddata.h"
#include "cloudprocessing.h"

namespace Ui {
class ColorFilteringDialog;
}

class ColorFilteringDialog : public CustomQFilterDialog
{
    Q_OBJECT

public:
    explicit ColorFilteringDialog(QWidget *parent = 0);
    ~ColorFilteringDialog();

private:
    Ui::ColorFilteringDialog *ui;

private slots:
    void on_pushButton_close_clicked();
    void on_pushButton_filter_clicked();
    void on_pushButton_batch_clicked();
};

#endif // COLORFILTERINGDIALOG_H
