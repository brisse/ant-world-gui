#ifndef CONCAVEHULLMESHDIALOG_H
#define CONCAVEHULLMESHDIALOG_H

#include <pcl/point_types.h>
#include <pcl/surface/gp3.h>

#include "cloudprocessing.h"
#include "clouddata.h"

#include "customqfilterdialog.h"

namespace Ui {
class ConcaveHullMeshDialog;
}

class ConcaveHullMeshDialog : public CustomQFilterDialog
{
    Q_OBJECT

public:
    explicit ConcaveHullMeshDialog(QWidget *parent = 0);
    ~ConcaveHullMeshDialog();

private slots:
    void on_pushButton_filter_clicked();
    void on_pushButton_close_clicked();

    void on_pushButton_batch_clicked();

private:
    Ui::ConcaveHullMeshDialog *ui;
};

#endif // CONCAVEHULLMESHDIALOG_H
