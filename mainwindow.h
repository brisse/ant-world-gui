#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#ifdef __APPLE__
#define _MAC_OS_ 1
#else
#define _MAC_OS_ 0
#endif

#include <QWidget>
#include <QMainWindow>
#include <QFileDialog>
#include <QTreeWidgetItem>
#include <QMessageBox>
#include <QDir>
#include <QStringList>
#include <QFile>
#include <QListView>
#include <QDirIterator>
#include <QColorDialog>
#include <iostream>
#include <cmath>

// Point Cloud Library
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/filters/passthrough.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/point_cloud_handlers.h>


// Visualization Toolkit (VTK)
#include <vtkRenderWindow.h>

#include "cloudprocessing.h"
#include "inputoutput.h"

#include "conditionalfilteringdialog.h"
#include "radiusoutlierremovaldialog.h"
#include "downsamplingdialog.h"
#include "mlsupsamplingdialog.h"
#include "calcnormalsdialog.h"
#include "trianglemeshingdialog.h"
#include "poissonsufracemeshingdialog.h"
#include "clouddata.h"
#include "extractclustersdialog.h"
#include "regiongrowingsegmentationdialog.h"
#include "chunksplitdialog.h"
#include "circularremovaldialog.h"
#include "colorfilteringdialog.h"
#include "disjoincloudsdialog.h"
#include "bilateralupsamplingdialog.h"
#include "statisticaloutlierremovaldialog.h"
#include "concavehullmeshdialog.h"
#include "concavehullpointclouddialog.h"
#include "smoothmeshlaplaciandialog.h"
#include "quadraticmeshdecimationdialog.h"
#include "linearmeshsubdivisiondialog.h"
#include "filterclusterdialog.h"
#include "progressivemorphologicalfilterdialog.h"
#include "smoothpolydatafilterdialog.h"
#include "meshquadraticclusteringdialog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void pointSizeValueChanged(int value);
    void refresh_cloud(void);
    void quitProgram(void);

protected:
    pcl::visualization::PCLVisualizer::Ptr viewer;

    // cloud pair stores the file name (string) and the pointer to the point cloud data class
    std::map<std::string, CloudData> clouds;

//    QString filename;
    QStringList filenames;
    unsigned int pointSize;

private slots:
    void show_message(QString message);

    void on_actionLoad_Point_Cloud_triggered();
    void on_actionReset_cloud_triggered();
    void on_actionConditional_Filtering_triggered();
    void on_actionRadius_based_Outlier_Removal_triggered();
    void on_actionDown_Sampling_triggered();
    void on_actionMLS_Up_Sampling_triggered();
    void on_checkBox_coordinate_system_clicked();
    void on_pushButton_reset_camera_clicked();
    void on_actionCalculate_Normals_triggered();
    void on_actionCalculate_Smooth_Normals_MLS_triggered();
    void on_actionTriangle_Meshing_triggered();
    void on_actionPoisson_Surface_Meshing_triggered();
    void on_treeWidget_itemChanged(QTreeWidgetItem *item, int column);
    void on_actionRemove_Clouds_triggered();
    void on_actionExtract_Clusters_triggered();
    void on_actionRegion_Growing_Segmentation_triggered();
    void on_actionSplit_Clouds_triggered();
    void on_actionSelect_Clouds_triggered();
    void on_actionSelect_All_Clouds_triggered();
    void on_actionMerge_Clouds_triggered();
    void on_actionSave_Selected_Clouds_triggered();
    void on_actionSplit_Cloud_Into_Chunks_triggered();
    void on_actionCircular_Point_Removal_triggered();
    void on_actionColor_Filtering_triggered();
    void on_actionDisjoin_Two_Clouds_triggered();
    void on_actionMerge_Cloud_Clusters_triggered();
    void on_actionSplit_Cloud_Clusters_Inverted_triggered();
    void on_actionBilateral_Upsampling_triggered();
    void on_actionBatch_Processor_triggered();
    void on_actionLoad_Cloud_Data_triggered();
    void on_actionAdd_Save_Clouds_triggered();
    void on_actionStatistical_Outlier_Removal_triggered();
    void on_checkBox_view_point_clouds_clicked(bool checked);
    void on_actionLoad_Multiple_Cloud_Data_triggered();
    void on_actionConcave_Hull_Meshing_triggered();
    void on_actionConcave_Hull_Cloud_Extraction_triggered();
    void on_actionLaplacian_Mesh_Smoothing_triggered();

    void on_actionQuadratic_Mesh_Decimation_triggered();

    void on_actionMesh_Subdivision_triggered();

    void on_pushButton_background_colour_clicked();

    void on_actionFilter_Clusters_triggered();

    void on_actionProgressive_Morphological_Filter_triggered();

    void on_actionSmooth_Poly_Data_Filter_triggered();

    void on_actionQuadratic_Clustering_Downsampling_triggered();

    void on_actionGet_Mesh_Quality_triggered();

private:
    Ui::MainWindow *ui;
    ConditionalFilteringDialog               *mConditionalFilteringDialog;
    RadiusOutlierRemovalDialog              *mRadiusOutlierRemovalDialog;
    DownSamplingDialog                      *mDownSamplingDialog;
    MLSUpsamplingDialog                     *mMLSUpsamplingDialog;
    CalcNormalsDialog                       *mCalcNormalsDialog;
    TriangleMeshingDialog                   *mTriangleMeshingDialog;
    PoissonSufraceMeshingDialog             *mPoissonSurfaceMesingDialog;
    ExtractClustersDialog                   *mExtractClustersDialog;
    RegionGrowingSegmentationDialog         *mRegionGrowingSegmentationDialog;
    ChunkSplitDialog                        *mChunkSplitDialog;
    CircularRemovalDialog                   *mCircularRemovalDialog;
    ColorFilteringDialog                    *mColorFilteringDialog;
    DisjoinCloudsDialog                     *mDisjoinCloudsDialog;
    BilateralUpsamplingDialog               *mBilateralUpsamplingDialog;
    StatisticalOutlierRemovalDialog         *mStatisticalOutlierRemovalDialog;
    ConcaveHullMeshDialog                   *mConcaveHullMeshDialog;
    ConcaveHullPointCloudDialog             *mConcaveHullPointCloudDialog;
    SmoothMeshLaplacianDialog               *mSmoothMeshLaplacianDialog;
    QuadraticMeshDecimationDialog           *mQuadraticMeshDecimationDialog;
    LinearMeshSubdivisionDialog             *mLinearMeshSubdivisionDialog;
    FilterClusterDialog                     *mFilterClusterDialog;
    ProgressiveMorphologicalFilterDialog    *mProgressiveMorphologicalFilterDialog;
    SmoothPolyDataFilterDialog              *mSmoothPolyDataFilterDialog;
    MeshQuadraticClusteringDialog           *mMeshQuadraticClusteringDialog;

    BatchProcessorDialog                    *mBatchProcessorDialog;

    void load_cloud(std::string path);
    void refresh_viewer(bool reset_camera);
    void add_cloud_item(QString cloud_name);
    std::vector<CloudData*> get_selected_clouds (void);
    std::vector<CloudData*> get_all_clouds (void);

    void init_dialog(CustomQFilterDialog *dialog);

    QColor background_colour;

    /**
    * @brief: function to view multiple point clouds in a single viewer (random colors for each cloud can be set by random_color_clouds)
    **/
    void view_point_clouds(const std::vector<pcl::PointCloud<pcl::PointXYZRGB>::Ptr> & cloud_vec,
                           const bool random_color_clouds);

};

#endif // MAINWINDOW_H
