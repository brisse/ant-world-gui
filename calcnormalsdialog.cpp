#include "calcnormalsdialog.h"
#include "../build/debug/ui_calcnormalsdialog.h"

CalcNormalsDialog::CalcNormalsDialog(QWidget *parent) :
    CustomQFilterDialog(parent),
    ui(new Ui::CalcNormalsDialog)
{
    ui->setupUi(this);
}

CalcNormalsDialog::~CalcNormalsDialog()
{
    delete ui;
}

void CalcNormalsDialog::on_pushButton_filter_clicked()
{
    double search_radius = ui->doubleSpinBox_search_radius->value();
    bool use_search_radius = ui->checkBox_use_search_radius->isChecked();
    int number_of_nearest_neighbors = ui->spinBox_number_of_neighbors->value();
    bool individual_cluster = ui->checkBox_individual_cluster->isChecked();
    bool set_view_point = ui->checkBox_set_view_point->isChecked();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        Process::extract_normals_inplace(*cloud_it, search_radius, number_of_nearest_neighbors, use_search_radius, set_view_point, individual_cluster, this);

        emit send_status_message(QString("Normals calculated! Number of normals: " + QString::number((*cloud_it)->get_normals_size())));
    }

    emit filter_button_pressed();
}

void CalcNormalsDialog::on_pushButton_close_clicked()
{
    this->hide();
}

void CalcNormalsDialog::on_pushButton_batch_clicked()
{
    double search_radius = ui->doubleSpinBox_search_radius->value();
    bool use_search_radius = ui->checkBox_use_search_radius->isChecked();
    int number_of_nearest_neighbors = ui->spinBox_number_of_neighbors->value();
    bool individual_cluster = ui->checkBox_individual_cluster->isChecked();
    bool set_view_point = ui->checkBox_set_view_point->isChecked();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        batch_processor->add_event(new CalcNormalsEvent(*cloud_it, search_radius, number_of_nearest_neighbors, use_search_radius, set_view_point, individual_cluster, batch_processor));
    }

    emit send_status_message(QString("Calculate normals batch processing for " + QString::number(clouds.size()) + " clouds." ));
}
