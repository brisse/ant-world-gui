#ifndef FILTERCLUSTERDIALOG_H
#define FILTERCLUSTERDIALOG_H

#include "customqfilterdialog.h"

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

#include <pcl/kdtree/kdtree.h>
#include <pcl/segmentation/extract_clusters.h>

#include "clouddata.h"
#include "cloudprocessing.h"

namespace Ui {
class FilterClusterDialog;
}

class FilterClusterDialog : public CustomQFilterDialog
{
    Q_OBJECT

public:
    explicit FilterClusterDialog(QWidget *parent = 0);
    ~FilterClusterDialog();

private slots:
    void on_pushButton_close_clicked();
    void on_pushButton_batch_clicked();
    void on_pushButton_filter_clicked();

private:
    Ui::FilterClusterDialog *ui;
};

#endif // FILTERCLUSTERDIALOG_H
