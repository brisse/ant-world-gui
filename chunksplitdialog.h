#ifndef CHUNKSPLITDIALOG_H
#define CHUNKSPLITDIALOG_H

#include "customqfilterdialog.h"
#include <QProgressDialog>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/passthrough.h>

#include "clouddata.h"
#include "cloudprocessing.h"

namespace Ui {
class ChunkSplitDialog;
}

class ChunkSplitDialog : public CustomQFilterDialog
{
    Q_OBJECT

public:
    explicit ChunkSplitDialog(QWidget *parent = 0);
    ~ChunkSplitDialog();


private slots:
    void on_pushButton_filter_clicked();

    void on_pushButton_clsoe_clicked();

    void on_pushButton_batch_clicked();

private:
    Ui::ChunkSplitDialog *ui;

};

#endif // CHUNKSPLITDIALOG_H
