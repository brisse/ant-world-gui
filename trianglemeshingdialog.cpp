#include "trianglemeshingdialog.h"
#include "../build/debug/ui_trianglemeshingdialog.h"

TriangleMeshingDialog::TriangleMeshingDialog(QWidget *parent) :
    CustomQFilterDialog(parent),
    ui(new Ui::TriangleMeshingDialog)
{
    ui->setupUi(this);
}

TriangleMeshingDialog::~TriangleMeshingDialog()
{
    delete ui;
}

void TriangleMeshingDialog::on_pushButton_close_clicked()
{
    this->hide();
}

void TriangleMeshingDialog::on_pushButton_filter_clicked()
{
    double search_radius = ui->doubleSpinBo_search_radius->value();
    double mu = ui->doubleSpinBox_mu->value();
    int max_nearest_neighbors = ui->spinBox_max_nearest_neighbors->value();
    double max_surface_angle = ui->doubleSpinBox_max_surface_angle->value() * M_PI/180;
    double min_angle = ui->doubleSpinBox_min_angle->value() * M_PI/180;
    double max_angle = ui->doubleSpinBox_max_angle->value() * M_PI/180;
    bool normal_consistency = ui->checkBox_normal_consistency->isChecked();
    bool individual_cluster = ui->checkBox_individual_cluster->isChecked();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        Process::fast_mesh_triangulation_inplace(*cloud_it, search_radius, mu, max_nearest_neighbors, max_surface_angle, min_angle, max_angle, normal_consistency, individual_cluster, this);
        emit send_status_message(QString("Triangle mesh calculated! Number of polygons: " + QString::number((*cloud_it)->get_mesh_size())));
    }

    emit filter_button_pressed();
}

void TriangleMeshingDialog::on_pushButton_batch_clicked()
{
    double search_radius = ui->doubleSpinBo_search_radius->value();
    double mu = ui->doubleSpinBox_mu->value();
    int max_nearest_neighbors = ui->spinBox_max_nearest_neighbors->value();
    double max_surface_angle = ui->doubleSpinBox_max_surface_angle->value() * M_PI/180;
    double min_angle = ui->doubleSpinBox_min_angle->value() * M_PI/180;
    double max_angle = ui->doubleSpinBox_max_angle->value() * M_PI/180;
    bool normal_consistency = ui->checkBox_normal_consistency->isChecked();
    bool individual_cluster = ui->checkBox_individual_cluster->isChecked();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        batch_processor->add_event(new TriangleMeshingEvent(*cloud_it, search_radius, mu, max_nearest_neighbors, max_surface_angle, min_angle, max_angle, normal_consistency, individual_cluster, batch_processor));
    }

    emit send_status_message(QString("Triangle meshing batch processing for " + QString::number(clouds.size()) + " clouds." ));

}
