#ifndef DISJOINCLOUDSDIALOG_H
#define DISJOINCLOUDSDIALOG_H

#include "customqfilterdialog.h"

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>

#include "clouddata.h"

namespace Ui {
class DisjoinCloudsDialog;
}

class DisjoinCloudsDialog : public CustomQFilterDialog
{
    Q_OBJECT

public:
    explicit DisjoinCloudsDialog(QWidget *parent = 0);
    ~DisjoinCloudsDialog();
    void setClouds(std::vector<CloudData *> &input_clouds);

private slots:
    void on_pushButton_filter_clicked();
    void on_pushButton_close_clicked();


private:
    Ui::DisjoinCloudsDialog *ui;

    void disjoin_clouds_inplace(CloudData *full_cloud, CloudData *exclusion_cloud);
};

#endif // DISJOINCLOUDSDIALOG_H
