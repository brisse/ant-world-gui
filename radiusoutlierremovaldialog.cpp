#include "radiusoutlierremovaldialog.h"
#include "../build/debug/ui_radiusoutlierremovaldialog.h"

RadiusOutlierRemovalDialog::RadiusOutlierRemovalDialog(QWidget *parent) :
    CustomQFilterDialog(parent),
    ui(new Ui::RadiusOutlierRemovalDialog)
{
    ui->setupUi(this);
}

RadiusOutlierRemovalDialog::~RadiusOutlierRemovalDialog()
{
    delete ui;
}

void RadiusOutlierRemovalDialog::on_pushButton_filter_clicked()
{
    double search_radius = ui->doubleSpinBox_search_radius->value();
    int min_neighbors_in_radius = ui->spinBox_min_neighbors->value();
    bool process_clusters = ui->checkBox_individual_cluster->isChecked();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        Process::remove_outliers_radius_inplace_1 (*cloud_it, search_radius, min_neighbors_in_radius, process_clusters, this);
        emit send_status_message(QString("Outlier removal done! New cloud size is " + QString::number((*cloud_it)->get_cloud_size())));
    }

    emit filter_button_pressed();
}

void RadiusOutlierRemovalDialog::on_pushButton_close_clicked()
{
     this->hide();
}

void RadiusOutlierRemovalDialog::on_pushButton_batch_clicked()
{
    double search_radius = ui->doubleSpinBox_search_radius->value();
    int min_neighbors_in_radius = ui->spinBox_min_neighbors->value();
    bool process_clusters = ui->checkBox_individual_cluster->isChecked();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        batch_processor->add_event(new RadiusOutlierRemovalEvent(*cloud_it, search_radius, min_neighbors_in_radius, process_clusters, batch_processor));
    }

    emit send_status_message(QString("Outlier removal batch processing for " + QString::number(clouds.size()) + " clouds." ));
}
