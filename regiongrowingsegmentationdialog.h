#ifndef REGIONGROWINGSEGMENTATIONDIALOG_H
#define REGIONGROWINGSEGMENTATIONDIALOG_H

#include "customqfilterdialog.h"
#include <QProgressDialog>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/segmentation/region_growing.h>
#include <pcl/segmentation/region_growing_rgb.h>
//#include <pcl/kdtree/kdtree.h>
//#include <pcl/search/search.h>
#include <pcl/search/kdtree.h>

#include "clouddata.h"
#include "cloudprocessing.h"

namespace Ui {
class RegionGrowingSegmentationDialog;
}

class RegionGrowingSegmentationDialog : public CustomQFilterDialog
{
    Q_OBJECT

public:
    explicit RegionGrowingSegmentationDialog(QWidget *parent = 0);
    ~RegionGrowingSegmentationDialog();

private slots:
    void on_pushButton_filter_clicked();
    void on_pushButton_close_clicked();
    void on_pushButto_batch_clicked();

private:
    Ui::RegionGrowingSegmentationDialog *ui;

};

#endif // REGIONGROWINGSEGMENTATIONDIALOG_H
