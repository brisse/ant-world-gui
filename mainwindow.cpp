#include "mainwindow.h"
#include "../build/debug/ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->mConditionalFilteringDialog = new ConditionalFilteringDialog(this);
    this->mRadiusOutlierRemovalDialog = new RadiusOutlierRemovalDialog(this);
    this->mDownSamplingDialog         = new DownSamplingDialog(this);
    this->mMLSUpsamplingDialog        = new MLSUpsamplingDialog(this);
    this->mCalcNormalsDialog          = new CalcNormalsDialog(this);
    this->mTriangleMeshingDialog      = new TriangleMeshingDialog(this);
    this->mPoissonSurfaceMesingDialog = new PoissonSufraceMeshingDialog(this);
    this->mExtractClustersDialog       = new ExtractClustersDialog(this);
    this->mRegionGrowingSegmentationDialog = new RegionGrowingSegmentationDialog(this);
    this->mChunkSplitDialog             = new ChunkSplitDialog(this);
    this->mCircularRemovalDialog        = new CircularRemovalDialog(this);
    this->mColorFilteringDialog         = new ColorFilteringDialog(this);
    this->mDisjoinCloudsDialog          = new DisjoinCloudsDialog(this);
    this->mBilateralUpsamplingDialog    = new BilateralUpsamplingDialog(this);
    this->mStatisticalOutlierRemovalDialog = new StatisticalOutlierRemovalDialog(this);
    this->mConcaveHullMeshDialog        = new ConcaveHullMeshDialog(this);
    this->mConcaveHullPointCloudDialog        = new ConcaveHullPointCloudDialog(this);
    this->mSmoothMeshLaplacianDialog        = new SmoothMeshLaplacianDialog(this);
    this->mQuadraticMeshDecimationDialog    = new QuadraticMeshDecimationDialog(this);
    this->mLinearMeshSubdivisionDialog      = new LinearMeshSubdivisionDialog(this);
    this->mFilterClusterDialog              = new FilterClusterDialog(this);
    this->mProgressiveMorphologicalFilterDialog   = new ProgressiveMorphologicalFilterDialog(this);
    this->mSmoothPolyDataFilterDialog               = new SmoothPolyDataFilterDialog(this);
    this->mMeshQuadraticClusteringDialog            = new MeshQuadraticClusteringDialog(this);


    this->mBatchProcessorDialog         = new BatchProcessorDialog(this);
    connect(mBatchProcessorDialog, SIGNAL(filter_button_pressed()), this, SLOT(refresh_cloud()));
    mBatchProcessorDialog->setWindowModality(Qt::WindowModal);
    //    mBatchProcessorDialog->setModal(true);

    this->init_dialog(mConditionalFilteringDialog);
    this->init_dialog(mRadiusOutlierRemovalDialog);
    this->init_dialog(mDownSamplingDialog);
    this->init_dialog(mMLSUpsamplingDialog);
    this->init_dialog(mCalcNormalsDialog);
    this->init_dialog(mTriangleMeshingDialog);
    this->init_dialog(mPoissonSurfaceMesingDialog);
    this->init_dialog(mExtractClustersDialog);
    this->init_dialog(mRegionGrowingSegmentationDialog);
    this->init_dialog(mChunkSplitDialog);
    this->init_dialog(mCircularRemovalDialog);
    this->init_dialog(mColorFilteringDialog);
    this->init_dialog(mDisjoinCloudsDialog);
    this->init_dialog(mBilateralUpsamplingDialog);
    this->init_dialog(mStatisticalOutlierRemovalDialog);
    this->init_dialog(mConcaveHullMeshDialog);
    this->init_dialog(mConcaveHullPointCloudDialog);
    this->init_dialog(mSmoothMeshLaplacianDialog);
    this->init_dialog(mQuadraticMeshDecimationDialog);
    this->init_dialog(mLinearMeshSubdivisionDialog);
    this->init_dialog(mLinearMeshSubdivisionDialog);
    this->init_dialog(mFilterClusterDialog);
    this->init_dialog(mProgressiveMorphologicalFilterDialog);
    this->init_dialog(mSmoothPolyDataFilterDialog);
    this->init_dialog(mMeshQuadraticClusteringDialog);
//    this->init_dialog(mSmoothedNormalMLSDialog);

    connect(ui->spinBox, SIGNAL(valueChanged(int)), this, SLOT(pointSizeValueChanged(int)));
    connect(ui->actionQuit, SIGNAL(triggered()), this, SLOT(quitProgram()));


    // Set up the QVTK window
    viewer.reset (new pcl::visualization::PCLVisualizer ("viewer", false));
    ui->qvtkWidget->SetRenderWindow (viewer->getRenderWindow ());
    viewer->setupInteractor (ui->qvtkWidget->GetInteractor (), ui->qvtkWidget->GetRenderWindow ());

    this->background_colour = QColor(0,0,0);
    this->ui->toolBox->setCurrentIndex(0);

    //DEBUG *******
    std::string path_nr96 = "/Users/bena/Dropbox/PostDoc/3D World Data/all/Nr96_subsample.pcd";
    std::string path_clipping1 = "/Users/bena/Documents/Workspace/C++/ant-world/point-cloud-data/clipping_1.pcd";
    std::string single_thistle = "/Users/bena/Documents/Workspace/C++/ant-world/point-cloud-data/single_bush_point_cloud.pcd";
    std::string path_nr100_linux ="/media/brisse/Data/3D World Data/Seville_raw/pcd/Nr100_subsampled_filtered.pcd";
    std::string path_clipping1_linux ="/home/brisse/Documents/Workspace/C++/ant-world/point-cloud-data/clipping_1.pcd";
    std::string path_nr99 = "/Users/bena/Dropbox/PostDoc/3D World Data/Filtered/all/Nr99_subsampled_filtered.pcd";
    std::string single_thistle_linux="/media/brisse/Data/clouds/thistles/individual_thistles/Nr100_subsampled_filtered_inverted_merged_0_2015-11-04_19-40-18/cloud.pcd";

    if (_MAC_OS_)
        this->load_cloud(single_thistle);
    else
        this->load_cloud(path_nr100_linux);
    
    this->refresh_viewer(true);
    //END DEBUG ***

    ui->qvtkWidget->update ();
}

void MainWindow::init_dialog(CustomQFilterDialog *dialog)
{
    dialog->setWindowModality(Qt::WindowModal);
    dialog->setModal(true);
    connect(dialog, SIGNAL(filter_button_pressed()), this, SLOT(refresh_cloud()));
    connect(dialog, SIGNAL(send_status_message(QString)), this, SLOT(show_message(QString)));
    dialog->setBatchProcessor(this->mBatchProcessorDialog);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::pointSizeValueChanged(int value)
{
    this->refresh_viewer(false);
}

void MainWindow::quitProgram()
{
    this->close();
}

void MainWindow::show_message(QString message)
{
    ui->statusBar->showMessage(message);
}

void MainWindow::on_actionReset_cloud_triggered()
{
    std::vector<CloudData*> selected_clouds = get_selected_clouds();
    for (std::vector<CloudData*>::iterator cloud_it = selected_clouds.begin(); cloud_it != selected_clouds.end(); ++cloud_it)
    {
        (*cloud_it)->reset_cloud();
        show_message(QString("Cloud reseted! Number of points: " + QString::number((*cloud_it)->get_cloud_size())));
    }
    refresh_cloud();
}

void MainWindow::view_point_clouds(const std::vector<pcl::PointCloud<pcl::PointXYZRGB>::Ptr> & cloud_vec,
                                   const bool random_color_clouds)
{
    int j=0;
    int point_size = ui->spinBox->value();
    for (std::vector<pcl::PointCloud<pcl::PointXYZRGB>::Ptr>::const_iterator it = cloud_vec.begin();
         it != cloud_vec.end(); ++it)
    {
        std::stringstream ss;
        ss << "cloud_cluster_" << j;

        if (random_color_clouds)
        {
            int red = rand() % 256;
            int green = rand() % 256;
            int blue = rand() % 256;
            pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGB> rgb (*it, red, green, blue);
            viewer->addPointCloud<pcl::PointXYZRGB> (*it, rgb, ss.str());
        }
        else
        {
            pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(*it);
            viewer->addPointCloud<pcl::PointXYZRGB> (*it, rgb, ss.str());
        }
        viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size, ss.str());

        ++j;
    }
}

void MainWindow::refresh_viewer(bool reset_camera)
{
    if (ui->checkBox_view_point_clouds->isChecked())
    {
        if(reset_camera)
            viewer->resetCamera ();

        viewer->removeAllPointClouds();
        viewer->removeAllShapes();

        viewer->setBackgroundColor(background_colour.redF(),
                                   background_colour.greenF(),
                                   background_colour.blueF());

        // TEST SOMETHING
        //    viewer->addCube(0,7,-5,0,-1,1,1,1,1,"cube");
        // ***************

        QTreeWidgetItemIterator tree_iterator(ui->treeWidget);
        std::string current_cloud_name;
        while (*tree_iterator)
        {
            if ((*tree_iterator)->text(0).toStdString() != "points" &&
                    (*tree_iterator)->text(0).toStdString() != "clusters" &&
                    (*tree_iterator)->text(0).toStdString() != "normals" &&
                    (*tree_iterator)->text(0).toStdString() != "mesh")
            {
                current_cloud_name = (*tree_iterator)->text(0).toStdString();
                CloudData tmp_cloud = clouds.find(current_cloud_name)->second;

                (*tree_iterator)->setText(1, QString(QString::number(tmp_cloud.get_original_cloud_size())));

                // cloud check box set (in general)
                if ((*tree_iterator)->checkState(1) == 2)
                {
                    // check for points
                    ++tree_iterator;
                    (*tree_iterator)->setText(1, QString(QString::number(tmp_cloud.get_cloud_size())));
                    if ((*tree_iterator)->checkState(1) == 2)
                    {
                        // check for clusters
                        ++tree_iterator;
                        (*tree_iterator)->setText(1, QString(QString::number(tmp_cloud.get_number_of_clusters())));
                        if (tmp_cloud.has_clusters() && (*tree_iterator)->checkState(1) == 2)
                        {
                            std::vector<pcl::PointCloud<pcl::PointXYZRGB>::Ptr> cloud_vec;
                            Process::extract_clusters_from_cloud(&tmp_cloud, cloud_vec);
                            view_point_clouds(cloud_vec, true);
                        }
                        else
                        {
                            // since no clusters exist remove possible ticking of the cluster check box
                            (*tree_iterator)->setCheckState(1, Qt::Unchecked);
                            viewer->removePointCloud(current_cloud_name);
                            viewer->addPointCloud(tmp_cloud.get_cloud(), current_cloud_name);
                            int point_size = ui->spinBox->value();
                            viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, point_size, current_cloud_name);
                        }

                    }
                    else
                    {
                        // check for clusters (but do nothing!)
                        ++tree_iterator;
                    }

                    // check for normals
                    ++tree_iterator;
                    (*tree_iterator)->setText(1, QString(QString::number(tmp_cloud.get_normals_size())));
                    if (tmp_cloud.has_normals())
                    {
                        if ((*tree_iterator)->checkState(1) == 2)
                        {
                            std::stringstream ss;
                            ss << current_cloud_name << "_normals";
                            viewer->removePointCloud(ss.str());
                            viewer->addPointCloudNormals<pcl::PointXYZRGB, pcl::Normal>(tmp_cloud.get_cloud(), tmp_cloud.get_normals(), 10, 0.05, ss.str());
                        }
                    }
                    else
                    {
                        (*tree_iterator)->setCheckState(1, Qt::Unchecked);
                    }

                    // check for mesh
                    ++tree_iterator;
                    (*tree_iterator)->setText(1, QString(QString::number(tmp_cloud.get_mesh_size())));
                    if (tmp_cloud.has_mesh())
                    {
                        if ((*tree_iterator)->checkState(1) == 2)
                        {
                            std::stringstream ss;
                            ss << current_cloud_name << "_mesh";
                            viewer->removePolygonMesh(ss.str());
                            viewer->addPolygonMesh(*tmp_cloud.get_mesh(), ss.str());
                        }
                    }
                    else
                    {
                        (*tree_iterator)->setCheckState(1, Qt::Unchecked);
                    }
                }
            }
            ++tree_iterator;
        }


        if (ui->checkBox_coordinate_system->isChecked())
            viewer->addCoordinateSystem();
        else
            viewer->removeCoordinateSystem();

        ui->qvtkWidget->update ();
    }
    else
    {
        viewer->removeAllPointClouds();
        ui->qvtkWidget->update ();

    }

}

void MainWindow::refresh_cloud()
{
    this->refresh_viewer(false);
}

void MainWindow::add_cloud_item(QString cloud_name)
{
    QTreeWidgetItem *itm = new QTreeWidgetItem();
    itm->setText(0, QString(cloud_name));
    itm->setText(1, QString(""));
    itm->setFlags(itm->flags() | Qt::ItemIsUserCheckable);
    itm->setCheckState(1, Qt::Checked);

    QTreeWidgetItem *color_itm = new QTreeWidgetItem();
    color_itm->setText(0, QString("points"));
    color_itm->setText(1, QString(""));
    color_itm->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsEnabled);
    color_itm->setCheckState(1, Qt::Checked);
    itm->addChild(color_itm);

    QTreeWidgetItem *cluster_itm = new QTreeWidgetItem();
    cluster_itm->setText(0, QString("clusters"));
    cluster_itm->setText(1, QString(""));
    cluster_itm->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsEnabled);
    cluster_itm->setCheckState(1, Qt::Unchecked);
    itm->addChild(cluster_itm);

    QTreeWidgetItem *normal_itm = new QTreeWidgetItem();
    normal_itm->setText(0, QString("normals"));
    normal_itm->setText(1, QString(""));
    normal_itm->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsEnabled);
    normal_itm->setCheckState(1, Qt::Unchecked);
    itm->addChild(normal_itm);

    QTreeWidgetItem *mesh_itm = new QTreeWidgetItem();
    mesh_itm->setText(0, QString("mesh"));
    mesh_itm->setText(1, QString(""));
    mesh_itm->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsEnabled);
    mesh_itm->setCheckState(1, Qt::Unchecked);
    itm->addChild(mesh_itm);

    ui->treeWidget->addTopLevelItem(itm);
    ui->treeWidget->setCurrentItem(itm);
}

std::vector<CloudData *> MainWindow::get_selected_clouds()
{
    QList<QTreeWidgetItem*> selected_items = ui->treeWidget->selectedItems();
    std::vector<CloudData*> selected_clouds;
    selected_clouds.reserve(selected_items.size());
    for (QList<QTreeWidgetItem*>::iterator itm_it = selected_items.begin(); itm_it != selected_items.end(); ++itm_it)
    {
        std::string current_cloud_name = (*itm_it)->text(0).toStdString();

        CloudData* tmp_cloud = &clouds.find(current_cloud_name)->second;
        selected_clouds.push_back(tmp_cloud);
    }
    return selected_clouds;
}

std::vector<CloudData*> MainWindow::get_all_clouds(void)
{
    std::vector<CloudData*> ret_vec;
    ret_vec.reserve(clouds.size());
    for (std::map<std::string, CloudData>::iterator map_it = clouds.begin(); map_it != clouds.end(); ++map_it)
    {

        ret_vec.push_back(&(*map_it).second);
    }

    return ret_vec;
}

void MainWindow::on_checkBox_coordinate_system_clicked()
{
    this->refresh_viewer(false);
}

void MainWindow::on_pushButton_reset_camera_clicked()
{
    this->refresh_viewer(true);
}

void MainWindow::on_treeWidget_itemChanged(QTreeWidgetItem *item, int column)
{
    refresh_viewer(false);
}

void MainWindow::on_actionRemove_Clouds_triggered()
{
    std::vector<CloudData*> selected_clouds = get_selected_clouds();

    QMessageBox msgBox;
    msgBox.setText(QString("Delete " + QString::number(selected_clouds.size()) + " clouds?"));
    msgBox.setInformativeText("The clouds and all modifications will be lost!");
    msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Ok);
    int ret = msgBox.exec();
    switch (ret) {
    case QMessageBox::Cancel:
        break;
    case QMessageBox::Ok:
        // erase selected items
        for (std::vector<CloudData*>::iterator cloud_it = selected_clouds.begin(); cloud_it != selected_clouds.end(); ++cloud_it)
        {
            clouds.erase ((*cloud_it)->get_cloud_name());
        }

        // refresh tree widget
        ui->treeWidget->clear();
        for (std::map<std::string, CloudData>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
        {
            add_cloud_item(QString::fromStdString(cloud_it->first));
        }

        refresh_viewer(false);
        break;
    }

}

void MainWindow::on_actionSplit_Clouds_triggered()
{
    std::vector<CloudData*> selected_clouds = get_selected_clouds();

    for (std::vector<CloudData*>::iterator cloud_it = selected_clouds.begin(); cloud_it != selected_clouds.end(); ++cloud_it)
    {
        CloudData* cloud = *cloud_it;
        if (cloud->has_clusters())
        {
            std::vector<CloudData> ret_clouds;
            Process::split_cloud_into_clusters(cloud, ret_clouds);
            for (std::vector<CloudData>::iterator cloud_it = ret_clouds.begin(); cloud_it != ret_clouds.end(); ++cloud_it)
            {
                clouds[(*cloud_it).get_cloud_name()] = (*cloud_it);
                add_cloud_item(QString::fromStdString((*cloud_it).get_cloud_name()));
            }
        }
    }

    refresh_viewer(false);
}

void MainWindow::on_actionSplit_Cloud_Clusters_Inverted_triggered()
{
    std::vector<CloudData*> selected_clouds = get_selected_clouds();

    for (std::vector<CloudData*>::iterator cloud_it = selected_clouds.begin(); cloud_it != selected_clouds.end(); ++cloud_it)
    {
        CloudData* cloud = *cloud_it;
        if (cloud->has_clusters())
        {

            CloudData tmp_cloud_data;
            Process::extract_clusters_from_cloud_inverted(cloud, tmp_cloud_data);
            clouds[tmp_cloud_data.get_cloud_name()] = tmp_cloud_data;
            add_cloud_item(QString::fromStdString(tmp_cloud_data.get_cloud_name()));

        }
    }

    refresh_viewer(false);
}

void MainWindow::on_actionSelect_Clouds_triggered()
{
    QList<QTreeWidgetItem*> selected_items = ui->treeWidget->selectedItems();
    for (QList<QTreeWidgetItem*>::iterator itm_it = selected_items.begin(); itm_it != selected_items.end(); ++itm_it)
    {
        if ((*itm_it)->checkState(1) == 2)
        {
            (*itm_it)->setCheckState(1, Qt::Unchecked);
        }
        else
        {
            (*itm_it)->setCheckState(1, Qt::Checked);
        }
    }

    refresh_viewer(false);
}

void MainWindow::on_actionSelect_All_Clouds_triggered()
{
    QTreeWidgetItemIterator tree_iterator(ui->treeWidget);
    while (*tree_iterator)
    {
        if ((*tree_iterator)->text(0).toStdString() != "points" &&
                (*tree_iterator)->text(0).toStdString() != "clusters" &&
                (*tree_iterator)->text(0).toStdString() != "normals" &&
                (*tree_iterator)->text(0).toStdString() != "mesh")
        {
            if ((*tree_iterator)->isSelected())
            {
                (*tree_iterator)->setSelected(false);
            }
            else
            {
                (*tree_iterator)->setSelected(true);
            }
        }
        ++tree_iterator;
    }
}

void MainWindow::on_actionMerge_Clouds_triggered()
{
    std::vector<CloudData*> selected_clouds = get_selected_clouds();

    CloudData tmp_cloud_data;
    Process::merge_clouds(selected_clouds, tmp_cloud_data);

    std::cout << "merge: " << tmp_cloud_data.get_cloud_name() << " size " << tmp_cloud_data.get_cloud_size() << std::endl;

    clouds[tmp_cloud_data.get_cloud_name()] = tmp_cloud_data;
    add_cloud_item(QString::fromStdString(tmp_cloud_data.get_cloud_name()));

    refresh_viewer(false);
}

void MainWindow::on_actionMerge_Cloud_Clusters_triggered()
{
    std::vector<CloudData*> selected_clouds = this->get_selected_clouds();
    for (std::vector<CloudData*>::iterator cloud_it = selected_clouds.begin(); cloud_it != selected_clouds.end(); ++cloud_it)
    {
        Process::merge_clusters(*cloud_it, *(*cloud_it));
    }
    refresh_viewer(false);
}

void MainWindow::on_actionLoad_Point_Cloud_triggered()
{
    filenames = QFileDialog::getOpenFileNames(this, QString("Open PCD File..."), QString(), QString("PCD File (*.pcd)"));
    if (!filenames.empty())
    {
        for(QStringList::iterator qstr_it = filenames.begin(); qstr_it != filenames.end(); ++qstr_it)
        {
            QString filename = *qstr_it;
            this->load_cloud(filename.toStdString());
        }

        this->refresh_viewer(true);
    }
}

void MainWindow::load_cloud(std::string path)
{
    CloudData tmp_cloud_data;
    InputOutput::load_color_cloud(path, tmp_cloud_data);
    std::string cur_file_name = tmp_cloud_data.get_cloud_name();
    clouds[cur_file_name] = tmp_cloud_data;
    add_cloud_item(QString::fromStdString(cur_file_name));

    std::cout << "Loaded cloud: " << path << std::endl;
    show_message(QString("Cloud loaded! Number of points: " + QString::number(clouds[cur_file_name].get_cloud_size())));

}

void MainWindow::on_actionLoad_Cloud_Data_triggered()
{
    QString cloudData_file = QFileDialog::getOpenFileName(this, QString("Open PCD File..."), QString(), QString("PCD File (*.cdat)"));
    if (cloudData_file != "")
    {
        CloudData ret_cloud;
        InputOutput::load_cloud_data(cloudData_file, ret_cloud);

        std::string cur_file_name = ret_cloud.get_cloud_name();
        clouds[cur_file_name] = ret_cloud;
        add_cloud_item(QString::fromStdString(cur_file_name));

        show_message(QString("Cloud loaded! Number of points: " + QString::number(clouds[cur_file_name].get_cloud_size())));
        refresh_cloud();
    }
}

void MainWindow::on_actionLoad_Multiple_Cloud_Data_triggered()
{
    QFileDialog fd;
    fd.setFileMode(QFileDialog::DirectoryOnly);
    fd.setOption(QFileDialog::DontUseNativeDialog, true);
    QListView *l = fd.findChild<QListView*>("listView");
    if (l)
    {
        l->setSelectionMode(QAbstractItemView::MultiSelection);
    }
    QTreeView *t = fd.findChild<QTreeView*>();
    if (t)
    {
        t->setSelectionMode(QAbstractItemView::MultiSelection);
    }
    fd.exec();

    QStringList selected_folders = fd.selectedFiles();
    if (!selected_folders.empty())
    {
        QStringList all_cdat_files;
        for (QStringList::iterator it = selected_folders.begin(); it != selected_folders.end(); ++it)
        {
            QDirIterator dir_it(*it, QStringList() << "*.cdat", QDir::Files, QDirIterator::Subdirectories);
            while(dir_it.hasNext())
            {
                all_cdat_files.push_back(dir_it.next());
            }
        }

        for (QStringList::iterator it = all_cdat_files.begin(); it != all_cdat_files.end(); ++it)
        {
            std::cout << it->toStdString() << std::endl;
            CloudData ret_cloud;
            InputOutput::load_cloud_data(*it, ret_cloud);

            std::string cur_file_name = ret_cloud.get_cloud_name();
            clouds[cur_file_name] = ret_cloud;
            add_cloud_item(QString::fromStdString(cur_file_name));

            show_message(QString("Cloud loaded! Number of points: " + QString::number(clouds[cur_file_name].get_cloud_size())));
        }
        refresh_cloud();
    }
    else
        std::cout << "empty test" << std::endl;

}

void MainWindow::on_actionSave_Selected_Clouds_triggered()
{
    std::vector<CloudData*> selected_clouds = get_selected_clouds();

    QString filename = QFileDialog::getSaveFileName(this, "Save files...", "", ".pcd");

    QFileInfo file_info(filename);
    QString cur_file_name = file_info.fileName();
    QString cur_file_path = file_info.absolutePath();
    InputOutput::save_clouds(selected_clouds, cur_file_path, cur_file_name);
}

void MainWindow::on_actionAdd_Save_Clouds_triggered()
{
    std::vector<CloudData*> selected_clouds = get_selected_clouds();

    QString filename = QFileDialog::getSaveFileName(this, "Save files...", "", ".pcd");
    QFileInfo file_info(filename);
    QString cloud_suffix = file_info.fileName();
    QString cur_file_path = file_info.absolutePath();

    for (std::vector<CloudData*>::iterator cloud_it = selected_clouds.begin(); cloud_it != selected_clouds.end(); ++cloud_it)
    {
        mBatchProcessorDialog->add_event(new SaveCloudsEvent(*cloud_it, cur_file_path, cloud_suffix));
    }
    show_message(QString("Save clouds batch processing for " + QString::number(selected_clouds.size()) + " clouds." ));
}

// ******************************************************
//                    PC Cloud Processing
// ******************************************************

void MainWindow::on_actionConditional_Filtering_triggered()
{
    std::vector<CloudData*> selected_clouds = get_selected_clouds();
    if (!selected_clouds.empty())
    {
        mConditionalFilteringDialog->setClouds(selected_clouds);
        mConditionalFilteringDialog->show();
    }
}

void MainWindow::on_actionRadius_based_Outlier_Removal_triggered()
{
    std::vector<CloudData*> selected_clouds = get_selected_clouds();
    if (!selected_clouds.empty())
    {
        mRadiusOutlierRemovalDialog->setClouds(selected_clouds);
        mRadiusOutlierRemovalDialog->show();
    }
}

void MainWindow::on_actionDown_Sampling_triggered()
{
    std::vector<CloudData*> selected_clouds = get_selected_clouds();
    if (!selected_clouds.empty())
    {
        mDownSamplingDialog->setClouds(selected_clouds);
        mDownSamplingDialog->show();
    }
}

void MainWindow::on_actionMLS_Up_Sampling_triggered()
{
    std::vector<CloudData*> selected_clouds = get_selected_clouds();
    if (!selected_clouds.empty())
    {
        mMLSUpsamplingDialog->setClouds(selected_clouds);
        mMLSUpsamplingDialog->set_calc_normals(false);
        mMLSUpsamplingDialog->show();
    }
}

void MainWindow::on_actionBilateral_Upsampling_triggered()
{
    std::vector<CloudData*> selected_clouds = get_selected_clouds();
    if (!selected_clouds.empty())
    {
        mBilateralUpsamplingDialog->setClouds(selected_clouds);
        mBilateralUpsamplingDialog->show();
    }
}

void MainWindow::on_actionCalculate_Normals_triggered()
{
    std::vector<CloudData*> selected_clouds = get_selected_clouds();
    if (!selected_clouds.empty())
    {
        mCalcNormalsDialog->setClouds(selected_clouds);
        mCalcNormalsDialog->show();
    }
}

void MainWindow::on_actionCalculate_Smooth_Normals_MLS_triggered()
{
    std::vector<CloudData*> selected_clouds = get_selected_clouds();
    if (!selected_clouds.empty())
    {
        mMLSUpsamplingDialog->setClouds(selected_clouds);
        mMLSUpsamplingDialog->set_calc_normals(true);
        mMLSUpsamplingDialog->show();
    }
}

void MainWindow::on_actionTriangle_Meshing_triggered()
{
    std::vector<CloudData*> selected_clouds = get_selected_clouds();
    if (!selected_clouds.empty())
    {
        mTriangleMeshingDialog->setClouds(selected_clouds);
        mTriangleMeshingDialog->show();
    }
}

void MainWindow::on_actionPoisson_Surface_Meshing_triggered()
{
    std::vector<CloudData*> selected_clouds = get_selected_clouds();
    if (!selected_clouds.empty())
    {
        mPoissonSurfaceMesingDialog->setClouds(selected_clouds);
        mPoissonSurfaceMesingDialog->show();
    }
}

void MainWindow::on_actionExtract_Clusters_triggered()
{
    std::vector<CloudData*> selected_clouds = get_selected_clouds();
    if (!selected_clouds.empty())
    {
        mExtractClustersDialog->setClouds(selected_clouds);
        mExtractClustersDialog->show();
    }
}

void MainWindow::on_actionFilter_Clusters_triggered()
{
    std::vector<CloudData*> selected_clouds = get_selected_clouds();
    if (!selected_clouds.empty())
    {
        mFilterClusterDialog->setClouds(selected_clouds);
        mFilterClusterDialog->show();
    }
}

void MainWindow::on_actionRegion_Growing_Segmentation_triggered()
{
    std::vector<CloudData*> selected_clouds = get_selected_clouds();
    if (!selected_clouds.empty())
    {
        mRegionGrowingSegmentationDialog->setClouds(selected_clouds);
        mRegionGrowingSegmentationDialog->show();
    }
}

void MainWindow::on_actionProgressive_Morphological_Filter_triggered()
{
    std::vector<CloudData*> selected_clouds = get_selected_clouds();
    if (!selected_clouds.empty())
    {
        mProgressiveMorphologicalFilterDialog->setClouds(selected_clouds);
        mProgressiveMorphologicalFilterDialog->show();
    }
}

void MainWindow::on_actionSplit_Cloud_Into_Chunks_triggered()
{
    std::vector<CloudData*> selected_clouds = get_selected_clouds();
    if (!selected_clouds.empty())
    {
        mChunkSplitDialog->setClouds(selected_clouds);
        mChunkSplitDialog->show();
    }
}

void MainWindow::on_actionColor_Filtering_triggered()
{
    std::vector<CloudData*> selected_clouds = get_selected_clouds();
    if (!selected_clouds.empty())
    {
        mColorFilteringDialog->setClouds(selected_clouds);
        mColorFilteringDialog->show();
    }
}

void MainWindow::on_actionCircular_Point_Removal_triggered()
{
    std::vector<CloudData*> selected_clouds = get_selected_clouds();
    if (!selected_clouds.empty())
    {
        mCircularRemovalDialog->setClouds(selected_clouds);
        mCircularRemovalDialog->show();
    }
}

void MainWindow::on_actionDisjoin_Two_Clouds_triggered()
{
    std::vector<CloudData*> all_clouds = this->get_selected_clouds();
    mDisjoinCloudsDialog->setClouds(all_clouds);
    this->mDisjoinCloudsDialog->show();
}

void MainWindow::on_actionBatch_Processor_triggered()
{
    this->mBatchProcessorDialog->refresh_list();
    this->mBatchProcessorDialog->show();
}


void MainWindow::on_actionStatistical_Outlier_Removal_triggered()
{
    std::vector<CloudData*> selected_clouds = get_selected_clouds();
    if (!selected_clouds.empty())
    {
        mStatisticalOutlierRemovalDialog->setClouds(selected_clouds);
        mStatisticalOutlierRemovalDialog->show();
    }
}

void MainWindow::on_actionConcave_Hull_Meshing_triggered()
{
    std::vector<CloudData*> selected_clouds = get_selected_clouds();
    if (!selected_clouds.empty())
    {
        mConcaveHullMeshDialog->setClouds(selected_clouds);
        mConcaveHullMeshDialog->show();
    }
}

void MainWindow::on_actionConcave_Hull_Cloud_Extraction_triggered()
{
    std::vector<CloudData*> selected_clouds = get_selected_clouds();
    if (!selected_clouds.empty())
    {
        mConcaveHullPointCloudDialog->setClouds(selected_clouds);
        mConcaveHullPointCloudDialog->show();
    }
}

void MainWindow::on_actionLaplacian_Mesh_Smoothing_triggered()
{
    std::vector<CloudData*> selected_clouds = get_selected_clouds();
    if (!selected_clouds.empty())
    {
        mSmoothMeshLaplacianDialog->setClouds(selected_clouds);
        mSmoothMeshLaplacianDialog->show();
    }
}

void MainWindow::on_actionSmooth_Poly_Data_Filter_triggered()
{
    std::vector<CloudData*> selected_clouds = get_selected_clouds();
    if (!selected_clouds.empty())
    {
        mSmoothPolyDataFilterDialog->setClouds(selected_clouds);
        mSmoothPolyDataFilterDialog->show();
    }
}

void MainWindow::on_actionGet_Mesh_Quality_triggered()
{
    std::vector<CloudData*> selected_clouds = get_selected_clouds();
    if (!selected_clouds.empty())
    {
        for (std::vector<CloudData*>::iterator cloudDataIt = selected_clouds.begin(); cloudDataIt != selected_clouds.end(); ++cloudDataIt)
        {
            std::cout << "\n ** QUALITY of cloud " << (*cloudDataIt)->get_cloud_name() << " **\n" << std::endl;
            double quality = Process::get_mesh_quality(*cloudDataIt);
            std::cout << ">> mean quality: " << quality << std::endl;
            std::cout << "\n ********************************************************************" << std::endl;

        }
    }
}

void MainWindow::on_actionQuadratic_Mesh_Decimation_triggered()
{
    std::vector<CloudData*> selected_clouds = get_selected_clouds();
    if (!selected_clouds.empty())
    {
        mQuadraticMeshDecimationDialog->setClouds(selected_clouds);
        mQuadraticMeshDecimationDialog->show();
    }
}

void MainWindow::on_actionQuadratic_Clustering_Downsampling_triggered()
{
    std::vector<CloudData*> selected_clouds = get_selected_clouds();
    if (!selected_clouds.empty())
    {
        mMeshQuadraticClusteringDialog->setClouds(selected_clouds);
        mMeshQuadraticClusteringDialog->show();
    }
}

void MainWindow::on_actionMesh_Subdivision_triggered()
{
    std::vector<CloudData*> selected_clouds = get_selected_clouds();
    if (!selected_clouds.empty())
    {
        mLinearMeshSubdivisionDialog->setClouds(selected_clouds);
        mLinearMeshSubdivisionDialog->show();
    }
}

void MainWindow::on_checkBox_view_point_clouds_clicked(bool checked)
{
    this->refresh_viewer(false);
}

void MainWindow::on_pushButton_background_colour_clicked()
{
    background_colour = QColorDialog::getColor(Qt::black, this );
    this->refresh_viewer(false);
}



