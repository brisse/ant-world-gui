#ifndef STATISTICALOUTLIERREMOVALDIALOG_H
#define STATISTICALOUTLIERREMOVALDIALOG_H

#include "customqfilterdialog.h"
#include <QProgressDialog>

#include <pcl/point_types.h>
#include <pcl/common/common.h>

#include "clouddata.h"
#include "cloudprocessing.h"

namespace Ui {
class StatisticalOutlierRemovalDialog;
}

class StatisticalOutlierRemovalDialog : public CustomQFilterDialog
{
    Q_OBJECT

public:
    explicit StatisticalOutlierRemovalDialog(QWidget *parent = 0);
    ~StatisticalOutlierRemovalDialog();

private slots:
    void on_pushButton_filter_clicked();
    void on_pushButton_batch_clicked();
    void on_pushButton_close_clicked();

private:
    Ui::StatisticalOutlierRemovalDialog *ui;
};

#endif // STATISTICALOUTLIERREMOVALDIALOG_H
