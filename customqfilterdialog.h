#ifndef CUSTOMQFILTERDIALOG_H
#define CUSTOMQFILTERDIALOG_H

#include <QDialog>
#include <QWidget>
#include <QAction>
#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

#include "clouddata.h"
#include "cloudprocessing.h"
#include "batchprocessordialog.h"
#include "batchevent.h"

class CustomQFilterDialog : public QDialog
{
    Q_OBJECT

public:
    CustomQFilterDialog(QWidget *parent = 0);
    void setClouds(std::vector<CloudData*> & input_clouds);
    void setBatchProcessor(BatchProcessorDialog* batch_processor);

protected:
    std::vector<CloudData*> clouds;
    BatchProcessorDialog* batch_processor;

signals:
    void filter_button_pressed();
    void send_status_message(QString message);
};

#endif // CUSTOMQFILTERDIALOG_H
