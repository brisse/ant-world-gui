#include "colorfilteringdialog.h"
#include "../build/debug/ui_colorfilteringdialog.h"

ColorFilteringDialog::ColorFilteringDialog(QWidget *parent) :
    CustomQFilterDialog(parent),
    ui(new Ui::ColorFilteringDialog)
{
    ui->setupUi(this);
}

ColorFilteringDialog::~ColorFilteringDialog()
{
    delete ui;
}

void ColorFilteringDialog::on_pushButton_close_clicked()
{
    this->hide();
}

void ColorFilteringDialog::on_pushButton_filter_clicked()
{
    int red_lower = ui->spinBox_red_lower->value();
    int red_upper = ui->spinBox_red_upper->value();
    int green_lower = ui->spinBox_green_lower->value();
    int green_upper = ui->spinBox_green_upper->value();
    int blue_lower = ui->spinBox_blue_lower->value();
    int blue_upper = ui->spinBox_blue_upper->value();
    bool inside_red = ui->checkBox_red_inside->isChecked();
    bool inside_green = ui->checkBox_green_inside->isChecked();
    bool inside_blue = ui->checkBox_blue_inside->isChecked();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        Process::filter_color_inplace(*cloud_it, red_lower, red_upper, green_lower, green_upper, blue_lower, blue_upper, inside_red, inside_green, inside_blue);
        emit send_status_message(QString("Color filtered! Number of points: " + QString::number((*cloud_it)->get_cloud_size())));
    }

    emit filter_button_pressed();

}

void ColorFilteringDialog::on_pushButton_batch_clicked()
{
    int red_lower = ui->spinBox_red_lower->value();
    int red_upper = ui->spinBox_red_upper->value();
    int green_lower = ui->spinBox_green_lower->value();
    int green_upper = ui->spinBox_green_upper->value();
    int blue_lower = ui->spinBox_blue_lower->value();
    int blue_upper = ui->spinBox_blue_upper->value();
    bool inside_red = ui->checkBox_red_inside->isChecked();
    bool inside_green = ui->checkBox_green_inside->isChecked();
    bool inside_blue = ui->checkBox_blue_inside->isChecked();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        batch_processor->add_event(new ColorFilteringEvent(*cloud_it, red_lower, red_upper, green_lower, green_upper, blue_lower, blue_upper, inside_red, inside_green, inside_blue));
    }

    emit send_status_message(QString("Color filtering batch processing for " + QString::number(clouds.size()) + " clouds." ));
}
