#include "disjoincloudsdialog.h"
#include "../build/debug/ui_disjoincloudsdialog.h"

DisjoinCloudsDialog::DisjoinCloudsDialog(QWidget *parent) :
    CustomQFilterDialog(parent),
    ui(new Ui::DisjoinCloudsDialog)
{
    ui->setupUi(this);
}

DisjoinCloudsDialog::~DisjoinCloudsDialog()
{
    delete ui;
}

void DisjoinCloudsDialog::setClouds(std::vector<CloudData*> & input_clouds)
{
    this->clouds = input_clouds;
    ui->comboBox_full_cloud->clear();
    ui->comboBox_exclusion_cloud->clear();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        std::string cloud_name = (*cloud_it)->get_cloud_name();
        QString cloud_name_qstr = QString::fromStdString(cloud_name);
        ui->comboBox_full_cloud->addItem(cloud_name_qstr);
        ui->comboBox_exclusion_cloud->addItem(cloud_name_qstr);
    }
}

void DisjoinCloudsDialog::on_pushButton_filter_clicked()
{
    QString full_cloud_name = ui->comboBox_full_cloud->currentText();
    QString exclusion_cloud_name = ui->comboBox_exclusion_cloud->currentText();

    CloudData* full_cloud;
    CloudData* exclusion_cloud;

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        if (full_cloud_name.toStdString() == (*cloud_it)->get_cloud_name())
        {
            full_cloud = *cloud_it;
        }
        else if (exclusion_cloud_name.toStdString() == (*cloud_it)->get_cloud_name())
        {
            exclusion_cloud = *cloud_it;
        }
    }

    this->disjoin_clouds_inplace(full_cloud, exclusion_cloud);

//    pcl::PointCloud<pcl::PointXYZRGB> ret_cloud;

//    if (full_cloud->has_cloud() && exclusion_cloud->has_cloud())
//    {
//        pcl::PointIndices::Ptr exclusion_cloud_indices_ptr = exclusion_cloud->get_cloud_point_indices_ptr();

//        pcl::ExtractIndices<pcl::PointXYZRGB> filter;
//        filter.setInputCloud(full_cloud->get_cloud());
//        filter.setIndices(exclusion_cloud_indices_ptr);
//        filter.setNegative(true);
//        filter.filter(ret_cloud);

//        full_cloud->set_cloud(ret_cloud);
//    }


}

void DisjoinCloudsDialog::on_pushButton_close_clicked()
{
    this->hide();
}

void DisjoinCloudsDialog::disjoin_clouds_inplace(CloudData *full_cloud, CloudData *exclusion_cloud)
{
    pcl::PointCloud<pcl::PointXYZRGB> fc = *full_cloud->get_cloud();
    pcl::PointCloud<pcl::PointXYZRGB> ec = *exclusion_cloud->get_cloud();

    pcl::PointCloud<pcl::PointXYZRGB> ret_cloud;
    int counter = 0;
    for (pcl::PointCloud<pcl::PointXYZRGB>::iterator point_fc = fc.begin(); point_fc != fc.end(); ++point_fc)
    {
        pcl::PointXYZRGB fc_point = *point_fc;
        bool not_found = true;
        for (pcl::PointCloud<pcl::PointXYZRGB>::iterator point_ec = ec.begin(); point_ec != ec.end(); ++point_ec)
        {
            pcl::PointXYZRGB ec_point = *point_ec;

            if (fc_point.x == ec_point.x &&
                    fc_point.y == ec_point.y &&
                    fc_point.z == ec_point.z &&
                    fc_point.rgb == ec_point.rgb)
            {
                not_found = false;
                break;
            }
        }
        std::cout << "iteration: " << ++counter << std::endl;

        if (not_found)
        {
            ret_cloud.push_back(fc_point);
        }

    }

    full_cloud->set_cloud(ret_cloud);

    emit send_status_message(QString("Clouds disjoined. New cloud size: " + QString::number(full_cloud->get_cloud_size())));
    emit filter_button_pressed();

}
