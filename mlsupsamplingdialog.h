#ifndef MLSUPSAMPLINGDIALOG_H
#define MLSUPSAMPLINGDIALOG_H

#include "customqfilterdialog.h"

#include <pcl/point_types.h>
#include <pcl/common/common.h>
#include <pcl/surface/mls.h>

#include "clouddata.h"
#include "cloudprocessing.h"

namespace Ui {
class MLSUpsamplingDialog;
}

class MLSUpsamplingDialog : public CustomQFilterDialog
{
    Q_OBJECT

public:
    explicit MLSUpsamplingDialog(QWidget *parent = 0);
    ~MLSUpsamplingDialog();

    void set_calc_normals(bool calculate_normals);

private:
    Ui::MLSUpsamplingDialog *ui;

private slots:
    void on_pushButton_2_clicked();
    void on_pushButton_clicked();
    void on_pushButton_batch_clicked();
};

#endif // MLSUPSAMPLINGDIALOG_H
