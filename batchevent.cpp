#include "batchevent.h"

CircularRemovalEvent::CircularRemovalEvent(CloudData *cloud, float x_center, float y_center, float distance)
{
    this->event_name = "Circular Removal";
    this->cloud = cloud;
    this->x_center = x_center;
    this->y_center = y_center;
    this->distance = distance;
}

void CircularRemovalEvent::execute(void)
{
    std::cout << "cloud before batch execution: " << cloud->get_cloud_size() << std::endl;
    Process::circular_removal_inplace(cloud,x_center,y_center,distance);
    std::cout << "cloud after batch execution: " << cloud->get_cloud_size() << std::endl;
}

QString CircularRemovalEvent::get_event_details(void)
{
    QString q_str = QString::fromStdString(this->event_name)
            + " (" +QString::fromStdString(this->cloud->get_cloud_name())
            + ") \n    Parameters: \n        X Center="
            + QString::number(x_center)
            + "\n        Y Center="
            + QString::number(y_center)
            + "\n        Distance="
            + QString::number(distance);
    return q_str;
}

CircularClusteringEvent::CircularClusteringEvent(CloudData *cloud, float x_center, float y_center, float distance)
{
    this->event_name = "Circular Clustering";
    this->cloud = cloud;
    this->x_center = x_center;
    this->y_center = y_center;
    this->distance = distance;
}

void CircularClusteringEvent::execute(void)
{
    std::cout << "cloud before batch execution: " << cloud->get_cloud_size() << std::endl;
    Process::circular_clustering_inplace(cloud,x_center,y_center,distance);
    std::cout << "cloud after batch execution: " << cloud->get_cloud_size() << std::endl;
}

QString CircularClusteringEvent::get_event_details(void)
{
    QString q_str = QString::fromStdString(this->event_name)
            + " (" +QString::fromStdString(this->cloud->get_cloud_name())
            + ") \n    Parameters: \n        X Center="
            + QString::number(x_center)
            + "\n        Y Center="
            + QString::number(y_center)
            + "\n        Distance="
            + QString::number(distance);
    return q_str;
}


DownSamplingEvent::DownSamplingEvent(CloudData *cloud, double leaf_x, double leaf_y, double leaf_z,
                                     bool use_min_number_of_points, int min_number_of_points,
                                     bool use_filter_limits, double min_limit, double max_limit,
                                     bool individual_cluster, bool enforce_reduction, int min_reduction_percentage,
                                     double reduction_increment, QWidget *caller_dialog)
{
    this->event_name = "Down Sampling";
    this->cloud = cloud;
    this->leaf_x = leaf_x;
    this->leaf_y = leaf_y;
    this->leaf_z = leaf_z;
    this->use_min_number_of_points = use_min_number_of_points;
    this->min_number_of_points = min_number_of_points;
    this->use_filter_limits = use_filter_limits;
    this->min_limit = min_limit;
    this->max_limit = max_limit;
    this->individual_cluster = individual_cluster;
    this->enforce_reduction = enforce_reduction;
    this->min_reduction_percentage = min_reduction_percentage;
    this->reduction_increment = reduction_increment;
    this->caller_dialog = caller_dialog;
}

void DownSamplingEvent::execute(void)
{
    std::cout << "cloud before batch execution: " << cloud->get_cloud_size() << std::endl;
    Process::downsample_cloud_inplace(cloud, leaf_x, leaf_y, leaf_z, use_min_number_of_points, min_number_of_points, use_filter_limits, min_limit, max_limit, individual_cluster,
                                      enforce_reduction, min_reduction_percentage, reduction_increment, caller_dialog);
    std::cout << "cloud after batch execution: " << cloud->get_cloud_size() << std::endl;
}

QString DownSamplingEvent::get_event_details(void)
{
    QString q_str = QString::fromStdString(this->event_name)
            + " (" +QString::fromStdString(this->cloud->get_cloud_name())
            + ") \n    Parameters:"
            + "\n        Leaf X=" + QString::number(leaf_x)
            + "\n        Leaf Y=" + QString::number(leaf_y)
            + "\n        Leaf Z=" + QString::number(leaf_z)
            + "\n        Use Min Number of Points=" + QString::number(use_min_number_of_points) + " Value: " + QString::number(min_number_of_points)
            + "\n        Use Filter Limits=" + QString::number(use_filter_limits) + " [" + QString::number(min_limit) + "," + QString::number(max_limit) + "]"
            + "\n        Enforce Reduction=" + QString::number(enforce_reduction) + " Min Red. %=" + QString::number(min_reduction_percentage) + "Red. Incr.=" + QString::number(reduction_increment)
            + "\n        Individual Cluster=" + QString::number(individual_cluster);
    return q_str;
}


CalcNormalsEvent::CalcNormalsEvent(CloudData *input_cloud, double search_radius, int number_of_nearest_neighbors, bool use_search_radius, bool set_view_port, bool individual_clusters, QWidget *caller_dialog)
{
    this->event_name = "Calculate Normals";
    this->cloud = input_cloud;
    this->search_radius = search_radius;
    this->number_of_nearest_neighbors = number_of_nearest_neighbors;
    this->use_search_radius = use_search_radius;
    this->set_view_port = set_view_port;
    this->individual_clusters = individual_clusters;
    this->caller_dialog = caller_dialog;
}
void CalcNormalsEvent::execute(void)
{
    std::cout << "cloud before batch execution: " << cloud->get_cloud_size() << std::endl;
    Process::extract_normals_inplace(cloud, search_radius, number_of_nearest_neighbors, use_search_radius, set_view_port, individual_clusters, caller_dialog);
    std::cout << "cloud after batch execution: " << cloud->get_cloud_size() << std::endl;
}
QString CalcNormalsEvent::get_event_details()
{
    QString q_str = QString::fromStdString(this->event_name)
            + " (" +QString::fromStdString(this->cloud->get_cloud_name())
            + ") \n    Parameters:"
            + "\n        Number of Neighbors=" + QString::number(number_of_nearest_neighbors)
            + "\n        Use Search Radius=" + QString::number(use_search_radius) + " Value: " + QString::number(search_radius)
            + "\n        Set View Port (0,0,0)=" + QString::number(set_view_port)
            + "\n        Individual Cluster=" + QString::number(individual_clusters);
    return q_str;
}


RadiusOutlierRemovalEvent::RadiusOutlierRemovalEvent(CloudData *input_cloud, double search_radius, int min_neighbors_in_radius, bool individual_clusters, QWidget *caller_dialog)
{
    this->event_name = "Outlier Removal";
    this->cloud = input_cloud;
    this->search_radius = search_radius;
    this->min_neighors_in_radius = min_neighbors_in_radius;
    this->individual_clusters = individual_clusters;
    this->caller_dialog = caller_dialog;
}
void RadiusOutlierRemovalEvent::execute(void)
{
    std::cout << "cloud before batch execution: " << cloud->get_cloud_size() << std::endl;
    Process::remove_outliers_radius_inplace_1 (cloud, search_radius, min_neighors_in_radius, individual_clusters, caller_dialog);
    std::cout << "cloud after batch execution: " << cloud->get_cloud_size() << std::endl;
}
QString RadiusOutlierRemovalEvent::get_event_details()
{
    QString q_str = QString::fromStdString(this->event_name)
            + " (" +QString::fromStdString(this->cloud->get_cloud_name())
            + ") \n    Parameters:"
            + "\n        Search Radius=" + QString::number(search_radius)
            + "\n        Min Neighbors in Radius=" + QString::number(min_neighors_in_radius)
            + "\n        Individual Cluster=" + QString::number(individual_clusters);
    return q_str;
}


StatisticalOutlierRemovalEvent::StatisticalOutlierRemovalEvent(CloudData *input_cloud, int number_of_neighbors, double std_dev_multiplier, bool individual_clusters, QWidget *caller_dialog)
{
    this->event_name = "Stat. outlier Removal";
    this->cloud = input_cloud;
    this->number_of_neighbors = number_of_neighbors;
    this->std_dev_multiplier = std_dev_multiplier;
    this->individual_clusters = individual_clusters;
    this->caller_dialog = caller_dialog;
}
void StatisticalOutlierRemovalEvent::execute(void)
{
    std::cout << "cloud before batch execution: " << cloud->get_cloud_size() << std::endl;
    Process::remove_outliers_statistical_inplace(cloud, number_of_neighbors, std_dev_multiplier, individual_clusters, caller_dialog);
    std::cout << "cloud after batch execution: " << cloud->get_cloud_size() << std::endl;
}
QString StatisticalOutlierRemovalEvent::get_event_details()
{
    QString q_str = QString::fromStdString(this->event_name)
            + " (" +QString::fromStdString(this->cloud->get_cloud_name())
            + ") \n    Parameters:"
            + "\n        Number of Neighbors=" + QString::number(number_of_neighbors)
            + "\n        Std Deviation Multipl.=" + QString::number(std_dev_multiplier)
            + "\n        Individual Cluster=" + QString::number(individual_clusters);
    return q_str;
}


ChunkSplitEvent::ChunkSplitEvent(CloudData *cloud, int p_split, bool enforce_reduction, int max_chunk_size, QWidget* caller_dialog)
{
    this->event_name = "Chunk Split";
    this->cloud = cloud;
    this->p_split = p_split;
    this->enforce_reduction = enforce_reduction;
    this->max_chunk_size = max_chunk_size;
    this->caller_dialog = caller_dialog;
}
void ChunkSplitEvent::execute(void)
{
    std::cout << "cloud before batch execution: " << cloud->get_cloud_size() << std::endl;
    Process::extract_chunks_inplace (cloud, p_split, enforce_reduction, max_chunk_size, caller_dialog);
    std::cout << "cloud after batch execution: " << cloud->get_cloud_size() << std::endl;
}
QString ChunkSplitEvent::get_event_details()
{
    QString q_str = QString::fromStdString(this->event_name)
            + " (" +QString::fromStdString(this->cloud->get_cloud_name())
            + ") \n    Parameters:"
            + "\n        Split %=" + QString::number(p_split)
            + "\n        Enforce Reduction=" + QString::number(enforce_reduction) + " Value: " + QString::number(max_chunk_size);
    return q_str;
}


ColorFilteringEvent::ColorFilteringEvent(CloudData *cloud, const int red_lower, const int red_upper, const int green_lower, const int green_upper, const int blue_lower, const int blue_upper, const bool inside_red, const bool inside_green, const bool inside_blue)
{
    this->event_name = "Color Filtering";
    this->cloud = cloud;
    this->red_lower = red_lower;
    this->red_upper = red_upper;
    this->green_lower = green_lower;
    this->green_upper = green_upper;
    this->blue_lower = blue_lower;
    this->blue_upper = blue_upper;
    this->inside_red = inside_red;
    this->inside_green = inside_green;
    this->inside_blue = inside_blue;
}
void ColorFilteringEvent::execute(void)
{
    std::cout << "cloud before batch execution: " << cloud->get_cloud_size() << std::endl;
    Process::filter_color_inplace(cloud, red_lower, red_upper, green_lower, green_upper, blue_lower, blue_upper, inside_red, inside_green, inside_blue);
    std::cout << "cloud after batch execution: " << cloud->get_cloud_size() << std::endl;
}
QString ColorFilteringEvent::get_event_details()
{
    QString q_str = QString::fromStdString(this->event_name)
            + " (" +QString::fromStdString(this->cloud->get_cloud_name())
            + ") \n    Parameters:"
            + "\n        Red=[" + QString::number(red_lower) + "," + QString::number(red_upper) + "] inside:" + QString::number(inside_red)
            + "\n        Green=[" + QString::number(green_lower) + "," + QString::number(green_upper) + "] inside:" + QString::number(inside_green)
            + "\n        Blue=[" + QString::number(blue_lower) + "," + QString::number(blue_upper) + "] inside:" + QString::number(inside_blue);
    return q_str;
}

ConditionalFilteringEvent::ConditionalFilteringEvent(CloudData *cloud, double lower_limit, double upper_limit, std::string axis)
{
    this->event_name = "Conditional Filtering";
    this->cloud = cloud;
    this->lower_limit = lower_limit;
    this->upper_limit = upper_limit;
    this->axis = axis;
}
void ConditionalFilteringEvent::execute(void)
{
    std::cout << "cloud before batch execution: " << cloud->get_cloud_size() << std::endl;
    Process::conditional_vertex_filter_inplace(cloud, lower_limit, upper_limit, axis);
    std::cout << "cloud after batch execution: " << cloud->get_cloud_size() << std::endl;
}
QString ConditionalFilteringEvent::get_event_details()
{
    QString q_str = QString::fromStdString(this->event_name)
            + " (" +QString::fromStdString(this->cloud->get_cloud_name())
            + ") \n    Parameters:"
            + "\n        Lower Limit=" + QString::number(lower_limit)
            + "\n        Upper Limit=" + QString::number(upper_limit)
            + "\n        Axis=" + QString::fromStdString(axis);
    return q_str;
}


MLSUpsamplingEvent::MLSUpsamplingEvent(CloudData* input_cloud,
                                       int sampling_method,
                                       bool use_search_radius,
                                       double search_radius,
                                       double sqrt_gaussian_neighor,
                                       bool polynomial_fit,
                                       int polynomial_order,
                                       double upsampling_radius,
                                       double upsampling_step_size,
                                       int desired_num_of_points_in_radius,
                                       int dilation_iterations,
                                       float dilation_voxel_size,
                                       bool calculate_normals,
                                       bool individual_cluster,
                                       QWidget* caller_dialog)
{
    this->event_name = "MLS Upsamping";
    this->cloud = input_cloud;
    this->sampling_method = sampling_method;
    this->use_search_radius = use_search_radius;
    this->search_radius = search_radius;
    this->sqrt_gaussian_neighor = sqrt_gaussian_neighor;
    this->polynomial_fit = polynomial_fit;
    this->polynomial_order = polynomial_order;
    this->upsampling_radius = upsampling_radius;
    this->upsampling_step_size = upsampling_step_size;
    this->desired_num_of_points_in_radius = desired_num_of_points_in_radius;
    this->dilation_iterations = dilation_iterations;
    this->dilation_voxel_size = dilation_voxel_size;
    this->calculate_normals = calculate_normals;
    this->individual_cluster = individual_cluster;
    this->caller_dialog = caller_dialog;
}
void MLSUpsamplingEvent::execute(void)
{
    std::cout << "cloud before batch execution: " << cloud->get_cloud_size() << std::endl;
    Process::mls_upsampling_inplace_1(cloud,
                                      sampling_method,
                                      use_search_radius,
                                      search_radius,
                                      sqrt_gaussian_neighor,
                                      polynomial_fit,
                                      polynomial_order,
                                      upsampling_radius,
                                      upsampling_step_size,
                                      desired_num_of_points_in_radius,
                                      dilation_iterations,
                                      dilation_voxel_size,
                                      calculate_normals,
                                      individual_cluster,
                                      caller_dialog);
    std::cout << "cloud after batch execution: " << cloud->get_cloud_size() << std::endl;
}
QString MLSUpsamplingEvent::get_event_details()
{
    QString q_str = QString::fromStdString(this->event_name)
            + " (" +QString::fromStdString(this->cloud->get_cloud_name())
            + ") \n    Parameters:"
            + "\n        Sampling Method=" + QString::number(sampling_method)
            + "\n        Use Search Radius=" + QString::number(use_search_radius) + " Value: " + QString::number(search_radius)
            + "\n        Sqrt. Gauss. Neigh.=" + QString::number(sqrt_gaussian_neighor)
            + "\n        Use Polyn. Fit=" + QString::number(polynomial_fit) + " Order: " + QString::number(polynomial_order)
            + "\n        Upsampling Radius=" + QString::number(upsampling_radius)
            + "\n        Upsampling Step Size=" + QString::number(upsampling_step_size)
            + "\n        Desired Num. Points in Rad.=" + QString::number(desired_num_of_points_in_radius)
            + "\n        Dilation Iterations=" + QString::number(dilation_iterations)
            + "\n        Dilation Voxel Size=" + QString::number(dilation_voxel_size)
            + "\n        Calculate Normals=" + QString::number(calculate_normals)
            + "\n        Individual Cluster=" + QString::number(individual_cluster);
    return q_str;
}


ExtractClustersEvent::ExtractClustersEvent(CloudData *cloud, const double cluster_tolerance, const bool use_min_cluster, const int min_cluster_size, const bool use_max_cluster, const int max_cluster_size)
{
    this->event_name = "Extract Clusters";
    this->cloud = cloud;
    this->cluster_tolerance = cluster_tolerance;
    this->min_cluster_size = min_cluster_size;
    this->max_cluster_size = max_cluster_size;
    this->use_min_cluster = use_min_cluster;
    this->use_max_cluster = use_max_cluster;
}
void ExtractClustersEvent::execute(void)
{
    std::cout << "cloud before batch execution: " << cloud->get_cloud_size() << std::endl;
    Process::extract_clusters_from_cloud_inplace(cloud, cluster_tolerance, use_min_cluster, min_cluster_size, use_max_cluster, max_cluster_size);
    std::cout << "cloud after batch execution: " << cloud->get_cloud_size() << std::endl;
}
QString ExtractClustersEvent::get_event_details()
{
    QString q_str = QString::fromStdString(this->event_name)
            + " (" +QString::fromStdString(this->cloud->get_cloud_name())
            + ") \n    Parameters:"
            + "\n        Cluster Tolerance=" + QString::number(cluster_tolerance)
            + "\n        Use Min Cluster Size=" + QString::number(use_min_cluster) + " Value: " + QString::number(min_cluster_size)
            + "\n        Use Max Cluster Size=" + QString::number(use_max_cluster) + " Value: " + QString::number(max_cluster_size);
    return q_str;
}

ProgressiveMorphologicalFilterEvent::ProgressiveMorphologicalFilterEvent(CloudData *input_cloud, int max_window_size, float slope, float initial_distance,
                                                                         float max_distance, float cell_size, float base, bool exponential,
                                                                         bool individual_cluster, bool merge_resultant_cluster, QWidget *caller_dialog)
{
    this->event_name = "Progressive Morphological Filter";
    this->cloud = input_cloud;
    this->max_window_size = max_window_size;
    this->slope = slope;
    this->initial_distance = initial_distance;
    this->max_distance = max_distance;
    this->cell_size = cell_size;
    this->base = base;
    this->exponential = exponential;
    this->individual_cluster = individual_cluster;
    this->merge_resultant_cluster = merge_resultant_cluster;
    this->caller_dialog = caller_dialog;
}
void ProgressiveMorphologicalFilterEvent::execute(void)
{
    std::cout << "cloud before batch execution: " << cloud->get_cloud_size() << std::endl;
    Process::progressive_morphological_filter_inplace(cloud, max_window_size, slope, initial_distance, max_distance, cell_size, base, exponential, individual_cluster, merge_resultant_cluster, caller_dialog);
    std::cout << "cloud after batch execution: " << cloud->get_cloud_size() << std::endl;
}
QString ProgressiveMorphologicalFilterEvent::get_event_details()
{
    QString q_str = QString::fromStdString(this->event_name)
            + " (" +QString::fromStdString(this->cloud->get_cloud_name())
            + ") \n    Parameters:"
            + "\n        Max Window Size=" + QString::number(max_window_size)
            + "\n        Slope=" + QString::number(slope)
            + "\n        Initial Distance=" + QString::number(initial_distance)
            + "\n        Max Distance=" + QString::number(max_distance)
            + "\n        Cell Size=" + QString::number(cell_size)
            + "\n        Base=" + QString::number(base)
            + "\n        Exponential=" + QString::number(exponential)
            + "\n        Individual Cluster=" + QString::number(individual_cluster)
            + "\n        Merge Res. Cluster=" + QString::number(merge_resultant_cluster);
    return q_str;
}

RegionGrowingSegmentationEvent::RegionGrowingSegmentationEvent(CloudData *cloud, bool individual_cluster, int number_of_neighbors, bool use_min_cluster_size,
                                                               int min_cluster_size, bool use_max_cluster_size, int max_cluster_size, bool use_smoothness_threshold,
                                                               double smoothness_threshold, bool use_curvature_threshold, double curvature_threshold, bool make_residual_test,
                                                               double residual_test, bool merge_resultant_clusters, QWidget *caller_dialog)
{
    this->event_name = "Region Growing Seg.";
    this->cloud = cloud;
    this->individual_cluster = individual_cluster;
    this->number_of_neighbors = number_of_neighbors;
    this->use_min_cluster_size = use_min_cluster_size;
    this->min_cluster_size = min_cluster_size;
    this->use_max_cluster_size = use_max_cluster_size;
    this->max_cluster_size = max_cluster_size;
    this->use_smoothness_threshold = use_smoothness_threshold;
    this->smoothness_threshold = smoothness_threshold;
    this->use_curvature_threshold = use_curvature_threshold;
    this->curvature_threshold = curvature_threshold;
    this->make_residual_test = make_residual_test;
    this->residual_test = residual_test;
    this->merge_resultant_clusters = merge_resultant_clusters;
    this->caller_dialog = caller_dialog;
}
void RegionGrowingSegmentationEvent::execute(void)
{
    std::cout << "cloud before batch execution: " << cloud->get_cloud_size() << std::endl;
    Process::region_growing_segmentation_inplace(cloud, individual_cluster, number_of_neighbors, use_min_cluster_size, min_cluster_size,
                                                 use_max_cluster_size, max_cluster_size, use_smoothness_threshold, smoothness_threshold,
                                                 use_curvature_threshold, curvature_threshold, make_residual_test, residual_test,
                                                 merge_resultant_clusters, caller_dialog);
    std::cout << "cloud after batch execution: " << cloud->get_cloud_size() << std::endl;
}
QString RegionGrowingSegmentationEvent::get_event_details(void)
{
    QString q_str = QString::fromStdString(this->event_name)
            + " (" +QString::fromStdString(this->cloud->get_cloud_name())
            + ") \n    Parameters:"
            + "\n        Number of Neighbors=" + QString::number(number_of_neighbors)
            + "\n        Use Min Cluster Size=" + QString::number(use_min_cluster_size) + " Value: " + QString::number(min_cluster_size)
            + "\n        Use Max Cluster Size=" + QString::number(use_max_cluster_size) + " Value: " + QString::number(max_cluster_size)
            + "\n        Use Smoothness Thesh=" + QString::number(use_smoothness_threshold) + " Value: " + QString::number(smoothness_threshold)
            + "\n        Use Curvature Thresh=" + QString::number(use_curvature_threshold) + " Value: " + QString::number(curvature_threshold)
            + "\n        Make Residual Test=" + QString::number(make_residual_test) + " Value: " + QString::number(residual_test)
            + "\n        Merge Res. Clusters=" + QString::number(merge_resultant_clusters)
            + "\n        Individual Cluster=" + QString::number(individual_cluster);
    return q_str;
}

ExtractConcaveHullCloudEvent::ExtractConcaveHullCloudEvent(CloudData *cloud, const double alpha, const int dimensions, const bool keep_informations, bool individual_clusters, QWidget* caller_dialog)
{
    this->event_name = "Concave Hull Cloud Extraction";
    this->cloud = cloud;
    this->alpha = alpha;
    this->dimensions = dimensions;
    this->keep_informations = keep_informations;
    this->individual_clusters = individual_clusters;
    this->caller_dialog = caller_dialog;
}
void ExtractConcaveHullCloudEvent::execute(void)
{
    std::cout << "cloud before batch execution: " << cloud->get_cloud_size() << std::endl;
    Process::extract_concave_hull_cloud_inplace(cloud, alpha, dimensions, keep_informations, individual_clusters, caller_dialog);
    std::cout << "cloud after batch execution: " << cloud->get_cloud_size() << std::endl;
}
QString ExtractConcaveHullCloudEvent::get_event_details()
{
    QString q_str = QString::fromStdString(this->event_name)
            + " (" +QString::fromStdString(this->cloud->get_cloud_name())
            + ") \n    Parameters:"
            + "\n        Alpha=" + QString::number(alpha)
            + "\n        Dimensions=" + QString::number(dimensions)
            + "\n        Keep Inform.=" + QString::number(keep_informations)
            + "\n        Individual Cluster=" + QString::number(individual_clusters);
    return q_str;
}

ConcaveHullMeshingEvent::ConcaveHullMeshingEvent(CloudData *cloud, const double alpha, const int dimensions, const bool keep_informations, bool individual_clusters, QWidget* caller_dialog)
{
    this->event_name = "Concave Hull Meshing";
    this->cloud = cloud;
    this->alpha = alpha;
    this->dimensions = dimensions;
    this->keep_informations = keep_informations;
    this->individual_clusters = individual_clusters;
    this->caller_dialog = caller_dialog;
}
void ConcaveHullMeshingEvent::execute(void)
{
    std::cout << "cloud before batch execution: " << cloud->get_cloud_size() << std::endl;
    Process::extract_concave_hull_mesh_inplace(cloud, alpha, dimensions, keep_informations, individual_clusters, caller_dialog);
    std::cout << "cloud after batch execution: " << cloud->get_cloud_size() << std::endl;
}
QString ConcaveHullMeshingEvent::get_event_details()
{
    QString q_str = QString::fromStdString(this->event_name)
            + " (" +QString::fromStdString(this->cloud->get_cloud_name())
            + ") \n    Parameters:"
            + "\n        Alpha=" + QString::number(alpha)
            + "\n        Dimensions=" + QString::number(dimensions)
            + "\n        Keep Inform.=" + QString::number(keep_informations)
            + "\n        Individual Cluster=" + QString::number(individual_clusters);
    return q_str;
}

TriangleMeshingEvent::TriangleMeshingEvent(CloudData *cloud, double search_radius, double mu, int max_nearest_neighbors,
                                           double max_surface_angle, double min_angle, double max_angle, bool normal_consistency,
                                           bool individual_cluster, QWidget* caller_dialog)
{
    this->event_name = "Triangle Meshing";
    this->cloud = cloud;
    this->search_radius = search_radius;
    this->mu = mu;
    this->max_nearest_neighbors = max_nearest_neighbors;
    this->max_surface_angle = max_surface_angle;
    this->min_angle = min_angle;
    this->max_angle = max_angle;
    this->normal_consistency = normal_consistency;
    this->individual_cluster = individual_cluster;
    this->caller_dialog = caller_dialog;
}
void TriangleMeshingEvent::execute(void)
{
    std::cout << "cloud before batch execution: " << cloud->get_cloud_size() << std::endl;
    Process::fast_mesh_triangulation_inplace(cloud, search_radius, mu, max_nearest_neighbors, max_surface_angle, min_angle, max_angle, normal_consistency, individual_cluster, caller_dialog);
    std::cout << "cloud after batch execution: " << cloud->get_cloud_size() << std::endl;
}
QString TriangleMeshingEvent::get_event_details(void)
{
    QString q_str = QString::fromStdString(this->event_name)
            + " (" +QString::fromStdString(this->cloud->get_cloud_name())
            + ") \n    Parameters:"
            + "\n        Search Radius=" + QString::number(search_radius)
            + "\n        Mu=" + QString::number(mu)
            + "\n        Max. Nearest Neigh.=" + QString::number(max_nearest_neighbors)
            + "\n        Max. Surface Angle=" + QString::number(max_surface_angle)
            + "\n        Min. Angle=" + QString::number(min_angle)
            + "\n        Max. Angle=" + QString::number(max_angle)
            + "\n        Normal Consistency=" + QString::number(normal_consistency)
            + "\n        Individual Cluster=" + QString::number(individual_cluster);
    return q_str;
}

PoissonSurfaceMeshingEvent::PoissonSurfaceMeshingEvent(CloudData *cloud, bool use_max_tree_depth, int maximum_tree_depth,
                                                       bool use_min_tree_depth, int min_tree_depth,
                                                       bool use_point_weight, double point_weight,
                                                       bool use_scale, double scale,
                                                       bool use_solver_divide, int solver_divide,
                                                       bool use_iso_divide, int iso_divide,
                                                       bool use_sample_per_node, double sample_per_node,
                                                       bool use_degree, int degree,
                                                       bool set_confidence, bool set_manifold, bool set_polygons,
                                                       bool individual_cluster, QWidget* caller_dialog)
{
    this->event_name = "Poisson Meshing";
    this->cloud = cloud;
    this->use_max_tree_depth = use_max_tree_depth;
    this->maximum_tree_depth = maximum_tree_depth;
    this->use_min_tree_depth = use_min_tree_depth;
    this->min_tree_depth = min_tree_depth;
    this->use_point_weight= use_point_weight;
    this->point_weight = point_weight;
    this->use_scale = use_scale;
    this->scale = scale;
    this->use_solver_divide = use_solver_divide;
    this->solver_divide = solver_divide;
    this->use_iso_divide = use_iso_divide;
    this->iso_divide = iso_divide;
    this->use_sample_per_node = use_sample_per_node;
    this->sample_per_node = sample_per_node;
    this->use_degree = use_degree;
    this->degree = degree;
    this->set_confidence = set_confidence;
    this->set_manifold = set_manifold;
    this->output_polygons = set_polygons;
    this->individual_cluster = individual_cluster;
    this->caller_dialog = caller_dialog;
}
void PoissonSurfaceMeshingEvent::execute(void)
{
    std::cout << "cloud before batch execution: " << cloud->get_cloud_size() << std::endl;
    Process::poisson_surface_reconstruction_inplace(cloud, use_max_tree_depth, maximum_tree_depth, use_min_tree_depth, min_tree_depth, use_point_weight, point_weight,
                                                    use_scale, scale, use_solver_divide, solver_divide, use_iso_divide, iso_divide, use_sample_per_node, sample_per_node,
                                                    use_degree, degree, set_confidence, set_manifold, output_polygons, individual_cluster, caller_dialog);
    std::cout << "cloud after batch execution: " << cloud->get_cloud_size() << std::endl;
}
QString PoissonSurfaceMeshingEvent::get_event_details(void)
{
    QString q_str = QString::fromStdString(this->event_name)
            + " (" +QString::fromStdString(this->cloud->get_cloud_name())
            + ") \n    Parameters:"
            + "\n        Use Max Tree Depth=" + QString::number(use_max_tree_depth) + " Value: " + QString::number(maximum_tree_depth)
            + "\n        Use Min Tree Depth=" + QString::number(use_min_tree_depth) + " Value: " + QString::number(min_tree_depth)
            + "\n        Use Point Weight=" + QString::number(use_point_weight) + " Value: " + QString::number(point_weight)
            + "\n        Use Scale=" + QString::number(use_scale) + " Value: " + QString::number(scale)
            + "\n        Use Solver Divide=" + QString::number(use_solver_divide) + " Value: " + QString::number(solver_divide)
            + "\n        Use Iso Divide=" + QString::number(use_iso_divide) + " Value: " + QString::number(iso_divide)
            + "\n        Use Sample Per Node=" + QString::number(use_sample_per_node) + " Value: " + QString::number(sample_per_node)
            + "\n        Use Degree=" + QString::number(use_degree) + " Value: " + QString::number(degree)
            + "\n        Set Confidence=" + QString::number(set_confidence)
            + "\n        Set Manifold=" + QString::number(set_manifold)
            + "\n        Output Polygons=" + QString::number(output_polygons)
            + "\n        Individual Cluster=" + QString::number(individual_cluster);
    return q_str;
}

QuadraticMeshDecimationEvent::QuadraticMeshDecimationEvent(CloudData *cloud, float reduction_factor, int iterations, QWidget* caller_dialog)
{
    this->event_name = "Quadratic Mesh Decimation";
    this->cloud = cloud;
    this->reduction_factor = reduction_factor;
    this->iterations = iterations;
    this->caller_dialog = caller_dialog;
}
void QuadraticMeshDecimationEvent::execute(void)
{
    std::cout << "cloud before batch execution: " << cloud->get_cloud_size() << std::endl;
    Process::quadratic_mesh_decimation(cloud, reduction_factor, iterations, caller_dialog);
    std::cout << "cloud after batch execution: " << cloud->get_cloud_size() << std::endl;
}
QString QuadraticMeshDecimationEvent::get_event_details(void)
{
    QString q_str = QString::fromStdString(this->event_name)
            + " (" +QString::fromStdString(this->cloud->get_cloud_name())
            + ") \n    Parameters:"
            + "\n        Reduction Factor=" + QString::number(reduction_factor)
            + "\n        Iterations=" + QString::number(iterations);
    return q_str;
}

MeshQuadraticClusteringEvent::MeshQuadraticClusteringEvent(CloudData *cloud,
                                                           bool auto_adjust_number_of_devisions,
                                                           int number_of_x_divisions,
                                                           int number_of_y_divisions,
                                                           int number_of_z_divisions,
                                                           bool use_feature_edges,
                                                           double feature_points_angle,
                                                           bool use_internal_triangles,
                                                           QWidget* caller_dialog)
{
    this->event_name = "Quadratic Clustering Event";
    this->cloud = cloud;
    this->auto_adjust_number_of_devisions = auto_adjust_number_of_devisions;
    this->number_of_x_divisions = number_of_x_divisions;
    this->number_of_y_divisions = number_of_y_divisions;
    this->number_of_z_divisions = number_of_z_divisions;
    this->use_feature_edges = use_feature_edges;
    this->feature_points_angle = feature_points_angle;
    this->use_internal_triangles = use_internal_triangles;
    this->caller_dialog = caller_dialog;
}
void MeshQuadraticClusteringEvent::execute(void)
{
    std::cout << "cloud before batch execution: " << cloud->get_cloud_size() << std::endl;
    Process::mesh_quadratic_clustering_inplace(cloud,
                                               auto_adjust_number_of_devisions,
                                               number_of_x_divisions,
                                               number_of_y_divisions,
                                               number_of_z_divisions,
                                               use_feature_edges,
                                               feature_points_angle,
                                               use_internal_triangles,
                                               caller_dialog);
    std::cout << "cloud after batch execution: " << cloud->get_cloud_size() << std::endl;
}
QString MeshQuadraticClusteringEvent::get_event_details(void)
{
    QString q_str = QString::fromStdString(this->event_name)
            + " (" +QString::fromStdString(this->cloud->get_cloud_name())
            + ") \n    Parameters:"
            + "\n        Auto Adj. Num. of Dev.=" + QString::number(auto_adjust_number_of_devisions) + " (X,Y,Z): " + QString::number(number_of_x_divisions) + "," + QString::number(number_of_y_divisions) + "," + QString::number(number_of_z_divisions)
            + "\n        Use Feature Edges=" + QString::number(use_feature_edges) + " Feat. Point Angles: " + QString::number(feature_points_angle)
            + "\n        Use Internal Triangles=" + QString::number(use_internal_triangles);
    return q_str;
}

MeshSubdivisionEvent::MeshSubdivisionEvent(CloudData *cloud, int filter_type, QWidget* caller_dialog)
{
    this->event_name = "Mesh Subdivision";
    this->cloud = cloud;
    this->filter_type = filter_type;
    this->caller_dialog = caller_dialog;
}
void MeshSubdivisionEvent::execute(void)
{
    std::cout << "cloud before batch execution: " << cloud->get_cloud_size() << std::endl;
    Process::mesh_subdivision(cloud, filter_type, caller_dialog);
    std::cout << "cloud after batch execution: " << cloud->get_cloud_size() << std::endl;
}
QString MeshSubdivisionEvent::get_event_details(void)
{
    QString q_str = QString::fromStdString(this->event_name)
            + " (" +QString::fromStdString(this->cloud->get_cloud_name())
            + ") \n    Parameters:"
            + "\n        Filter Type=" + QString::number(filter_type);
    return q_str;
}

SmoothMeshLaplacianEvent::SmoothMeshLaplacianEvent(CloudData *cloud,
                                                   int number_of_iterations,
                                                   float convergence,
                                                   float relaxation_factor,
                                                   bool use_feature_edge_smoothing,
                                                   float feature_angle,
                                                   float edge_angle,
                                                   bool use_boundary_smoothing,
                                                   QWidget *caller_dialog)
{
    this->event_name = "Lapl Mesh Smooth";
    this->cloud = cloud;
    this->number_of_iterations = number_of_iterations;
    this->convergence = convergence;
    this->relaxation_factor = relaxation_factor;
    this->use_feature_edge_smoothing = use_feature_edge_smoothing;
    this->feature_angle = feature_angle;
    this->edge_angle = edge_angle;
    this->use_boundary_smoothing = use_boundary_smoothing;
    this->caller_dialog = caller_dialog;
}
void SmoothMeshLaplacianEvent::execute(void)
{
    std::cout << "cloud before batch execution: " << cloud->get_cloud_size() << std::endl;
    Process::smooth_mesh_laplacian(cloud, number_of_iterations, convergence, relaxation_factor, use_feature_edge_smoothing, feature_angle, edge_angle, use_boundary_smoothing, caller_dialog);
    std::cout << "cloud after batch execution: " << cloud->get_cloud_size() << std::endl;
}
QString SmoothMeshLaplacianEvent::get_event_details(void)
{
    QString q_str = QString::fromStdString(this->event_name)
            + " (" +QString::fromStdString(this->cloud->get_cloud_name())
            + ") \n    Parameters:"
            + "\n        Number of iter=" + QString::number(number_of_iterations)
            + "\n        Convergence=" + QString::number(convergence)
            + "\n        Relaxation factor=" + QString::number(relaxation_factor)
            + "\n        Use feat. edge smo.=" + QString::number(use_feature_edge_smoothing)
            + "\n        Feature angle=" + QString::number(feature_angle)
            + "\n        Edge angle=" + QString::number(edge_angle)
            + "\n        Use bound. smo.=" + QString::number(use_boundary_smoothing);
    return q_str;
}

SmoothPolyDataFilterEvent::SmoothPolyDataFilterEvent(CloudData *cloud,
                                                     double convergence,
                                                     int iterations,
                                                     double relaxation_factor,
                                                     bool use_feature_edge_smoothing,
                                                     double  feature_angle,
                                                     double edge_angle,
                                                     bool use_boundary_smoothing,
                                                     QWidget *caller_dialog)
{
    this->event_name = "Smooth Poly Data";
    this->cloud = cloud;
    this->convergence = convergence;
    this->iterations = iterations;
    this->relaxation_factor = relaxation_factor;
    this->use_feature_edge_smoothing = use_feature_edge_smoothing;
    this->feature_angle = feature_angle;
    this->edge_angle = edge_angle;
    this->use_boundary_smoothing = use_boundary_smoothing;
    this->caller_dialog = caller_dialog;
}
void SmoothPolyDataFilterEvent::execute(void)
{
    std::cout << "cloud before batch execution: " << cloud->get_cloud_size() << std::endl;
    Process::smooth_poly_data_filter_inplace(cloud, convergence, iterations, relaxation_factor, use_feature_edge_smoothing, feature_angle, edge_angle, use_boundary_smoothing, caller_dialog);
    std::cout << "cloud after batch execution: " << cloud->get_cloud_size() << std::endl;
}
QString SmoothPolyDataFilterEvent::get_event_details(void)
{
    QString q_str = QString::fromStdString(this->event_name)
            + " (" +QString::fromStdString(this->cloud->get_cloud_name())
            + ") \n    Parameters:"
            + "\n        Convergence=" + QString::number(convergence)
            + "\n        Number of iter=" + QString::number(iterations)
            + "\n        Relaxation factor=" + QString::number(relaxation_factor)
            + "\n        Use feat. edge smo.=" + QString::number(use_feature_edge_smoothing)
            + "\n        Use bound. smo.=" + QString::number(use_boundary_smoothing)
            + "\n        Feature angle=" + QString::number(feature_angle)
            + "\n        Edge angle=" + QString::number(edge_angle);
    return q_str;
}

FilterClustersEvent::FilterClustersEvent(CloudData *cloud, bool use_max_allowed_height, double max_allowed_height,
                                         bool use_min_allowed_height, double min_allowed_height, bool use_max_allowed_cluster_size,
                                         int max_allowed_cluster_size, bool use_min_allowed_cluster_size, int min_allowed_cluster_size,
                                         QWidget* caller_dialog)
{
    this->event_name = "Filter Clusters";
    this->cloud = cloud;
    this->use_max_allowed_height = use_max_allowed_height;
    this->max_allowed_height = max_allowed_height;
    this->use_min_allowed_height = use_min_allowed_height;
    this->min_allowed_height = min_allowed_height;
    this->use_max_allowed_cluster_size = use_max_allowed_cluster_size;
    this->max_allowed_cluster_size = max_allowed_cluster_size;
    this->use_min_allowed_cluster_size = use_min_allowed_cluster_size;
    this->min_allowed_cluster_size = min_allowed_cluster_size;
    this->caller_dialog = caller_dialog;
}

void FilterClustersEvent::execute(void)
{
    std::cout << "cloud before batch execution: " << cloud->get_cloud_size() << std::endl;
    Process::filter_clusters(cloud, use_max_allowed_height, max_allowed_height, use_min_allowed_height, min_allowed_height,
                             use_max_allowed_cluster_size, max_allowed_cluster_size, use_min_allowed_cluster_size, min_allowed_cluster_size);
    std::cout << "cloud after batch execution: " << cloud->get_cloud_size() << std::endl;
}

QString FilterClustersEvent::get_event_details(void)
{
    QString q_str = QString::fromStdString(this->event_name)
            + " (" +QString::fromStdString(this->cloud->get_cloud_name())
            + ") \n    Parameters:"
            + "\n        Use Max Height =" + QString::number(use_max_allowed_height) + " Value: " + QString::number(max_allowed_height)
            + "\n        Use Min Height =" + QString::number(use_min_allowed_height) + " Value: " + QString::number(min_allowed_height)
            + "\n        Use Max Size =" + QString::number(use_max_allowed_cluster_size) + " Value: " + QString::number(max_allowed_cluster_size)
            + "\n        Use Max Size =" + QString::number(use_min_allowed_cluster_size) + " Value: " + QString::number(min_allowed_cluster_size);
    return q_str;
}

SaveCloudsEvent::SaveCloudsEvent(CloudData *cloud, QString path, QString cloud_suffix)
{
    this->event_name = "Save Cloud";
    this->cloud = cloud;
    this->path = path;
    this->cloud_suffix = cloud_suffix;
}
void SaveCloudsEvent::execute(void)
{
    std::cout << "cloud before batch execution: " << cloud->get_cloud_size() << std::endl;
    InputOutput::save_cloud(cloud, path, cloud_suffix);
    std::cout << "cloud after batch execution: " << cloud->get_cloud_size() << std::endl;
}
QString SaveCloudsEvent::get_event_details(void)
{
    QString q_str = QString::fromStdString(this->event_name)
            + " (" +QString::fromStdString(this->cloud->get_cloud_name())
            + ") \n    Parameters:"
            + "\n        Path=" + path
            + "\n        Cloud Suffix=" + cloud_suffix;
    return q_str;
}

