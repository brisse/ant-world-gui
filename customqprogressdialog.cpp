#include "customqprogressdialog.h"

CustomQProgressDialog::CustomQProgressDialog(CloudData *input_cloud, const bool &individual_cluster, QWidget *caller_dialog) :QProgressDialog(caller_dialog)
{
    this->cloud = input_cloud;
    this->individual_usage = false;
    QString initial_label_text = "Process " + QString::fromStdString(cloud->get_cloud_name()) + " with " + QString::number(cloud->get_cloud_size()) + " points...";
    this->setLabelText(initial_label_text);

    int max_progress_value = 2;
    if (individual_cluster)
    {
        max_progress_value = cloud->get_number_of_clusters();
        this->setCancelButtonText("Cancel");
    }
    else
    {
        this->setCancelButton(0);
        this->setValue(1);
    }

    this->setMinimum(0);
    this->max_progress_value = max_progress_value;
    this->setMaximum(max_progress_value);
    this->setWindowModality(Qt::WindowModal);
    this->setVisible(true);

}

CustomQProgressDialog::CustomQProgressDialog(CloudData *input_cloud, const int &max_progress_value, QWidget *caller_dialog) :QProgressDialog(caller_dialog)
{
    this->cloud = input_cloud;
    this->individual_usage = true;
    QString initial_label_text = "Process " + QString::fromStdString(cloud->get_cloud_name()) + " with " + QString::number(cloud->get_cloud_size()) + " points...";
    this->setLabelText(initial_label_text);
    this->setCancelButtonText("Cancel");
    this->setMinimum(0);

    this->max_progress_value = max_progress_value;
    this->setMaximum(max_progress_value);
    this->setWindowModality(Qt::WindowModal);
    this->setVisible(true);

}

void CustomQProgressDialog::update_progress(const int &value)
{
    this->setValue(value);
    if (! this->individual_usage)
    {
        int number_of_points = cloud->get_cluster_indices().at(value).indices.size();
        QString label_text = "Process cluster of " + QString::fromStdString(cloud->get_cloud_name()) + " with " + QString::number(number_of_points) + " points...";
        this->setLabelText(label_text);
    }
    else
    {
        QString label_text = "Process " + QString::fromStdString(cloud->get_cloud_name()) + " with " + QString::number(cloud->get_cloud_size()) + " points...";
        this->setLabelText(label_text);
    }
}

void CustomQProgressDialog::end_progress()
{
    this->setValue(max_progress_value);
    this->setVisible(false);
}
