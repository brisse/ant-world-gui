#ifndef BILATERALUPSAMPLINGDIALOG_H
#define BILATERALUPSAMPLINGDIALOG_H

#include "customqfilterdialog.h"

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/surface/bilateral_upsampling.h>

#include "clouddata.h"
#include "cloudprocessing.h"

namespace Ui {
class BilateralUpsamplingDialog;
}

class BilateralUpsamplingDialog : public CustomQFilterDialog
{
    Q_OBJECT

public:
    explicit BilateralUpsamplingDialog(QWidget *parent = 0);
    ~BilateralUpsamplingDialog();

private slots:
    void on_pushButton_filter_clicked();
    void on_pushButton_close_clicked();

private:
    Ui::BilateralUpsamplingDialog *ui;
};

#endif // BILATERALUPSAMPLINGDIALOG_H
