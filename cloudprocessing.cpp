#include "cloudprocessing.h"

namespace Process
{
void convert_cloudXYZRGB_2_cloudXYZ(pcl::PointCloud<pcl::PointXYZRGB> const & cloudXYZRGB,
                                    pcl::PointCloud<pcl::PointXYZ> & cloudXYZ)
{
    // cloudXYZ.clear();
    // cloudXYZ.reserve(cloudXYZRGB.size());
    cloudXYZ.points.resize(cloudXYZRGB.size());

    for (size_t i=0; i<cloudXYZRGB.size(); ++i)
    {
        cloudXYZ.points[i].x = cloudXYZRGB.points[i].x;
        cloudXYZ.points[i].y = cloudXYZRGB.points[i].y;
        cloudXYZ.points[i].z = cloudXYZRGB.points[i].z;
    }
    std::cout << "new cloud: " << cloudXYZ.size() << std::endl;
}

void extract_normals_from_pointnormals(pcl::PointCloud<pcl::PointNormal> const & cloudPointNormal,
                                       pcl::PointCloud<pcl::Normal> & ret_normals)
{
    ret_normals.reserve(cloudPointNormal.size());
    for (size_t i=0; i<cloudPointNormal.size(); ++i)
    {
        pcl::PointNormal p = cloudPointNormal.at(i);
        float normalx = p.normal_x;
        float normaly = p.normal_y;
        float normalz = p.normal_z;
        pcl::Normal normal(normalx,normaly,normalz);
        std::cout << normal << std::endl;

        ret_normals.push_back(normal);
    }
    std::cout << "new normal cloud: " << ret_normals.size() << std::endl;

}

void extract_points_from_pointnormals(pcl::PointCloud<pcl::PointNormal> const & cloudPointNormal,
                                      pcl::PointCloud<pcl::PointXYZRGB> & ret_points)
{
    ret_points.reserve(cloudPointNormal.size());
    for (size_t i=0; i<cloudPointNormal.size(); ++i)
    {
        ret_points.push_back(pcl::PointXYZRGB(cloudPointNormal.points[i].x, cloudPointNormal.points[i].y, cloudPointNormal.points[i].z));
        //      ret_points.points[i].x = cloudPointNormal.points[i].x;
        //      ret_points.points[i].y = cloudPointNormal.points[i].y;
        //      ret_points.points[i].z = cloudPointNormal.points[i].z;
        //TODO: how to add color from original cloud to this new cloud?
    }
    std::cout << "new point cloud: " << ret_points.size() << std::endl;
}

void extract_clusters_from_cloud (CloudData* cur_cloud, std::vector<pcl::PointCloud<pcl::PointXYZRGB>::Ptr> & ret_cloud_clusters)
{
    std::vector<pcl::PointIndices> cluster_indices = cur_cloud->get_cluster_indices();
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud = cur_cloud->get_cloud();

    for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it)
    {
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_cluster (new pcl::PointCloud<pcl::PointXYZRGB>);
        for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); ++pit)
        {
            cloud_cluster->points.push_back (cloud->points[*pit]); //*
        }

        cloud_cluster->width = cloud_cluster->points.size ();
        cloud_cluster->height = 1;
        cloud_cluster->is_dense = true;

        // std::cout << "PointCloud representing the Cluster: " << cloud_cluster->points.size () << " data points." << std::endl;
        ret_cloud_clusters.push_back(cloud_cluster);
    }

    std::cout << "Number of clusters found: " << ret_cloud_clusters.size() << std::endl;
}

void extract_normal_clusters_from_cloud(CloudData *cur_cloud, std::vector<pcl::PointCloud<pcl::Normal>::Ptr> &ret_normal_clusters)
{
    std::vector<pcl::PointIndices> cluster_indices = cur_cloud->get_cluster_indices();
    pcl::PointCloud<pcl::Normal>::Ptr normals = cur_cloud->get_normals();

    for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it)
    {
        pcl::PointCloud<pcl::Normal>::Ptr normals_cluster (new pcl::PointCloud<pcl::Normal>);
        for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); ++pit)
        {
            normals_cluster->points.push_back(pcl::Normal(normals->points[*pit].normal[0],
                    normals->points[*pit].normal[1],
                    normals->points[*pit].normal[2]));//*
        }

        normals_cluster->width = normals_cluster->points.size ();
        normals_cluster->height = 1;
        normals_cluster->is_dense = true;

        // std::cout << "PointCloud representing the Cluster: " << cloud_cluster->points.size () << " data points." << std::endl;
        ret_normal_clusters.push_back(normals_cluster);
    }

    std::cout << "Number of normals found: " << ret_normal_clusters.size() << std::endl;
}

void extract_clusters_from_cloud_inverted(CloudData *cloud, CloudData & tmp_cloud_data)
{
    CloudData tmp_cloud;
    merge_clusters(cloud, tmp_cloud);
    std::vector<pcl::PointIndices> cluster = tmp_cloud.get_cluster_indices();
    pcl::PointIndices::Ptr indices_ptr = boost::make_shared<pcl::PointIndices>(cluster.at(0));

    pcl::ExtractIndices<pcl::PointXYZRGB> filter (true);
    filter.setInputCloud(cloud->get_cloud());
    filter.setIndices(indices_ptr);
    filter.setNegative(true);

    pcl::PointCloud<pcl::PointXYZRGB>::Ptr ret_cloud (new pcl::PointCloud<pcl::PointXYZRGB>);
    filter.filter(*ret_cloud);

    std::stringstream ss;
    ss << cloud->get_cloud_name() << "_inverted";

    tmp_cloud_data.set_original_cloud(*ret_cloud);
    tmp_cloud_data.set_cloud_name(ss.str());
    tmp_cloud_data.set_cloud_path(cloud->get_cloud_path());

    //    CloudData tmp_cloud_data(ret_cloud, ss.str(), cloud->get_cloud_path());
    //    return tmp_cloud_data;

}

float dist_x_y(pcl::PointXYZRGB &p1, pcl::PointXYZRGB &p2)
{
    float x_dist = p1.x - p2.x;
    float y_dist = p1.y - p2.y;

    float dist = pow(x_dist,2) + pow(y_dist,2);
    dist = sqrt(dist);
    return dist;
}

void merge_clusters(CloudData *cur_cloud, CloudData &output_cloud)
{
    if (cur_cloud->has_clusters())
    {
        std::vector<pcl::PointIndices> cluster_indices;
        pcl::PointIndices point_indices;

        std::vector<pcl::PointIndices> cur_cluster_indices = cur_cloud->get_cluster_indices();

        for (std::vector<pcl::PointIndices>::iterator it = cur_cluster_indices.begin(); it != cur_cluster_indices.end(); ++it)
        {
            std::vector<int> cur_point_indices = (*it).indices;
            for (std::vector<int>::iterator p_it = cur_point_indices.begin(); p_it != cur_point_indices.end(); ++p_it)
            {
                point_indices.indices.push_back(*p_it);
            }
        }
        cluster_indices.push_back(point_indices);

        output_cloud = *cur_cloud;
        output_cloud.set_cluster_indices(cluster_indices);
    }
}

void filter_clusters(CloudData *cloud, const bool &use_max_allowed_height,
                     const double &max_allowed_height, const bool &use_min_allowed_height,
                     const double &min_allowed_height, const bool &use_max_allowed_cluster_size,
                     const int &max_allowed_cluster_size, const bool &use_min_allowed_cluster_size,
                     const int &min_allowed_cluster_size)
{
    if (cloud->has_clusters())
    {
        std::vector<pcl::PointIndices> cluster_indices;

        std::vector<pcl::PointIndices> point_indices_tmp = cloud->get_cluster_indices();

        for (std::vector<pcl::PointIndices>::iterator point_indices = point_indices_tmp.begin();
             point_indices != point_indices_tmp.end(); ++ point_indices)
        {
            pcl::PointIndices cur_point_indices = *point_indices;

            Eigen::Vector4f min_point;
            Eigen::Vector4f max_point;
            pcl::getMinMax3D(*cloud->get_cloud(), cur_point_indices, min_point, max_point);

            bool b_min_height = true;
            if (use_min_allowed_height && (min_point(2) < min_allowed_height))
            {
                b_min_height = false;
            }
            bool b_max_height = true;
            if (use_max_allowed_height && (max_point(2) > max_allowed_height))
            {
                b_max_height = false;
            }

            bool b_min_size = true;
            if (use_min_allowed_cluster_size && (cur_point_indices.indices.size() < min_allowed_cluster_size))
            {
                b_min_size = false;
            }
            bool b_max_size = true;
            if (use_max_allowed_cluster_size && (cur_point_indices.indices.size() > max_allowed_cluster_size))
            {
                b_max_size = false;
            }

            if (b_min_height && b_max_height && b_min_size && b_max_size)
            {
                cluster_indices.push_back(cur_point_indices);
            }
        }

        cloud->set_cluster_indices(cluster_indices);
    }
}

void merge_clouds(std::vector<CloudData *> cloud_vec, CloudData & ret_cloud_data)
{
    QTime myTimer;
    myTimer.start();

    pcl::PointCloud<pcl::PointXYZRGB> concat_cloud;
    pcl::PointCloud<pcl::Normal> concat_normals;
    pcl::PolygonMesh concat_mesh;

    // check for normals
    bool all_have_normals = true;
    for (std::vector<CloudData*>::iterator cloud_it = cloud_vec.begin(); cloud_it != cloud_vec.end(); ++cloud_it)
    {
        all_have_normals = all_have_normals && (*cloud_it)->has_normals();
    }

    bool all_have_meshes = true;
    for (std::vector<CloudData*>::iterator cloud_it = cloud_vec.begin(); cloud_it != cloud_vec.end(); ++cloud_it)
    {
        all_have_meshes = all_have_meshes && (*cloud_it)->has_mesh();
    }

    std::string first_cloud_name;
    std::string first_cloud_path;

    // merge clouds
    bool first_cloud = true;
    for (std::vector<CloudData*>::iterator cloud_it = cloud_vec.begin(); cloud_it != cloud_vec.end(); ++cloud_it)
    {
        if (first_cloud)
        {
            concat_cloud = *((*cloud_it)->get_cloud());
            if (all_have_normals)
            {
                concat_normals = *((*cloud_it)->get_normals());
            }
            first_cloud_name = ((*cloud_it)->get_cloud_name());
            first_cloud_path = ((*cloud_it)->get_cloud_path());
            first_cloud = false;
        }
        else
        {
            concat_cloud += *((*cloud_it)->get_cloud());
            if (all_have_normals)
            {
                concat_normals += *((*cloud_it)->get_normals());
            }
        }
    }

    // merge meshes
    if (all_have_meshes)
    {
        concatinate_meshes(cloud_vec, concat_mesh);
    }

    std::stringstream ss;
    ss << first_cloud_name << "_merged";

    ret_cloud_data.set_original_cloud(concat_cloud);
    ret_cloud_data.set_cloud_name(ss.str());
    ret_cloud_data.set_cloud_path(first_cloud_path);
    if (all_have_normals)
        ret_cloud_data.set_normals(concat_normals);
    if (all_have_meshes)
        ret_cloud_data.set_mesh(concat_mesh);

    int milliseconds = myTimer.elapsed();
    std::cout << "Merging time: "  << milliseconds << " ms." << std::endl;

}

void split_cloud_into_clusters(CloudData *cloud, std::vector<CloudData> & ret_clouds)
{
    std::string cloud_name = cloud->get_cloud_name();
    std::string cloud_path = cloud->get_cloud_path();
    std::vector<pcl::PointCloud<pcl::PointXYZRGB>::Ptr> cloud_vec;
    cloud_vec.reserve(cloud->get_number_of_clusters());
    extract_clusters_from_cloud (cloud, cloud_vec);

    std::vector<pcl::PointCloud<pcl::Normal>::Ptr> normals_vec;
    if (cloud->has_normals())
    {
        extract_normal_clusters_from_cloud(cloud, normals_vec);
    }

    int cluster_number = 0;
    for (std::vector<pcl::PointCloud<pcl::PointXYZRGB>::Ptr>::iterator cluster_it = cloud_vec.begin();
         cluster_it != cloud_vec.end(); ++cluster_it)
    {
        std::stringstream ss;
        ss << cloud_name << "_" << cluster_number;

        CloudData tmp_cloud_data(*cluster_it, ss.str(), cloud_path);
        if (cloud->has_normals())
        {
            tmp_cloud_data.set_normals(*normals_vec.at(cluster_number));
        }

        ret_clouds.push_back(tmp_cloud_data);
        ++cluster_number;
    }
}



void concatinate_meshes(std::vector<CloudData *> cloud_vec, pcl::PolygonMesh &ret_mesh)
{
    // merge meshes
    vtkSmartPointer<vtkAppendPolyData> appendFilter = vtkSmartPointer<vtkAppendPolyData>::New();

    for (std::vector<CloudData*>::iterator cloud_it = cloud_vec.begin(); cloud_it != cloud_vec.end(); ++cloud_it)
    {
        CloudData* cur_cloud_ptr = *cloud_it;
        if (cur_cloud_ptr->has_mesh())
        {
            pcl::PolygonMesh pcl_mesh = *cur_cloud_ptr->get_mesh();
            vtkSmartPointer<vtkPolyData> vtk_mesh = vtkSmartPointer<vtkPolyData>::New();
            pcl::VTKUtils::convertToVTK(pcl_mesh, vtk_mesh);
#if VTK_MAJOR_VERSION <= 5
            appendFilter->AddInput(vtk_mesh);
#else
            appendFilter->AddInputData(vtk_mesh);
#endif
        }
    }
    appendFilter->Update();

    vtkSmartPointer<vtkPolyData> concat_vtk_mesh = appendFilter->GetOutput();
    pcl::VTKUtils::convertToPCL(concat_vtk_mesh, ret_mesh);

}

void concatinate_meshes(std::vector<PolygonMesh> meshes_vec, PolygonMesh &ret_mesh)
{
    // merge meshes
    vtkSmartPointer<vtkAppendPolyData> appendFilter = vtkSmartPointer<vtkAppendPolyData>::New();

    for (std::vector<pcl::PolygonMesh>::iterator meshes_it = meshes_vec.begin(); meshes_it != meshes_vec.end(); ++meshes_it)
    {
        pcl::PolygonMesh pcl_mesh = (*meshes_it);

        vtkSmartPointer<vtkPolyData> vtk_mesh = vtkSmartPointer<vtkPolyData>::New();
        pcl::VTKUtils::convertToVTK(pcl_mesh, vtk_mesh);
#if VTK_MAJOR_VERSION <= 5
        appendFilter->AddInput(vtk_mesh);
#else
        appendFilter->AddInputData(vtk_mesh);
#endif
    }
    appendFilter->Update();

    vtkSmartPointer<vtkPolyData> concat_vtk_mesh = appendFilter->GetOutput();
    pcl::VTKUtils::convertToPCL(concat_vtk_mesh, ret_mesh);
}

void smooth_poly_data_filter_inplace(CloudData* input_cloud,
                                     const double & convergence,
                                     const int & iterations,
                                     const double & relaxation_factor,
                                     const bool & use_feature_edge_smoothing,
                                     const double & feature_angle,
                                     const double & edge_angle,
                                     const bool & use_boundary_smoothing,
                                     QWidget *caller_dialog)
{
    // converte pcl mesh to vtk mesh
    pcl::PolygonMesh pcl_mesh = *(input_cloud->get_mesh());
    vtkSmartPointer<vtkPolyData> vtk_mesh = vtkSmartPointer<vtkPolyData>::New();
    pcl::VTKUtils::convertToVTK(pcl_mesh, vtk_mesh);

    // initialise smooth filter and set input mesh
    vtkSmartPointer<vtkSmoothPolyDataFilter> smoothFilter =
            vtkSmartPointer<vtkSmoothPolyDataFilter>::New();
#if VTK_MAJOR_VERSION <= 5
    smoothFilter->SetInput(vtk_mesh);
#else
    smoothFilter->SetInputData(vtk_mesh);
#endif

    // set filter parameters
    smoothFilter->SetConvergence(convergence);
    smoothFilter->SetNumberOfIterations(iterations);
    smoothFilter->SetRelaxationFactor(relaxation_factor);

    if (use_feature_edge_smoothing)
        smoothFilter->FeatureEdgeSmoothingOn();
    else
        smoothFilter->FeatureEdgeSmoothingOff();

    smoothFilter->SetFeatureAngle(feature_angle);
    smoothFilter->SetEdgeAngle(edge_angle);

    if (use_boundary_smoothing)
        smoothFilter->BoundarySmoothingOn();
    else
        smoothFilter->BoundarySmoothingOff();

    // run filter
    smoothFilter->Update();

    // Update normals on newly smoothed polydata
    vtkSmartPointer<vtkPolyDataNormals> normalGenerator = vtkSmartPointer<vtkPolyDataNormals>::New();
    normalGenerator->SetInputConnection(smoothFilter->GetOutputPort());
    normalGenerator->ComputePointNormalsOn();
    normalGenerator->ComputeCellNormalsOn();
    normalGenerator->Update();

    // get resultant mesh
//    vtkSmartPointer<vtkPolyData> vtk_smoothed_mesh = smoothFilter->GetOutput();
    vtkSmartPointer<vtkPolyData> vtk_smoothed_mesh = normalGenerator->GetOutput();

    // convert vtk mesh back to pcl mesh
    pcl::PolygonMesh ret_mesh;
    pcl::VTKUtils::convertToPCL(vtk_smoothed_mesh, ret_mesh);

    // set resultant mesh
    input_cloud->set_mesh(ret_mesh);
}

void mesh_decimate_pro_inplace(CloudData *input_cloud,
                               const double &target_reduction,
                               const bool &preserve_topology)
{
    // converte pcl mesh to vtk mesh
    pcl::PolygonMesh pcl_mesh = *(input_cloud->get_mesh());
    vtkSmartPointer<vtkPolyData> vtk_mesh = vtkSmartPointer<vtkPolyData>::New();
    pcl::VTKUtils::convertToVTK(pcl_mesh, vtk_mesh);

    // initialise decimate filter and set input mesh
    vtkSmartPointer<vtkDecimatePro> decimate =
        vtkSmartPointer<vtkDecimatePro>::New();
#if VTK_MAJOR_VERSION <= 5
    decimate->SetInput(vtk_mesh);
#else
    decimate->SetInputData(vtk_mesh);
#endif

    decimate->SetTargetReduction(target_reduction); //10% reduction (if there was 100 triangles, now there will be 90)
    if(preserve_topology)
        decimate->PreserveTopologyOn();
    else
        decimate->PreserveTopologyOff();

//    decimate->SplittingOff();
//    decimate->PreSplitMeshOn();
//    decimate->SetSplitAngle(2.0);

    decimate->Update();

    // get resultant mesh
    vtkSmartPointer<vtkPolyData> vtk_decimated_mesh = decimate->GetOutput();

    // convert vtk mesh back to pcl mesh
    pcl::PolygonMesh ret_mesh;
    pcl::VTKUtils::convertToPCL(vtk_decimated_mesh, ret_mesh);

    // set resultant mesh
    input_cloud->set_mesh(ret_mesh);
}

void mesh_quadratic_clustering_inplace(CloudData* input_cloud,
                                       const bool & auto_adjust_number_of_devisions,            //true
                                       const int & number_of_x_divisions,                       //50
                                       const int & number_of_y_divisions,                       //50
                                       const int & number_of_z_divisions,                       //50
                                       const bool & use_feature_edges,                          // false
                                       const double & feature_points_angle,                     // 1.0
                                       const bool &use_internal_triangles,                      // true
                                       QWidget* caller_dialog)
{
    // converte pcl mesh to vtk mesh
    pcl::PolygonMesh pcl_mesh = *(input_cloud->get_mesh());
    vtkSmartPointer<vtkPolyData> vtk_mesh = vtkSmartPointer<vtkPolyData>::New();
    pcl::VTKUtils::convertToVTK(pcl_mesh, vtk_mesh);

    // initialise decimate filter and set input mesh
    vtkSmartPointer<vtkQuadricClustering> decimate =
        vtkSmartPointer<vtkQuadricClustering>::New();
#if VTK_MAJOR_VERSION <= 5
    decimate->SetInput(vtk_mesh);
#else
    decimate->SetInputData(vtk_mesh);
#endif

    if (auto_adjust_number_of_devisions)
    {
        decimate->AutoAdjustNumberOfDivisionsOn();
    }
    else
    {
        decimate->AutoAdjustNumberOfDivisionsOff();
        decimate->SetNumberOfXDivisions(number_of_x_divisions);
        decimate->SetNumberOfYDivisions(number_of_y_divisions);
        decimate->SetNumberOfZDivisions(number_of_z_divisions);
    }

    if (use_feature_edges)
    {
        decimate->UseFeatureEdgesOn();
        decimate->SetFeaturePointsAngle(feature_points_angle);
    }
    else
    {
        decimate->UseFeatureEdgesOff();
    }

    if (use_internal_triangles)
    {
        decimate->UseInternalTrianglesOn();
    }
    else
    {
        decimate->UseInternalTrianglesOff();
    }

    decimate->Update();

    // get resultant mesh
    vtkSmartPointer<vtkPolyData> vtk_decimated_mesh = decimate->GetOutput();

    // convert vtk mesh back to pcl mesh
    pcl::PolygonMesh ret_mesh;
    pcl::VTKUtils::convertToPCL(vtk_decimated_mesh, ret_mesh);

    // set resultant mesh
    input_cloud->set_mesh(ret_mesh);
}

/**
  * vtk meshQuality:
  *  http://www.vtk.org/Wiki/VTK/Examples/Cxx/PolyData/MeshQuality
  *  http://www.vtk.org/doc/nightly/html/classvtkMeshQuality.html
  *  triangle quality measure to area explanation: https://uk.mathworks.com/help/pde/ug/pdetriq.html

 * @brief get_mesh_quality
 * @param input_cloud
 * @return
 */
double get_mesh_quality(CloudData *input_cloud)
{
    // converte pcl mesh to vtk mesh
    pcl::PolygonMesh pcl_mesh = *(input_cloud->get_mesh());
    vtkSmartPointer<vtkPolyData> vtk_mesh = vtkSmartPointer<vtkPolyData>::New();
    pcl::VTKUtils::convertToVTK(pcl_mesh, vtk_mesh);

    // create vtk mesh quality filter
    vtkSmartPointer<vtkMeshQuality> qualityFilter =
            vtkSmartPointer<vtkMeshQuality>::New();
#if VTK_MAJOR_VERSION <= 5
    qualityFilter->SetInputConnection(vtk_mesh->GetProducerPort());
#else
    qualityFilter->SetInputData(vtk_mesh);
#endif

    // get the 'triangle quality measure to area' quality measure
    qualityFilter->SetTriangleQualityMeasureToArea();
    qualityFilter->Update();

    vtkSmartPointer<vtkDoubleArray> qualityArray =
            vtkDoubleArray::SafeDownCast(qualityFilter->GetOutput()->GetCellData()->GetArray("Quality"));

//    std::cout << "There are " << qualityArray->GetNumberOfTuples() << " values." << std::endl;


    double mean_quality;
    for(vtkIdType i = 0; i < qualityArray->GetNumberOfTuples(); i++)
    {
        double val = qualityArray->GetValue(i);
//        std::cout << "value " << i << " : " << val << std::endl;
        mean_quality += val;
    }

    mean_quality /= qualityArray->GetNumberOfTuples();
    return mean_quality;

}


template<typename filterType, typename cloudType>
void opt_cluster_cloud_processing(CloudData* input_cloud, QWidget* caller_dialog, filterType & filter, const bool & individual_clusters,
                                  const int & min_cluster_size, cloudType & ret_cloud)
{
    CustomQProgressDialog progress(input_cloud, individual_clusters, caller_dialog);

    std::vector<pcl::PointIndices> point_indices_tmp = input_cloud->get_cluster_indices();
    if (individual_clusters && input_cloud->has_clusters())
    {
        int counter = 0;

        for (std::vector<pcl::PointIndices>::iterator point_indices = point_indices_tmp.begin();
             point_indices != point_indices_tmp.end(); ++ point_indices)
        {
            cloudType ret_cloud_cluster;
            pcl::PointIndices cur_point_indices = *point_indices;

            progress.update_progress(counter);

            if (progress.wasCanceled())
                break;

            pcl::IndicesPtr current_indices= boost::make_shared<std::vector<int> >(cur_point_indices.indices);

            if (point_indices->indices.size() > min_cluster_size)
            {
                filter.setIndices(current_indices);
                filter.process(ret_cloud_cluster);

                for (typename cloudType::iterator it = ret_cloud_cluster.begin(); it != ret_cloud_cluster.end(); ++it)
                {
                    ret_cloud.push_back(*it);
                }
            }
            counter++;
        }
    }
    else
    {
        // apply filter
        filter.process(ret_cloud);
    }
    progress.end_progress();
}

template<typename filterType, typename cloudType>
void opt_cluster_cloud_filtering(CloudData* input_cloud, QWidget* caller_dialog, filterType filter, const bool & individual_clusters,
                                 const int & min_cluster_size, cloudType & ret_cloud)
{
    CustomQProgressDialog progress(input_cloud, individual_clusters, caller_dialog);

    std::vector<pcl::PointIndices> point_indices_tmp = input_cloud->get_cluster_indices();
    if (individual_clusters && input_cloud->has_clusters())
    {
        int counter = 0;

        for (std::vector<pcl::PointIndices>::iterator point_indices = point_indices_tmp.begin();
             point_indices != point_indices_tmp.end(); ++ point_indices)
        {
            cloudType ret_cloud_cluster;
            pcl::PointIndices cur_point_indices = *point_indices;

            progress.update_progress(counter);

            if (progress.wasCanceled())
                break;

            pcl::IndicesPtr current_indices= boost::make_shared<std::vector<int> >(cur_point_indices.indices);

            if (point_indices->indices.size() > min_cluster_size)
            {
                filter.setIndices(current_indices);
                filter.filter(ret_cloud_cluster);

                for (typename cloudType::iterator it = ret_cloud_cluster.begin(); it != ret_cloud_cluster.end(); ++it)
                {
                    ret_cloud.push_back(*it);
                }
            }
            counter++;
        }
    }
    else
    {
        // apply filter
        filter.filter(ret_cloud);
    }
    progress.end_progress();
}

template<typename filterType, typename cloudType>
void opt_cluster_cloud_computing(CloudData* input_cloud, QWidget* caller_dialog, filterType filter, const bool & individual_clusters,
                                 const int & min_cluster_size, cloudType & ret_cloud)
{
    CustomQProgressDialog progress(input_cloud, individual_clusters, caller_dialog);

    std::vector<pcl::PointIndices> point_indices_tmp = input_cloud->get_cluster_indices();
    if (individual_clusters && input_cloud->has_clusters())
    {
        int counter = 0;

        for (std::vector<pcl::PointIndices>::iterator point_indices = point_indices_tmp.begin();
             point_indices != point_indices_tmp.end(); ++ point_indices)
        {
            cloudType ret_cloud_cluster;
            pcl::PointIndices cur_point_indices = *point_indices;

            progress.update_progress(counter);

            if (progress.wasCanceled())
                break;

            pcl::IndicesPtr current_indices= boost::make_shared<std::vector<int> >(cur_point_indices.indices);

            if (point_indices->indices.size() > min_cluster_size)
            {
                filter.setIndices(current_indices);
                filter.compute(ret_cloud_cluster);

                for (typename cloudType::iterator it = ret_cloud_cluster.begin(); it != ret_cloud_cluster.end(); ++it)
                {
                    ret_cloud.push_back(*it);
                }
            }
            counter++;
        }
    }
    else
    {
        // apply filter
        filter.compute(ret_cloud);
    }
    progress.end_progress();
}

template<typename filterType, typename cloudType>
void opt_cluster_cloud_extraction(CloudData* input_cloud, QWidget* caller_dialog, filterType filter, const bool & individual_clusters,
                                  const int & min_cluster_size, cloudType & ret_cloud)
{
    CustomQProgressDialog progress(input_cloud, individual_clusters, caller_dialog);

    std::vector<pcl::PointIndices> point_indices_tmp = input_cloud->get_cluster_indices();

    if (individual_clusters && input_cloud->has_clusters())
    {
        int counter = 0;

        for (std::vector<pcl::PointIndices>::iterator point_indices = point_indices_tmp.begin();
             point_indices != point_indices_tmp.end(); ++ point_indices)
        {
            cloudType ret_cloud_cluster;
            pcl::PointIndices cur_point_indices = *point_indices;

            progress.update_progress(counter);

            if (progress.wasCanceled())
                break;

            pcl::IndicesPtr current_indices= boost::make_shared<std::vector<int> >(cur_point_indices.indices);

            if (point_indices->indices.size() > min_cluster_size)
            {
                filter.setIndices(current_indices);
                filter.extract(ret_cloud_cluster);

                for (typename cloudType::iterator it = ret_cloud_cluster.begin(); it != ret_cloud_cluster.end(); ++it)
                {
                    ret_cloud.push_back(*it);
                }
            }
            counter++;
        }
    }
    else
    {
        // apply filter
        filter.extract(ret_cloud);
    }
}

template<typename filterType, typename cloudType>
void opt_cluster_cloud_reconstruction(CloudData* input_cloud, QWidget* caller_dialog, filterType & filter, const bool & individual_clusters,
                                      const int & min_cluster_size, cloudType & ret_cloud)
{
    CustomQProgressDialog progress(input_cloud, individual_clusters, caller_dialog);

    std::vector<pcl::PointIndices> point_indices_tmp = input_cloud->get_cluster_indices();
    if (individual_clusters && input_cloud->has_clusters())
    {
        int counter = 0;

        for (std::vector<pcl::PointIndices>::iterator point_indices = point_indices_tmp.begin();
             point_indices != point_indices_tmp.end(); ++ point_indices)
        {
            cloudType ret_cloud_cluster;
            pcl::PointIndices cur_point_indices = *point_indices;

            progress.update_progress(counter);

            if (progress.wasCanceled())
                break;

            pcl::IndicesPtr current_indices= boost::make_shared<std::vector<int> >(cur_point_indices.indices);

            if (point_indices->indices.size() > min_cluster_size)
            {
                filter.setIndices(current_indices);
                filter.reconstruct(ret_cloud_cluster);

                for (typename cloudType::iterator it = ret_cloud_cluster.begin(); it != ret_cloud_cluster.end(); ++it)
                {
                    ret_cloud.push_back(*it);
                }
            }
            counter++;
        }
    }
    else
    {
        // apply filter
        filter.reconstruct(ret_cloud);
    }
    progress.end_progress();
}

template<typename filterType>
void opt_cluster_mesh_reconstruction(CloudData* input_cloud, QWidget* caller_dialog, filterType filter, const bool & individual_clusters,
                                     const int & min_cluster_size, pcl::PolygonMesh & ret_mesh)
{
    CustomQProgressDialog progress(input_cloud, individual_clusters, caller_dialog);

    //    std::vector<pcl::PointIndices> point_indices_tmp = input_cloud->get_cluster_indices();
    if (individual_clusters && input_cloud->has_clusters())
    {
        int counter = 0;

        std::vector<pcl::PointCloud<pcl::PointXYZRGB>::Ptr> ret_cloud_clusters;
        extract_clusters_from_cloud(input_cloud, ret_cloud_clusters);

        std::vector<pcl::PointCloud<pcl::Normal>::Ptr> ret_normals_clusters;
        extract_normal_clusters_from_cloud(input_cloud, ret_normals_clusters);

        std::vector<pcl::PolygonMesh> tmp_meshes;

        for (std::vector<pcl::PointCloud<pcl::PointXYZRGB>::Ptr>::iterator cloud_it = ret_cloud_clusters.begin();
             cloud_it != ret_cloud_clusters.end(); ++cloud_it)
        {
            pcl::PolygonMesh ret_mesh_cluster;

            pcl::PointCloud<pcl::PointNormal>::Ptr cloud_with_normals (new pcl::PointCloud<pcl::PointNormal>);
            pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_xyz (new pcl::PointCloud<pcl::PointXYZ>);
            Process::convert_cloudXYZRGB_2_cloudXYZ(*(*cloud_it), *cloud_xyz);
            concatenateFields(*cloud_xyz, *ret_normals_clusters.at(counter), *cloud_with_normals);

            progress.update_progress(counter);
            if (progress.wasCanceled())
                break;

            if ((*cloud_it)->size() > min_cluster_size)
            {
                filter.setInputCloud(cloud_with_normals);

                filter.reconstruct(ret_mesh_cluster);

                tmp_meshes.push_back(ret_mesh_cluster);
            }
            counter++;
        }

        concatinate_meshes(tmp_meshes, ret_mesh);
    }
    else
    {
        pcl::PointCloud<pcl::PointNormal>::Ptr cloud_with_normals (new pcl::PointCloud<pcl::PointNormal>);
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_xyz (new pcl::PointCloud<pcl::PointXYZ>);
        Process::convert_cloudXYZRGB_2_cloudXYZ(*input_cloud->get_cloud(), *cloud_xyz);
        concatenateFields(*cloud_xyz, *input_cloud->get_normals(), *cloud_with_normals);
        // apply filter
        filter.setInputCloud(cloud_with_normals);
        filter.reconstruct(ret_mesh);
    }
    progress.end_progress();
}

template<typename filterType>
void opt_cluster_mesh_reconstruction_no_normals(CloudData* input_cloud, QWidget* caller_dialog, filterType filter, const bool & individual_clusters,
                                                const int & min_cluster_size, pcl::PolygonMesh & ret_mesh)
{
    CustomQProgressDialog progress(input_cloud, individual_clusters, caller_dialog);

    std::vector<pcl::PointIndices> point_indices_tmp = input_cloud->get_cluster_indices();
    if (individual_clusters && input_cloud->has_clusters())
    {
        int counter = 0;

        // DEBUG
        //        std::vector<pcl::PointCloud<pcl::PointXYZRGB>::Ptr> ret_cloud_clusters;
        //        extract_clusters_from_cloud(input_cloud, ret_cloud_clusters);

        //        std::vector<double> Qs;
        //        double max = -1;
        //        double min = 99999999999;
        //        for (int i=0; i<ret_cloud_clusters.size(); ++i)
        //        {
        //            pcl::PointCloud<pcl::PointXYZRGB>::Ptr cur_cloud_ptr = ret_cloud_clusters.at(i);
        //            int size = cur_cloud_ptr->size();
        //            pcl::PointXYZRGB minPt, maxPt;
        //            pcl::getMinMax3D(*cur_cloud_ptr, minPt, maxPt);
        //            double a = std::abs(maxPt.x - minPt.x);
        //            double b = std::abs(maxPt.y - minPt.y);
        //            double c = std::abs(maxPt.z - minPt.z);
        //            double V = a*b*c;
        //            double Q =  V / (double) size;
        //            if (Q>=max)
        //                max = Q;
        //            if (Q<=min)
        //                min = Q;

        //            Qs.push_back(Q);

        ////            pcl::ConvexHull<pcl::PointXYZRGB> cHull;
        ////            pcl::PointCloud<pcl::PointXYZRGB> cHull_points;
        ////            cHull.setInputCloud(cur_cloud_ptr);
        ////            cHull.setDimension(3);
        ////            cHull.reconstruct(cHull_points);
        ////            double cHull_V = cHull.getTotalVolume();

        //            std::cout << ">>>>>> VOLUME: " << V << std::endl;
        ////            std::cout << "> HULL VOLUME: " << cHull_V << " given points in cHull: " << cHull_points.size() <<  std::endl;
        //            std::cout << ">>>>>>   SIZE: " << size << std::endl;
        //            std::cout << ">>>> QUOTIENT: " << Q << std::endl;
        //            std::cout << "min: (" << minPt.x << "," << minPt.y << "," << minPt.y << ") and max: (" << maxPt.x << "," << maxPt.y << "," << maxPt.y << ")" << std::endl;
        //            std::cout << "a: " << a << " b: " << b << " c: " << c << std::endl;
        //        }
        //        std::vector<double> Qs_n;
        //        for (int i=0; i<Qs.size(); ++i)
        //        {
        //           double tmp = Qs.at(i)/max;
        //           Qs_n.push_back(tmp);
        //           std::cout << ">>>>>> Q normalised: " << tmp << std::endl;
        //        }
        //        double maxAlpha = 0.1;
        //        double minAlpha = 0.01;
        //        double m = maxAlpha - minAlpha;
        //        std::vector<double> alphas;
        //        for (int i=0; i<Qs_n.size(); ++i)
        //        {
        //            double f = m*Qs_n.at(i) + minAlpha;
        //            std::cout << ">>>>>>   alpha: " << f << std::endl;
        //            alphas.push_back(f);
        //        }
        // END

        std::vector<pcl::PolygonMesh> tmp_meshes;

        for (std::vector<pcl::PointIndices>::iterator point_indices = point_indices_tmp.begin();
             point_indices != point_indices_tmp.end(); ++ point_indices)
        {
            pcl::PolygonMesh ret_mesh_cluster;
            pcl::PointIndices cur_point_indices = *point_indices;

            progress.update_progress(counter);

            if (progress.wasCanceled())
                break;

            pcl::IndicesPtr current_indices= boost::make_shared<std::vector<int> >(cur_point_indices.indices);

            if (point_indices->indices.size() > min_cluster_size)
            {
                filter.setIndices(current_indices);
                // DEBUG
                //                filter.setAlpha(alphas.at(counter));
                // END
                filter.reconstruct(ret_mesh_cluster);
                tmp_meshes.push_back(ret_mesh_cluster);
            }
            counter++;
        }

        concatinate_meshes(tmp_meshes, ret_mesh);
    }
    else
    {
        filter.reconstruct(ret_mesh);
    }
    progress.end_progress();
}


// **********************************************************
// ******************** PCL Functions ***********************
// **********************************************************

void extract_normals_inplace (CloudData* input_cloud,  const double search_radius, const int number_of_nearest_neighbors,
                              const bool use_search_radius, const bool set_view_point, const bool individual_clusters, QWidget* caller_dialog)
{
    // Create the normal estimation class, and pass the input dataset to it
    pcl::NormalEstimation<pcl::PointXYZRGB, pcl::Normal> n;
    n.setInputCloud (input_cloud->get_cloud());

    // Create an empty kdtree representation, and pass it to the normal estimation object.
    // Its content will be filled inside the object, based on the given input dataset (as no other search surface is given).
    pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGB>);
    n.setSearchMethod (tree);

    if (use_search_radius)
    {
        // Use all neighbors in a sphere of radius 3cm
        n.setRadiusSearch(search_radius);   // 0.03
    }
    else
    {
        n.setKSearch(number_of_nearest_neighbors);  //20
    }

    if (set_view_point)
    {
        // normals shoud direct towards the view point (up)
        n.setViewPoint(0.0f, 0.0f, 0.0f);
        std::cout << "view point to zero" << std::endl;
    }
    else
    {
        Eigen::Vector4f centroid;
        pcl::compute3DCentroid (*input_cloud->get_cloud(), centroid);
        n.setViewPoint (centroid[0], centroid[1], centroid[2]);

        std::cout << "view point (" << centroid[0] << "," << centroid[1] << "," << centroid[2] << ")" << std::endl;
    }

    pcl::PointCloud<pcl::Normal> ret_normals;

    opt_cluster_cloud_computing(input_cloud, caller_dialog, n, individual_clusters, 0, ret_normals);

    // normals are pointing to the inside of the centroid - make them point outwards...
    if (!set_view_point)
    {
        for (size_t i = 0; i < ret_normals.size (); ++i)
          {
            ret_normals.points[i].normal_x *= -1;
            ret_normals.points[i].normal_y *= -1;
            ret_normals.points[i].normal_z *= -1;
          }
    }

    input_cloud->set_normals(ret_normals);

}

void split_square(CloudData * cloud, float x0, float y0, float x1, float y1, pcl::PointIndices & indices)
{
    pcl::PassThrough<pcl::PointXYZRGB> filter;
    filter.setInputCloud(cloud->get_cloud());
    filter.setFilterFieldName("x");
    filter.setFilterLimits(x0, x1);
    pcl::IndicesPtr x_indices_tmp (new std::vector <int>);
    filter.filter(*x_indices_tmp);
    filter.setIndices(x_indices_tmp);
    pcl::IndicesPtr y_indices_tmp (new std::vector <int>);
    filter.setFilterFieldName("y");
    filter.setFilterLimits(y0, y1);
    filter.filter(*y_indices_tmp);
    indices.indices = *y_indices_tmp;
}

void check_subsplit(float x0, float y0, float x1, float y1, int max_chunk_size, CloudData* cloud, std::vector<pcl::PointIndices> & indices)
{
    pcl::PointIndices tmp_indices;
    split_square(cloud, x0, y0, x1, y1, tmp_indices);
    if (tmp_indices.indices.size() > max_chunk_size)
    {
        recursive_split(x0, y0, x1, y1, max_chunk_size, cloud, indices);
    }
    else
    {
        if (tmp_indices.indices.size() > 0)
        {
            indices.push_back(tmp_indices);
        }
    }
}

void recursive_split(float x0, float y0, float x1, float y1, int max_chunk_size, CloudData* cloud, std::vector<pcl::PointIndices> & indices)
{
    float x_half = (x0+x1)/2.0;
    float y_half = (y0+y1)/2.0;

    check_subsplit(x0, y0, x_half, y_half, max_chunk_size, cloud, indices);
    check_subsplit(x0, y_half, x_half, y1, max_chunk_size, cloud, indices);
    check_subsplit(x_half, y0, x1, y_half, max_chunk_size, cloud, indices);
    check_subsplit(x_half, y_half, x1, y1, max_chunk_size, cloud, indices);
}

void extract_chunks_inplace(CloudData *input_cloud, const int p_split, bool enforce_reduction, int max_chunk_size, QWidget* caller_dialog)
{
    pcl::PointXYZRGB min_point;
    pcl::PointXYZRGB max_point;

    pcl::getMinMax3D(*(input_cloud->get_cloud()), min_point, max_point);

    float x_min = min_point.x;
    float x_max = max_point.x;
    float y_min = min_point.y;
    float y_max = max_point.y;

    float x_offset = (x_max - x_min) * float(p_split) / 100.0;
    float y_offset = (y_max - y_min) * float(p_split) / 100.0;

    // conditional passthrough filter for all intervals
    std::vector<pcl::PointIndices> split_indices;

    float x_0 = x_min;
    float x_1 = x_0;
    while (x_1 != x_max)
    {
        x_1 = (x_0+x_offset >= x_max) ? x_max : x_0+x_offset;
        // filter given the x interval
        pcl::PassThrough<pcl::PointXYZRGB> pass;
        pass.setInputCloud(input_cloud->get_cloud());
        pass.setFilterFieldName("x");
        pass.setFilterLimits(x_0, x_1);
        pcl::IndicesPtr x_indices_tmp (new std::vector <int>);
        pass.filter(*x_indices_tmp);
        pass.setIndices(x_indices_tmp);

        float y_0 = y_min;
        float y_1 = y_0;
        while (y_1 != y_max)
        {
            y_1 = (y_0+y_offset >= y_max) ? y_max : y_0+y_offset;
            pcl::IndicesPtr y_indices_tmp (new std::vector <int>);
            pass.setFilterFieldName("y");
            pass.setFilterLimits(y_0, y_1);
            pass.filter(*y_indices_tmp);

            int cur_size = y_indices_tmp->size();
            if (enforce_reduction && cur_size > max_chunk_size)
            {
                recursive_split(x_0, y_0, x_1, y_1, max_chunk_size, input_cloud, split_indices);
            }
            else
            {
                // push point indices into cloud
                pcl::PointIndices point_indices;
                point_indices.indices = *y_indices_tmp;
                if (point_indices.indices.size() > 0)
                {
                    split_indices.push_back(point_indices);
                }
            }
            y_0=y_1;
        }
        x_0 = x_1;
    }
    input_cloud->set_cluster_indices(split_indices);
}

void circular_removal_inplace(CloudData *cloud, const float x_center, const float y_center, const float distance)
{
    pcl::PointXYZRGB center_point;
    center_point.x = x_center;
    center_point.y = y_center;
    center_point.z = 0.0f;

    pcl::PointCloud<pcl::PointXYZRGB> c = *(cloud->get_cloud());

    pcl::PointCloud<pcl::PointXYZRGB> ret_cloud;

    for (pcl::PointCloud<pcl::PointXYZRGB>::iterator cloud_it = c.begin();
         cloud_it != c.end(); ++cloud_it)
    {
        pcl::PointXYZRGB cur_point = *cloud_it;
        float cur_dist = Process::dist_x_y(cur_point, center_point);
        if( cur_dist <= distance)
        {
            ret_cloud.push_back(cur_point);
        }
    }

    cloud->set_cloud(ret_cloud);
}

void circular_clustering_inplace(CloudData *input_cloud, const float x_center, const float y_center, const float distance)
{
    pcl::PointXYZRGB center_point;
    center_point.x = x_center;
    center_point.y = y_center;
    center_point.z = 0.0f;

    pcl::PointCloud<pcl::PointXYZRGB> c = *(input_cloud->get_cloud());

    std::vector<pcl::PointIndices> split_indices;
    std::vector<int> inside;
    std::vector<int> outside;

    //    pcl::PointCloud<pcl::PointXYZRGB> ret_cloud;

    int counter = 0;
    for (pcl::PointCloud<pcl::PointXYZRGB>::iterator cloud_it = c.begin();
         cloud_it != c.end(); ++cloud_it)
    {
        pcl::PointXYZRGB cur_point = *cloud_it;
        float cur_dist = Process::dist_x_y(cur_point, center_point);
        if( cur_dist <= distance)
        {
            inside.push_back(counter);
        }
        else
        {
            outside.push_back(counter);
        }
        ++counter;
    }
    pcl::PointIndices point_indices_inside;
    point_indices_inside.indices = inside;
    pcl::PointIndices point_indices_outside;
    point_indices_outside.indices = outside;

    split_indices.push_back(point_indices_inside);
    split_indices.push_back(point_indices_outside);

    input_cloud->set_cluster_indices(split_indices);
}

void filter_color_inplace(CloudData *cloud, const int red_lower, const int red_upper, const int green_lower, const int green_upper, const int blue_lower,
                          const int blue_upper, const bool inside_red, const bool inside_green, const bool inside_blue)
{
    pcl::PointCloud<pcl::PointXYZRGB> ret_cloud;
    pcl::PointCloud<pcl::PointXYZRGB> cur_cloud = *cloud->get_cloud();

    for (pcl::PointCloud<pcl::PointXYZRGB>::iterator cloud_it = cur_cloud.begin();
         cloud_it != cur_cloud.end(); ++cloud_it)
    {
        pcl::PointXYZRGB cur_point = *cloud_it;
        int cur_red = int(cur_point.r);
        int cur_green = int(cur_point.g);
        int cur_blue = int(cur_point.b);

        bool red_ok = false;
        bool green_ok = false;
        bool blue_ok = false;

        if ((inside_red && (cur_red >= red_lower && cur_red <= red_upper)) ||
                (!inside_red && (cur_red < red_lower || cur_red > red_upper)))
        {
            red_ok = true;
        }

        if ((inside_green && (cur_green >= green_lower && cur_green <= green_upper)) ||
                (!inside_green && (cur_green < green_lower || cur_green > green_upper)))
        {
            green_ok = true;
        }

        if ((inside_blue && (cur_blue >= blue_lower && cur_blue <= blue_upper)) ||
                (!inside_blue && (cur_blue < blue_lower || cur_blue > blue_upper)))
        {
            blue_ok = true;
        }

        if (red_ok && green_ok && blue_ok)
        {
            ret_cloud.push_back(cur_point);
        }
    }

    cloud->set_cloud(ret_cloud);
}


void conditional_vertex_filter_inplace(CloudData *input_cloud, const double lower_limit, const double upper_limit, const std::string axis)
{
    pcl::PassThrough<pcl::PointXYZRGB> pass;
    pcl::PointCloud<pcl::PointXYZRGB> ret_cloud;
    pass.setInputCloud (input_cloud->get_cloud());
    pass.setFilterFieldName (axis);
    pass.setFilterLimits (lower_limit, upper_limit);  // -1.5, -1.15
    // pass.setFilterLimitsNegative(true);
    pass.filter (ret_cloud);
    input_cloud->set_cloud(ret_cloud);
    std::cout << "Cloud filtered by conditionally height in ["
              << lower_limit << "," << upper_limit << "]! "
              << input_cloud->get_cloud_size()
              << " data points remaining."
              << std::endl;
}



void downsample_cloud_inplace(CloudData *input_cloud, const float leaf_x, const float leaf_y, const float leaf_z, const bool use_min_number_of_points,
                              const int min_number_of_points, const bool use_filter_limits, const double min_limit, const double max_limit,
                              const bool individual_clusters,const bool enforce_reduction, const int min_reduction_percentage, const double reduction_increment,  QWidget* caller_dialog)
{
    int cloud_size_before = input_cloud->get_cloud_size();
    double max_allowed_cloud_size = double(cloud_size_before) * double(min_reduction_percentage)/100.0;
    pcl::VoxelGrid<pcl::PointXYZRGB> sor;
    sor.setInputCloud (input_cloud->get_cloud());
    sor.setLeafSize (leaf_x, leaf_y, leaf_z);

    if (use_min_number_of_points)
    {
        sor.setMinimumPointsNumberPerVoxel(min_number_of_points);
    }
    if (use_filter_limits)
    {
        sor.setFilterLimits(min_limit, max_limit);
    }

    pcl::PointCloud<pcl::PointXYZRGB> ret_cloud;

    opt_cluster_cloud_filtering(input_cloud, caller_dialog, sor, individual_clusters, 0, ret_cloud);

    if (enforce_reduction)
    {
        float new_x = leaf_x;
        float new_y = leaf_y;
        float new_z = leaf_z;
        while (ret_cloud.size() > int(max_allowed_cloud_size))
        {
            new_x += reduction_increment;
            new_y += reduction_increment;
            new_z += reduction_increment;

            sor.setLeafSize (new_x, new_y, new_z);
            opt_cluster_cloud_filtering(input_cloud, caller_dialog, sor, individual_clusters, 0, ret_cloud);
            std::cout << "new cloud size: " << ret_cloud.size() << " > " << max_allowed_cloud_size << " given leaf_x: " << new_x << std::endl;

        }
    }

    input_cloud->set_cloud(ret_cloud);

    std::cout << "PointCloud after downsampling: " << ret_cloud.width * ret_cloud.height
              << " data points" << std::endl;
}


void extract_clusters_from_cloud_inplace(CloudData *input_clouds, const double cluster_tolerance, const bool use_min_cluster, const int min_cluster_size, const bool use_max_cluster, const int max_cluster_size)
{
    // Creating the KdTree object for the search method of the extraction
    pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGB>);
    tree->setInputCloud (input_clouds->get_cloud());

    std::vector<pcl::PointIndices> cluster_indices;
    pcl::EuclideanClusterExtraction<pcl::PointXYZRGB> ec;
    ec.setClusterTolerance (cluster_tolerance); // 2cm  0.02
    if(use_min_cluster)
        ec.setMinClusterSize (min_cluster_size);    //100
    if(use_max_cluster)
        ec.setMaxClusterSize (max_cluster_size);  //25000
    ec.setSearchMethod (tree);
    ec.setInputCloud (input_clouds->get_cloud());
    ec.extract (cluster_indices);

    input_clouds->set_cluster_indices(cluster_indices);

}

void progressive_morphological_filter_inplace(CloudData *input_cloud, const int max_window_size, const float slope, const float initial_distance,
                                              const float max_distance, const float cell_size, const float base, const bool exponential,
                                              const bool individual_cluster, const bool merge_resultant_cluster, QWidget* caller_dialog)
{
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud = input_cloud->get_cloud();

    // Create the filtering object
    pcl::ProgressiveMorphologicalFilter<pcl::PointXYZRGB> pmf;

    pmf.setInputCloud (cloud);
    // SET WINDOW SIZE:
    //  non exponential: w(k) = 2kb + 1
    //      exponential: w(k) = 2b^(k) + 1
    //  k = k-th iteration (k = 1,2,3,...,M)
    //  b = base
    // max window size = 2Mb + 1 (non exponential) or 2b^(M) + 1 (exponential)
    pmf.setExponential(exponential);
    pmf.setBase(base);   // 2.0f
    //    pmf.setMaxWindowSize(8); // 2Mb + 1  with  M = 20 and b = 1
    pmf.setMaxWindowSize (max_window_size);


    // ELEVATION THRESHOLD:
    //              dh0                     if w(k) <= 3
    // dhT(k) = {   s(w(k)-w(k-1))c+dh0     if w(k) > 3
    //              dhmax                   if dhT(k) > dhmax
    // s = slope
    // c = cell size
    // dh0 = initiale distance
    // dhmax = max distance
    pmf.setSlope (slope);
    pmf.setCellSize(cell_size);  // 1.0f
    pmf.setInitialDistance (initial_distance);
    pmf.setMaxDistance (max_distance);

    std::vector<pcl::PointIndices> cluster_indices;

    CustomQProgressDialog progress(input_cloud, individual_cluster, caller_dialog);

    std::vector<pcl::PointIndices> point_indices_tmp = input_cloud->get_cluster_indices();


    if (individual_cluster && input_cloud->has_clusters())
    {
        int counter = 0;

        for (std::vector<pcl::PointIndices>::iterator point_indices = point_indices_tmp.begin();
             point_indices != point_indices_tmp.end(); ++ point_indices)
        {
            //             cloudType ret_cloud_cluster;
            pcl::PointIndices cur_point_indices = *point_indices;

            progress.update_progress(counter);

            if (progress.wasCanceled())
                break;

            pcl::IndicesPtr current_indices= boost::make_shared<std::vector<int> >(cur_point_indices.indices);

            if (point_indices->indices.size() > 0)
            {
                pcl::PointIndicesPtr ground (new pcl::PointIndices);
                pmf.setIndices(current_indices);
                pmf.extract(ground->indices);

                //                 for (typename cloudType::iterator it = ret_cloud_cluster.begin(); it != ret_cloud_cluster.end(); ++it)
                //                 {
                //                     ret_cloud.push_back(*it);
                //                 }
                cluster_indices.push_back(*ground);
            }
            counter++;
        }
    }
    else
    {
        pcl::PointIndicesPtr ground (new pcl::PointIndices);
        // apply filter
        pmf.extract(ground->indices);

        cluster_indices.push_back(*ground);
    }


    input_cloud->set_cluster_indices(cluster_indices);

    if (merge_resultant_cluster)
    {
        merge_clusters(input_cloud, *input_cloud);
    }

    std::cout << "Number of clusters found: " << input_cloud->get_number_of_clusters() << std::endl;
}


void mls_upsampling_inplace(CloudData *input_cloud, const double search_radius, const bool ponlynomial_fit, const int polynomial_order,
                            const double upsampling_radius, const double upsampling_step_size, const int upsampling_method,
                            bool individual_clusters, QWidget* caller_dialog)
{
    pcl::MovingLeastSquares<pcl::PointXYZRGB, pcl::PointXYZRGB> mls;
    mls.setInputCloud (input_cloud->get_cloud());

    mls.setSearchRadius (search_radius);         //0.01
    mls.setPolynomialFit (ponlynomial_fit);        //true
    mls.setPolynomialOrder (polynomial_order);         //2
    switch(upsampling_method) {
    case 0: mls.setUpsamplingMethod (pcl::MovingLeastSquares<pcl::PointXYZRGB, pcl::PointXYZRGB>::NONE); break;
    case 1: mls.setUpsamplingMethod (pcl::MovingLeastSquares<pcl::PointXYZRGB, pcl::PointXYZRGB>::DISTINCT_CLOUD); break;
    case 2: mls.setUpsamplingMethod (pcl::MovingLeastSquares<pcl::PointXYZRGB, pcl::PointXYZRGB>::SAMPLE_LOCAL_PLANE); break;
    case 3: mls.setUpsamplingMethod (pcl::MovingLeastSquares<pcl::PointXYZRGB, pcl::PointXYZRGB>::RANDOM_UNIFORM_DENSITY); break;
    case 4: mls.setUpsamplingMethod (pcl::MovingLeastSquares<pcl::PointXYZRGB, pcl::PointXYZRGB>::VOXEL_GRID_DILATION); break;
    }
    //  mls.setUpsamplingMethod (pcl::MovingLeastSquares<T, T>::SAMPLE_LOCAL_PLANE);
    mls.setUpsamplingRadius (upsampling_radius);    //0.005
    mls.setUpsamplingStepSize (upsampling_step_size);  //0.003

    pcl::PointCloud<pcl::PointXYZRGB> ret_cloud;

    opt_cluster_cloud_processing(input_cloud, caller_dialog, mls, individual_clusters, 0, ret_cloud);
    input_cloud->set_cloud(ret_cloud);

    std::cout << "PointCloud after upsampling: " << ret_cloud.width * ret_cloud.height
              << " data points (" << pcl::getFieldsList (ret_cloud) << ")." << std::endl;

}

void mls_upsampling_inplace_1(CloudData *input_cloud,
                              const int sampling_method,
                              const bool use_search_radius,
                              const double search_radius,
                              const double sqrt_gaussian_neighor,
                              const bool polynomial_fit,
                              const int polynomial_order,
                              const double upsampling_radius,
                              const double upsampling_step_size,
                              const int desired_num_of_points_in_radius,
                              const int dilation_iterations,
                              const float dilation_voxel_size,
                              const bool calculate_normals,
                              const bool individual_cluster,
                              QWidget *caller_dialog)
{
    MovingLeastSquaresNormals mls;
    mls.setInputCloud (input_cloud->get_cloud());

    switch(sampling_method) {
    case 0: mls.setUpsamplingMethod (pcl::MovingLeastSquares<pcl::PointXYZRGB, pcl::PointXYZRGB>::NONE); break;
    case 1: mls.setUpsamplingMethod (pcl::MovingLeastSquares<pcl::PointXYZRGB, pcl::PointXYZRGB>::SAMPLE_LOCAL_PLANE); break;
    case 2: mls.setUpsamplingMethod (pcl::MovingLeastSquares<pcl::PointXYZRGB, pcl::PointXYZRGB>::RANDOM_UNIFORM_DENSITY); break;
    case 3: mls.setUpsamplingMethod (pcl::MovingLeastSquares<pcl::PointXYZRGB, pcl::PointXYZRGB>::VOXEL_GRID_DILATION); break;
    }

    // general parameters
    if (use_search_radius)
        mls.setSearchRadius (search_radius);
    mls.setSqrGaussParam(sqrt_gaussian_neighor);
    if (polynomial_fit)
    {
        mls.setPolynomialFit (polynomial_fit);
        mls.setPolynomialOrder (polynomial_order);
    }

    // sampling method: SAMPLE LOCAL PLANE
    if (sampling_method == 1)
    {
        mls.setUpsamplingRadius(upsampling_radius);
        mls.setUpsamplingStepSize(upsampling_step_size);
    }
    // sampling method: RANDOM UNIFORM DENSITY
    else if (sampling_method == 2)
    {
        mls.setPointDensity(desired_num_of_points_in_radius);
    }
    // sampling method: VOXEL GRID DILATION
    else if (sampling_method == 3)
    {
        mls.setDilationIterations(dilation_iterations);
        mls.setDilationVoxelSize(dilation_voxel_size);
    }

    // calculate normals
    mls.setComputeNormals(calculate_normals);
    // Create a KD-Tree
    pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGB>);
    tree->setInputCloud(input_cloud->get_cloud());
    mls.setSearchMethod (tree);

    pcl::PointCloud<pcl::PointXYZRGB> ret_cloud;

    opt_cluster_cloud_processing(input_cloud, caller_dialog, mls, individual_cluster, 0, ret_cloud);
    input_cloud->set_cloud(ret_cloud);

    if (calculate_normals)
    {
        //        pcl::PointCloud<pcl::Normal>::Ptr normals;
        //        normals = mls.get_normals();
        pcl::PointCloud<pcl::Normal> normals;
        mls.get_normals(normals);
        input_cloud->set_normals(normals);
    }

    std::cout << "PointCloud after upsampling: " << ret_cloud.width * ret_cloud.height
              << " data points (" << pcl::getFieldsList (ret_cloud) << ")." << std::endl;

}

void bilateral_upsampling_inplace(CloudData *input_cloud, const int window_size, const bool use_sigma_color, const double sigma_color, const bool use_sigma_depth,
                                  const double sigma_depth, const bool individual_cluster, QWidget *caller_dialog)
{
    pcl::BilateralUpsampling<pcl::PointXYZRGB, pcl::PointXYZRGB> bi_up;
    bi_up.setInputCloud(input_cloud->get_cloud());
    bi_up.setWindowSize(window_size);
    if(use_sigma_color)
        bi_up.setSigmaColor(sigma_color);
    if(use_sigma_depth)
        bi_up.setSigmaDepth(sigma_depth);

    pcl::PointCloud<pcl::PointXYZRGB> ret_cloud;

    opt_cluster_cloud_processing(input_cloud, caller_dialog, bi_up, individual_cluster, 0, ret_cloud);
    input_cloud->set_cloud(ret_cloud);

    std::cout << "PointCloud after bilateral upsampling: "<< ret_cloud.size() << " data points." << std::endl;
}

void remove_outliers_radius_inplace_1(CloudData *input_cloud, const double search_radius, const int min_neighbors_in_radius, const bool individual_clusters, QWidget* caller_dialog)
{
    pcl::RadiusOutlierRemoval<pcl::PointXYZRGB> outrem;
    outrem.setInputCloud(input_cloud->get_cloud());
    outrem.setRadiusSearch(search_radius);
    outrem.setMinNeighborsInRadius (min_neighbors_in_radius);

    pcl::PointCloud<pcl::PointXYZRGB> ret_cloud;
    opt_cluster_cloud_filtering(input_cloud, caller_dialog, outrem, individual_clusters, min_neighbors_in_radius, ret_cloud);
    input_cloud->set_cloud(ret_cloud);
}

void remove_outliers_statistical_inplace(CloudData *input_cloud, const int number_of_neighbors, const double std_dev_multiplyer, const bool individual_clusters, QWidget *caller_dialog)
{
    pcl::StatisticalOutlierRemoval<pcl::PointXYZRGB> outrem;
    outrem.setInputCloud(input_cloud->get_cloud());
    outrem.setMeanK(number_of_neighbors);
    outrem.setStddevMulThresh (std_dev_multiplyer);

    pcl::PointCloud<pcl::PointXYZRGB> ret_cloud;
    opt_cluster_cloud_filtering(input_cloud, caller_dialog, outrem, individual_clusters, number_of_neighbors, ret_cloud);  // min_number_of_neighbors = 0?
    input_cloud->set_cloud(ret_cloud);
}

void region_growing_segmentation_inplace(CloudData *input_cloud, const bool individual_cluster, const int number_of_neighbors,
                                         const bool use_min_cluster_size, const int min_cluster_size, const bool use_max_cluster_size,
                                         const int max_cluster_size, const bool use_smoothness_threshold, const double smoothness_threshold,
                                         const bool use_curvature_threshold, const double curvature_threshold, const bool make_residual_test,
                                         const double residual_test, const bool merge_resultant_clusters, QWidget* caller_dialog)
{

    pcl::search::Search<pcl::PointXYZRGB>::Ptr tree = boost::shared_ptr<pcl::search::Search<pcl::PointXYZRGB> > (new pcl::search::KdTree<pcl::PointXYZRGB>);
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud = input_cloud->get_cloud();
    pcl::PointCloud<pcl::Normal>::Ptr normals = input_cloud->get_normals();

    pcl::RegionGrowing<pcl::PointXYZRGB, pcl::Normal> reg;
    if (use_min_cluster_size)
        reg.setMinClusterSize (min_cluster_size);
    if (use_max_cluster_size)
        reg.setMaxClusterSize (max_cluster_size);

    reg.setSearchMethod (tree);
    reg.setNumberOfNeighbours (number_of_neighbors);
    reg.setInputCloud (cloud);
    reg.setInputNormals (normals);

    reg.setSmoothModeFlag(use_smoothness_threshold);
    reg.setSmoothnessThreshold (smoothness_threshold);
    reg.setCurvatureTestFlag(use_curvature_threshold);
    reg.setCurvatureThreshold (curvature_threshold);

    reg.setResidualTestFlag(make_residual_test);
    reg.setResidualThreshold(residual_test);

    std::vector <pcl::PointIndices> cluster_indices;
    opt_cluster_cloud_extraction(input_cloud, caller_dialog, reg, individual_cluster, 0, cluster_indices);

    input_cloud->set_cluster_indices(cluster_indices);

    if (merge_resultant_clusters)
    {
        merge_clusters(input_cloud, *input_cloud);
    }

    std::cout << "Number of clusters found: " << input_cloud->get_number_of_clusters() << std::endl;
}



void poisson_surface_reconstruction_inplace(CloudData *input_cloud, const bool use_max_tree_depth, const int maximum_tree_depth,
                                            const bool use_min_tree_depth, const int min_tree_depth,
                                            const bool use_point_weight, const double point_weight,
                                            const bool use_scale, const double scale,
                                            const bool use_solver_divide, const int solver_divide,
                                            const bool use_iso_divide, const int iso_divide,
                                            const bool use_sample_per_node, const double sample_per_node,
                                            const bool use_degree, const int degree,
                                            const bool set_confidence, const bool set_manifold, const bool set_polygons,
                                            const bool individual_cluster, QWidget* caller_dialog)
{
    pcl::PointCloud<pcl::PointNormal>::Ptr cloud_smoothed_normals (new pcl::PointCloud<pcl::PointNormal>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_xyz (new pcl::PointCloud<pcl::PointXYZ>);
    Process::convert_cloudXYZRGB_2_cloudXYZ(*input_cloud->get_cloud(), *cloud_xyz);
    concatenateFields(*cloud_xyz, *input_cloud->get_normals(), *cloud_smoothed_normals);

    pcl::Poisson<pcl::PointNormal> poisson;
    if (use_max_tree_depth)
        poisson.setDepth(maximum_tree_depth);
    if (use_min_tree_depth)
        poisson.setMinDepth(min_tree_depth);
    if (use_point_weight)
        poisson.setPointWeight(point_weight);
    if (use_scale)
        poisson.setScale(scale);
    if (use_solver_divide)
        poisson.setSolverDivide(solver_divide);
    if (use_iso_divide)
        poisson.setIsoDivide(iso_divide);
    if (use_sample_per_node)
        poisson.setSamplesPerNode(sample_per_node);
    if (use_degree)
        poisson.setDegree(degree);

    poisson.setConfidence(set_confidence);
    poisson.setManifold(set_manifold);
    poisson.setOutputPolygons(set_polygons);

    poisson.setInputCloud(cloud_smoothed_normals);
    pcl::PolygonMesh ret_mesh;
    //    poisson.reconstruct(ret_mesh);
    opt_cluster_mesh_reconstruction(input_cloud, caller_dialog, poisson, individual_cluster, 0, ret_mesh);

    input_cloud->set_mesh(ret_mesh);
}

void fast_mesh_triangulation_inplace(CloudData *input_cloud, const double search_radius, const double mu, const int max_nearest_neighbors,
                                     const double max_surface_angle, const double min_angle, const double max_angle, const bool normal_consistency,
                                     const bool individual_cluster, QWidget* caller_dialog)
{
    if (input_cloud->has_normals())
    {
        pcl::PointCloud<pcl::PointXYZ> cloudXYZ;
        Process::convert_cloudXYZRGB_2_cloudXYZ(*input_cloud->get_cloud(), cloudXYZ);

        // Concatenate the XYZ and normal fields*
        pcl::PointCloud<pcl::PointNormal>::Ptr cloud_with_normals (new pcl::PointCloud<pcl::PointNormal>);
        pcl::concatenateFields(cloudXYZ, *input_cloud->get_normals(), *cloud_with_normals);

        // Create search tree*
        pcl::search::KdTree<pcl::PointNormal>::Ptr tree (new pcl::search::KdTree<pcl::PointNormal>);
        tree->setInputCloud (cloud_with_normals);

        // Initialize objects
        pcl::GreedyProjectionTriangulation<pcl::PointNormal> gp3;

        // Set the maximum distance between connected points (maximum edge length)
        gp3.setSearchRadius (search_radius);

        // Set typical values for the parameters
        gp3.setMu (mu);
        gp3.setMaximumNearestNeighbors (max_nearest_neighbors);
        gp3.setMaximumSurfaceAngle(max_surface_angle); // 45 degrees
        gp3.setMinimumAngle(min_angle); // 10 degrees
        gp3.setMaximumAngle(max_angle); // 120 degrees
        gp3.setNormalConsistency(normal_consistency);

        // Get result
        gp3.setInputCloud (cloud_with_normals);
        gp3.setSearchMethod (tree);

        pcl::PolygonMesh ret_triangle_mesh;
        //        gp3.reconstruct (ret_triangle_mesh);
        opt_cluster_mesh_reconstruction(input_cloud, caller_dialog, gp3, individual_cluster, 0, ret_triangle_mesh);

        input_cloud->set_mesh(ret_triangle_mesh);
    }

}

void extract_concave_hull_mesh_inplace(CloudData *input_cloud,
                                       const double alpha,
                                       const int dimensions,
                                       const bool keep_informations,
                                       const bool individual_cluster,
                                       QWidget *caller_dialog)
{
    pcl::ConcaveHull<pcl::PointXYZRGB> chull;
    chull.setInputCloud(input_cloud->get_cloud());
    chull.setAlpha(alpha);
    chull.setDimension(dimensions);
    chull.setKeepInformation(keep_informations);

    pcl::PolygonMesh ret_mesh;
    opt_cluster_mesh_reconstruction_no_normals(input_cloud,caller_dialog,chull,individual_cluster,0,ret_mesh);
    //    chull.reconstruct(ret_mesh);

    input_cloud->set_mesh(ret_mesh);
}

void extract_concave_hull_cloud_inplace(CloudData *input_cloud, const double alpha, const int dimensions, const bool keep_informations, const bool individual_cluster, QWidget *caller_dialog)
{
    pcl::ConcaveHull<pcl::PointXYZRGB> chull;
    chull.setInputCloud(input_cloud->get_cloud());
    chull.setAlpha(alpha);
    chull.setDimension(dimensions);
    chull.setKeepInformation(keep_informations);

    pcl::PointCloud<pcl::PointXYZRGB> tmp_cloud;
    opt_cluster_cloud_reconstruction(input_cloud, caller_dialog, chull, individual_cluster, 0, tmp_cloud);
    //    chull.reconstruct(tmp_cloud);

    input_cloud->set_cloud(tmp_cloud);
}

void smooth_mesh_laplacian(CloudData *input_cloud,
                           const int number_of_iterations,
                           const float convergence,
                           const float relaxation_factor,
                           const bool use_feature_edge_smoothing,
                           const float feature_angle,
                           const float edge_angle,
                           const bool use_boundary_smoothing,
                           QWidget *caller_dialog)
{
    pcl::MeshSmoothingLaplacianVTK smoother;
    smoother.setInputMesh(input_cloud->get_mesh());
    smoother.setNumIter(number_of_iterations);
    smoother.setConvergence(convergence);
    smoother.setRelaxationFactor(relaxation_factor);
    smoother.setFeatureEdgeSmoothing(use_feature_edge_smoothing);
    smoother.setFeatureAngle(feature_angle);
    smoother.setEdgeAngle(edge_angle);
    smoother.setBoundarySmoothing(use_boundary_smoothing);


    pcl::PolygonMesh smoothed_mesh;
    smoother.process(smoothed_mesh);

    input_cloud->set_mesh(smoothed_mesh);
}

//void smoothing_windowed_sinc(CloudData *input_cloud)
//{
//    pcl::MeshSmoothingWindowedSincVTK smoother;
//    smoother.setInputMesh(input_cloud->get_mesh());
//    smoother.setNumIter(5);
//    smoother.setPassBand(10000);
//    smoother.setNormalizeCoordinates(true);
//    smoother.setFeatureEdgeSmoothing(true);
//    smoother.setFeatureAngle(200);
//    smoother.setEdgeAngle(200);
//    smoother.setBoundarySmoothing(true);

//    pcl::PolygonMesh smoothed_mesh;
//    smoother.process(smoothed_mesh);

//    input_cloud->set_mesh(smoothed_mesh);
//    std::cout << "windowed sinc smoothing done!" << std::endl;
//}

void quadratic_mesh_decimation(CloudData *input_cloud, const float target_reduction_factor, const int iterations, QWidget *caller_dialog)
{
    pcl::MeshQuadricDecimationVTK smoother;
    smoother.setTargetReductionFactor(target_reduction_factor);
    smoother.setInputMesh(input_cloud->get_mesh());

    pcl::PolygonMesh smoothed_mesh;
    smoother.process(smoothed_mesh);
    if (iterations > 1)
    {
        for (int i = 2; i<=iterations; ++i)
        {
            pcl::PolygonMesh next_smoothed;
            pcl::PolygonMesh::Ptr smoothed_ptr = boost::make_shared<pcl::PolygonMesh>(smoothed_mesh);
            smoother.setInputMesh(smoothed_ptr);
            smoother.process(next_smoothed);
            smoothed_mesh = next_smoothed;
        }
    }

    input_cloud->set_mesh(smoothed_mesh);

}

void mesh_subdivision(CloudData *input_cloud, const int filter_type, QWidget *caller_dialog)
{
    pcl::MeshSubdivisionVTK smoother;
    smoother.setInputMesh(input_cloud->get_mesh());
    if (filter_type == 0)
    {
        smoother.setFilterType(pcl::MeshSubdivisionVTK::LINEAR);
        std::cout << "LINEAR" << std::endl;

    }
    else if (filter_type == 1)
    {
        smoother.setFilterType(pcl::MeshSubdivisionVTK::LOOP);
        std::cout << "LOOP" << std::endl;
    }
    else if (filter_type == 2)
    {
        smoother.setFilterType(pcl::MeshSubdivisionVTK::BUTTERFLY);
        std::cout << "BUTTERFLY" << std::endl;

    }

    pcl::PolygonMesh smoothed_mesh;
    smoother.process(smoothed_mesh);

    input_cloud->set_mesh(smoothed_mesh);

}


//// TEST SURFL SMOOTHING (NOT WORKING) ***********************
//pcl::SurfelSmoothing<pcl::PointXYZRGB, pcl::Normal> surfel_smoothing;
//std::vector<CloudData*> selected_clouds = get_selected_clouds();
//CloudData* first_cloud = selected_clouds.at(0);
//surfel_smoothing.setInputCloud(first_cloud->get_cloud());
//pcl::PointCloud<pcl::Normal>::Ptr normals = first_cloud->get_normals();
//surfel_smoothing.setInputNormals(normals);
//pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGB>);
//tree->setInputCloud(first_cloud->get_cloud());
//surfel_smoothing.setSearchMethod (tree);

//pcl::PointCloud<pcl::PointXYZRGB>::Ptr ret_cloud (new pcl::PointCloud<pcl::PointXYZRGB>);
//pcl::PointCloud<pcl::Normal>::Ptr ret_normals (new pcl::PointCloud<pcl::Normal>);
////    bool init_success = surfel_smoothing.initCompute();
//surfel_smoothing.computeSmoothedCloud(ret_cloud, ret_normals);
//first_cloud->set_cloud(*ret_cloud);
//first_cloud->set_normals(*ret_normals);
//this->refresh_cloud();
//// **********************************************************

}
