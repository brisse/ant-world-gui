# README #

This README explains necessary steps to get Habitat3D up and running.

### What is this repository for? ###

* This repository is about Habitat3D, an open source software to convert terrestrial LiDAR scans of natural environments into photorealistic meshes.
* Version 0.9

### How do I get set up? ###

Habitat3D is written in C++ and requires the following libraries:

* Point Cloud Library (PCL v1.7.2)
* Visualization Toolkit (VTK v3.6.0)
* Qt (v4.8.7)
* BOOST (v1.58.0)

Please use the CMakeLists.txt file to compile the code. We recommend Qt Creator as an IDE.

### Who do I talk to? ###

* Habitat3D was implemented by Benjamin Risse at the University of Edinburgh.
* For any questions / suggestions please contact brisse@inf.ed.ac.uk