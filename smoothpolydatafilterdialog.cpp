#include "smoothpolydatafilterdialog.h"
#include "../build/debug/ui_smoothpolydatafilterdialog.h"

SmoothPolyDataFilterDialog::SmoothPolyDataFilterDialog(QWidget *parent) :
    CustomQFilterDialog(parent),
    ui(new Ui::SmoothPolyDataFilterDialog)
{
    ui->setupUi(this);
}

SmoothPolyDataFilterDialog::~SmoothPolyDataFilterDialog()
{
    delete ui;
}

void SmoothPolyDataFilterDialog::on_pushButton_filter_clicked()
{
    double convergence = this->ui->doubleSpinBox_convergence->value();
    int iterations = this->ui->spinBox_iterations->value();
    double relaxation_factor = this->ui->doubleSpinBox_relaxation_factor->value();
    bool use_feature_edge_smoothing = this->ui->checkBox_use_feature_edge_smoothing->isChecked();
    bool use_boundary_smoothing = this->ui->checkBox_use_boundary_smoothing->isChecked();
    double feature_angle = this->ui->doubleSpinBox_feature_angle->value();
    double edge_angle = this->ui->doubleSpinBox_edge_angle->value();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        Process::smooth_poly_data_filter_inplace(*cloud_it, convergence,
                                                 iterations, relaxation_factor,
                                                 use_feature_edge_smoothing,
                                                 feature_angle, edge_angle,
                                                 use_boundary_smoothing,
                                                 this);
        emit send_status_message(QString("Smoothing poly data done! Number of polygons: " + QString::number((*cloud_it)->get_mesh_size())));
    }

    emit filter_button_pressed();

}

void SmoothPolyDataFilterDialog::on_pushButton_batch_clicked()
{
    double convergence = this->ui->doubleSpinBox_convergence->value();
    int iterations = this->ui->spinBox_iterations->value();
    double relaxation_factor = this->ui->doubleSpinBox_relaxation_factor->value();
    bool use_feature_edge_smoothing = this->ui->checkBox_use_feature_edge_smoothing->isChecked();
    bool use_boundary_smoothing = this->ui->checkBox_use_boundary_smoothing->isChecked();
    double feature_angle = this->ui->doubleSpinBox_feature_angle->value();
    double edge_angle = this->ui->doubleSpinBox_edge_angle->value();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        batch_processor->add_event(new SmoothPolyDataFilterEvent(*cloud_it, convergence, iterations, relaxation_factor, use_feature_edge_smoothing, feature_angle, edge_angle, use_boundary_smoothing, batch_processor));
    }

    emit send_status_message(QString("Smooth poly data filter batch processing for " + QString::number(clouds.size()) + " clouds." ));

}

void SmoothPolyDataFilterDialog::on_pushButton_close_clicked()
{
    this->hide();
}
