#include "batchprocessordialog.h"
#include "../build/debug/ui_batchprocessordialog.h"

BatchProcessorDialog::BatchProcessorDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::BatchProcessorDialog)
{
    ui->setupUi(this);
}

BatchProcessorDialog::~BatchProcessorDialog()
{
    delete ui;
}

void BatchProcessorDialog::add_event(BatchEvent *event)
{
    this->events.push_back(event);
    std::cout << "Event #" << events.size() << " of type " << event->get_event_name() << " added!" << std::endl;
}

void BatchProcessorDialog::start_execution(void)
{
    QTime myTimer;
    myTimer.start();
    for (std::vector<BatchEvent*>::iterator event_it = events.begin(); event_it != events.end(); ++event_it)
    {
        (*event_it)->execute();
        events_processed_list.push_back(events_unprocessed_list.at(0));
        events_unprocessed_list.erase(events_unprocessed_list.begin());
        update_dialog();
    }
    int milliseconds = myTimer.elapsed();
    std::cout << "Processing time: "  << milliseconds << " ms." << std::endl;

    this->events.clear();
}

void BatchProcessorDialog::refresh_list()
{
    int counter = 1;
    this->events_processed_list.clear();
    this->events_unprocessed_list.clear();;
    for (std::vector<BatchEvent*>::iterator event_it = events.begin(); event_it != events.end(); ++event_it)
    {
        QString q_str = QString::number(counter) +": " + (*event_it)->get_event_details();
        this->events_unprocessed_list.push_back(q_str);
        ++counter;
    }
    this->update_dialog();
}

void BatchProcessorDialog::update_dialog()
{
    this->ui->listWidget_processed->clear();
    this->ui->listWidget_unprocessed->clear();
    this->ui->progressBar->setValue(0);
    int number_of_events = events.size();
    int number_of_processed_events = events_processed_list.size();
    this->ui->progressBar->setMaximum(number_of_events);
    this->ui->progressBar->setValue(number_of_processed_events);
    for (std::vector<QString>::iterator it = events_processed_list.begin(); it != events_processed_list.end(); ++it)
    {
        this->ui->listWidget_processed->addItem(*it);
    }
    for (std::vector<QString>::iterator it = events_unprocessed_list.begin(); it != events_unprocessed_list.end(); ++it)
    {
        this->ui->listWidget_unprocessed->addItem(*it);
    }
    this->ui->listWidget_processed->repaint();
    this->ui->listWidget_unprocessed->repaint();
    qApp->processEvents();
}


void BatchProcessorDialog::on_pushButton_start_clicked()
{
    this->start_execution();
    emit filter_button_pressed();
}

void BatchProcessorDialog::on_pushButton_close_clicked()
{
    this->hide();
}

