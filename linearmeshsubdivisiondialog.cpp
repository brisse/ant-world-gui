#include "linearmeshsubdivisiondialog.h"
#include "../build/debug/ui_linearmeshsubdivisiondialog.h"

LinearMeshSubdivisionDialog::LinearMeshSubdivisionDialog(QWidget *parent) :
    CustomQFilterDialog(parent),
    ui(new Ui::LinearMeshSubdivisionDialog)
{
    ui->setupUi(this);
}

LinearMeshSubdivisionDialog::~LinearMeshSubdivisionDialog()
{
    delete ui;
}

void LinearMeshSubdivisionDialog::on_pushButton_filter_clicked()
{
    int filter_type = ui->comboBox_filter_type->currentIndex();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        Process::mesh_subdivision(*cloud_it, filter_type, this);
        emit send_status_message(QString("Mesh subdivision done! New mesh size is " + QString::number((*cloud_it)->get_mesh_size())));
    }

    emit filter_button_pressed();
}

void LinearMeshSubdivisionDialog::on_pushButton_close_clicked()
{
    this->hide();
}

void LinearMeshSubdivisionDialog::on_pushButton_batch_clicked()
{
    int filter_type = ui->comboBox_filter_type->currentIndex();

    for (std::vector<CloudData*>::iterator cloud_it = clouds.begin(); cloud_it != clouds.end(); ++cloud_it)
    {
        batch_processor->add_event(new MeshSubdivisionEvent(*cloud_it, filter_type, batch_processor));
    }

    emit send_status_message(QString("Mesh subdividion batch processing for " + QString::number(clouds.size()) + " clouds." ));
}
