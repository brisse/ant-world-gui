#include "inputoutput.h"

namespace InputOutput
{

void save_cloud(CloudData *cloud, QString cur_file_path, QString cloud_suffix)
{
    std::string cloud_name = cloud->get_cloud_name();

    QString absPath = cur_file_path;
    absPath.append("/");
    absPath.append(QString::fromStdString(cloud_name)).append("_");
    QTime time = QTime::currentTime();
    QDate date = QDate::currentDate();
    QString strDate = date.toString("yyyy-MM-dd");
    QString strTime = time.toString("hh-mm-ss");
    absPath.append(strDate);
    absPath.append("_");
    absPath.append(strTime);

    QDir dir = QDir::root();
    dir.mkpath(absPath);

    QString cloud_file_name("/");
    cloud_file_name.append(QString::fromStdString(cloud_name)).append(".cdat");
    QString curPath = absPath + cloud_file_name;
    QFile cloud_file(curPath);
    if (!cloud_file.open(QIODevice::WriteOnly | QIODevice::Text))
        return;
    QTextStream out(&cloud_file);
    out.setCodec("UTF-8");
    out << "CLOUD_NAME" << endl;
    out << QString::fromStdString(cloud_name) << endl;
    cloud_file.close();


    // write cloud indices
    if (cloud->has_clusters())
    {
        curPath = absPath + QString::fromStdString("/indices.ind");
        QFile indices_file(curPath);
        if (!indices_file.open(QIODevice::WriteOnly | QIODevice::Text))
            return;
        QTextStream ind_out(&indices_file);
        ind_out.setCodec("UTF-8");
        std::vector<pcl::PointIndices> indices = cloud->get_cluster_indices();
        for (std::vector<pcl::PointIndices>::iterator pi_it = indices.begin(); pi_it != indices.end(); ++pi_it)
        {
            std::vector<int> p_indi = (*pi_it).indices;
            for (std::vector<int>::iterator in_it = p_indi.begin(); in_it != p_indi.end(); ++in_it)
            {
                ind_out << *in_it << " ";
            }
            ind_out << endl;
        }
        indices_file.close();
    }



    curPath = absPath + QString::fromStdString("/cloud.pcd");
    pcl::io::savePCDFile (curPath.toStdString(), *(cloud->get_cloud()), true);

    curPath = absPath + QString::fromStdString("/cloud.ply");
    pcl::io::savePLYFileASCII (curPath.toStdString(), *(cloud->get_cloud()));

    if (cloud->has_normals())
    {
        curPath = absPath + QString::fromStdString("/normals.pcd");
        pcl::io::savePCDFile(curPath.toStdString(), *(cloud->get_normals()), true);
    }

    if (cloud->has_mesh())
    {
        curPath = absPath + QString::fromStdString("/mesh.ply");

        pcl::io::savePLYFile(curPath.toStdString(), *(cloud->get_mesh()));
//        pcl::io::save(curPath.toStdString(), *(cloud->get_mesh()));

        curPath = absPath + QString::fromStdString("/mesh.vtk");
        pcl::io::savePLYFile(curPath.toStdString(), *(cloud->get_mesh()));
//        pcl::io::save(curPath.toStdString(), *(cloud->get_mesh()));

        curPath = absPath + QString::fromStdString("/mesh.obj");
        pcl::io::savePLYFile(curPath.toStdString(), *(cloud->get_mesh()));
//        pcl::io::save(curPath.toStdString(), *(cloud->get_mesh()));
    }
}

void save_clouds(std::vector<CloudData*> selected_clouds, QString cur_file_path, QString cloud_suffix)
{
    for (std::vector<CloudData*>::iterator cloud_it = selected_clouds.begin(); cloud_it != selected_clouds.end(); ++cloud_it)
    {
        save_cloud(*cloud_it, cur_file_path, cloud_suffix);


//        std::string cloud_name = (*cloud_it)->get_cloud_name();

//        QString absPath = cur_file_path;
//        absPath.append("/");
//        absPath.append(QString::fromStdString(cloud_name)).append("_");
//        QTime time = QTime::currentTime();
//        QDate date = QDate::currentDate();
//        QString strDate = date.toString("yyyy-MM-dd");
//        QString strTime = time.toString("hh-mm-ss");
//        absPath.append(strDate);
//        absPath.append("_");
//        absPath.append(strTime);

//        QDir dir = QDir::root();
//        dir.mkpath(absPath);

//        QString cloud_file_name("/");
//        cloud_file_name.append(QString::fromStdString(cloud_name)).append(".cdat");
//        QString curPath = absPath + cloud_file_name;
//        QFile cloud_file(curPath);
//        if (!cloud_file.open(QIODevice::WriteOnly | QIODevice::Text))
//            return;
//        QTextStream out(&cloud_file);
//        out.setCodec("UTF-8");
//        out << "CLOUD_NAME" << endl;
//        out << QString::fromStdString(cloud_name) << endl;
//        cloud_file.close();


//        // write cloud indices
//        if ((*cloud_it)->has_clusters())
//        {
//            curPath = absPath + QString::fromStdString("/indices.ind");
//            QFile indices_file(curPath);
//            if (!indices_file.open(QIODevice::WriteOnly | QIODevice::Text))
//                return;
//            QTextStream ind_out(&indices_file);
//            ind_out.setCodec("UTF-8");
//            std::vector<pcl::PointIndices> indices = (*cloud_it)->get_cluster_indices();
//            for (std::vector<pcl::PointIndices>::iterator pi_it = indices.begin(); pi_it != indices.end(); ++pi_it)
//            {
//                std::vector<int> p_indi = (*pi_it).indices;
//                for (std::vector<int>::iterator in_it = p_indi.begin(); in_it != p_indi.end(); ++in_it)
//                {
//                    ind_out << *in_it << " ";
//                }
//                ind_out << endl;
//            }
//            indices_file.close();
//        }



//        curPath = absPath + QString::fromStdString("/cloud.pcd");
//        pcl::io::savePCDFile (curPath.toStdString(), *((*cloud_it)->get_cloud()), true);

//        curPath = absPath + QString::fromStdString("/cloud.ply");
//        pcl::io::savePLYFileASCII (curPath.toStdString(), *((*cloud_it)->get_cloud()));

//        if ((*cloud_it)->has_normals())
//        {
//            curPath = absPath + QString::fromStdString("/normals.pcd");
//            pcl::io::savePCDFile(curPath.toStdString(), *((*cloud_it)->get_normals()), true);
//        }

//        if ((*cloud_it)->has_mesh())
//        {
//            curPath = absPath + QString::fromStdString("/mesh.ply");
//            pcl::io::save(curPath.toStdString(), *((*cloud_it)->get_mesh()));

//            curPath = absPath + QString::fromStdString("/mesh.vtk");
//            pcl::io::save(curPath.toStdString(), *((*cloud_it)->get_mesh()));

//            curPath = absPath + QString::fromStdString("/mesh.obj");
//            pcl::io::save(curPath.toStdString(), *((*cloud_it)->get_mesh()));
//        }

    }
}

void load_color_cloud(std::string path, CloudData &ret_cloud)
{
    pcl::PointCloud<pcl::PointXYZRGB> tmp_cloud;
    pcl::io::loadPCDFile<pcl::PointXYZRGB> (path, tmp_cloud);
    QFileInfo file_info(QString::fromStdString(path));
    std::string cur_file_name = file_info.fileName().toStdString();
    std::string cur_file_path = file_info.absolutePath().toStdString();

    // remove file ending from string
    cur_file_name = cur_file_name.substr(0, cur_file_name.size()-4);
    ret_cloud.set_cloud_name(cur_file_name);
    ret_cloud.set_cloud_path(cur_file_path);
    ret_cloud.set_original_cloud(tmp_cloud);
    ret_cloud.set_cloud(tmp_cloud);

}

void load_cloud_data(QString cloudData_file, CloudData &ret_cloud)
{
    QFileInfo file_info(cloudData_file);
    QString cur_file_path = file_info.absolutePath();
    QStringList filter("*.pcd");
    QDir dir(cur_file_path);
    QStringList pcd_files = dir.entryList(filter);

    QFile cdat_file(cloudData_file);
    if (!cdat_file.open(QIODevice::ReadOnly | QIODevice::Text))
        return;
    QTextStream in(&cdat_file);
    QString cloud_name = "";
    while(!in.atEnd())
    {
        QString line = in.readLine();
        if (line == "CLOUD_NAME")
        {
            cloud_name = in.readLine();
            break;
        }
    }
    cdat_file.close();

    if (pcd_files.contains("cloud.pcd"))
    {
        pcl::PointCloud<pcl::PointXYZRGB> tmp_cloud;
        QString cloud_file = cur_file_path + "/cloud.pcd";
        pcl::io::loadPCDFile<pcl::PointXYZRGB> (cloud_file.toStdString(), tmp_cloud);

        std::string cur_cloud_name = cloud_name.toStdString();
        ret_cloud.init(tmp_cloud, cur_cloud_name, cur_file_path.toStdString());

        // load normals
        if (pcd_files.contains("normals.pcd"))
        {
            pcl::PointCloud<pcl::Normal> tmp_normals;
            QString normals_file = cur_file_path + "/normals.pcd";
            pcl::io::loadPCDFile<pcl::Normal> (normals_file.toStdString(), tmp_normals);
            ret_cloud.set_normals(tmp_normals);
        }

        // load mesh
        QStringList filter_ply("*.obj");
        QStringList ply_files = dir.entryList(filter_ply);
        if (ply_files.contains("mesh.obj"))
        {
            pcl::PolygonMesh tmp_mesh;
            QString mesh_file = cur_file_path + "/mesh.obj";

            pcl::io::loadPLYFile(mesh_file.toStdString(), tmp_mesh);
//            pcl::io::load(mesh_file.toStdString(), tmp_mesh);

            ret_cloud.set_mesh(tmp_mesh);
        }

        // load cluster indices
        QStringList filter_ind("*.ind");
        QStringList ind_files = dir.entryList(filter_ind);
        if (ind_files.contains("indices.ind"))
        {
            std::vector<pcl::PointIndices> tmp_indices;
            QString indices_file_name = cur_file_path + "/indices.ind";
            QFile indices_file(indices_file_name);
            if (!indices_file.open(QIODevice::ReadOnly | QIODevice::Text))
                return;
            QTextStream indices_in(&indices_file);
            while(!indices_in.atEnd())
            {
                QString line = indices_in.readLine();
                pcl::PointIndices indices_line;
                foreach (QString numStr, line.split(" ", QString::SkipEmptyParts))
                {
                    int value = numStr.toInt();
                    indices_line.indices.push_back(value);
                }
                tmp_indices.push_back(indices_line);
            }
            cdat_file.close();
            ret_cloud.set_cluster_indices(tmp_indices);
        }
    }
}


}
