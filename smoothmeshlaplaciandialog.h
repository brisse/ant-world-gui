#ifndef SMOOTHMESHLAPLACIANDIALOG_H
#define SMOOTHMESHLAPLACIANDIALOG_H

#include <pcl/point_types.h>
#include <pcl/surface/gp3.h>

#include "cloudprocessing.h"
#include "clouddata.h"

#include "customqfilterdialog.h"

namespace Ui {
class SmoothMeshLaplacianDialog;
}

class SmoothMeshLaplacianDialog : public CustomQFilterDialog
{
    Q_OBJECT

public:
    explicit SmoothMeshLaplacianDialog(QWidget *parent = 0);
    ~SmoothMeshLaplacianDialog();

private slots:
    void on_pushButton_filter_clicked();
    void on_pushButton_batch_clicked();
    void on_pushButton_close_clicked();

private:
    Ui::SmoothMeshLaplacianDialog *ui;
};

#endif // SMOOTHMESHLAPLACIANDIALOG_H
